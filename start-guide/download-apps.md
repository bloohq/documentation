---
description: Blue is available everywhere you work — Web, MacOS, Windows, iOS, & Android.
---

# Download Apps

## Desktop App

The benefits of installing Blue as a desktop application are:

* Faster performance&#x20;
* There is no browser interface around the Blue interface
* Save Blue to Mac Dock or as a Windows desktop shortcut
* True Fullscreen Mode
* Native desktop notifications&#x20;
* Automatic updates&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.18.45@2x (1).png" alt=""><figcaption><p>An immersive experience with the Blue Desktop Application</p></figcaption></figure>

</div>

To install Blue as a desktop application, you'll need a Chromium-based browser such as:

* [Google Chrome](https://www.google.com/chrome/)
* [Brave](https://brave.com/)&#x20;
* [Microsoft Edge](https://www.microsoft.com/edge)
* [Opera](https://www.opera.com/)
* [Vivaldi](https://vivaldi.com/)
* [Chromium](https://www.chromium.org/)&#x20;

Once you have installed any of the above browsers, simply[ log in to Blue](https://app.blue.cc) as usual. You will then notice that there is an install icon on the right-hand side of the URL Address Bar:

<figure><img src="../.gitbook/assets/desktop install.png" alt=""><figcaption><p>Desktop app install icon</p></figcaption></figure>

Click the install icon in your browser's address bar to install the Blue desktop application. This icon is typically represented by a small computer or plus symbol.&#x20;

Once you click on it, you will be prompted through a straightforward installation process. Follow these prompts carefully to ensure that the Blue application is correctly installed on your computer. After the installation is complete, you'll have the advantage of accessing Blue directly from your computer, bypassing the need to open your web browser. This direct access streamlines your workflow and enhances the overall user experience.

You can also watch this how-to video for instructions:

{% embed url="https://www.youtube.com/watch?v=TZye8hnk4ac" %}

For Mac users, once the Blue application is installed, you can add it to your Dock. This provides you with quick and easy access to Blue, right from your Dock. To do this, open the Blue application and right-click (or control-click) on its icon in the Dock. From the menu that appears, select "Options," and then choose "Keep in Dock." This will ensure that the Blue icon remains in your Dock even after you close the application, allowing for faster and more convenient access in the future.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-30 at 15.25.07@2x.png" alt=""><figcaption></figcaption></figure>

Windows users can also benefit from quick access by creating a shortcut for the Blue application on their desktop. After installing Blue, navigate to your system's location where the application is installed. Right-click on the Blue application icon and select "Create shortcut." Then, choose to place this shortcut on your desktop. This creates an easily accessible icon on your desktop, enabling you to launch Blue with a double-click without navigating through your system's menu or file explorer.

## Mobile Applications (iOS & Android)

Blue has native iOS and Android applications, and these can be downloaded via the following link:

* [iOS](https://apps.apple.com/app/blue-teamwork/id6443870775)
* [Android](https://play.google.com/store/apps/details?id=io.bloo.android)

<div>

<figure><img src="../.gitbook/assets/iPhone 14 Pro Max-1.png" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/iPhone 14 Pro Max.png" alt=""><figcaption></figcaption></figure>

</div>

The mobile applications are designed for work on the go and do not have the full functionality of the web applications. You can review and reply to all comments and see the boards and calendars.

Features that are not available on the mobile applications:

* [Dashboards](../dashboards/)
* [Database View ](../views/database.md)
* [List View](../views/list.md)
* [Map View](../views/map.md)
* [Timeline / Gantt Charts](../views/timeline-and-gantt-charts.md)
* [CSV Import](../integrations/csv-import.md)
* [CSV Export ](../integrations/csv-export.md)

## iPad Application

Blue has a specific iPad application that is optimised to take advantage of the tablet's large screen real estate.&#x20;

[You can download the Blue iPad app here. ](https://apps.apple.com/app/blue-teamwork/id6443870775)

## Web Application

<div align="left">

<figure><img src="../.gitbook/assets/Chrome.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Safari.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Edge.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Firefox.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Opera.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Brave.svg" alt=""><figcaption></figcaption></figure>

</div>

Blue is available as a web application that works on all major web browsers. You can access Blue by going to:

{% embed url="https://app.blue.cc" %}

{% hint style="info" %}
**Access Beta**

If you would like to try the latest features of Blue, [you can access Blue Beta by visiting this link.](https://beta.app.blue.cc) You will see all of your regular data, but with additional features in Beta [as noted in our release notes. ](https://blue.cc/releases)
{% endhint %}

## Progressive Web Application (PWA)

Progressive Web Apps (PWAs) are a type of application software delivered through the web, built using standard web technologies including HTML, CSS, and JavaScript. They offer a high-quality user experience that is remarkably similar to native applications.  The Blue PWA combines the convenience and accessibility of a web application with the robust features and performance of a native app, providing a seamless and efficient way to manage your projects and tasks directly from your mobile browser.&#x20;

PWAs are great if you do not have access to the App Store (iOS) or Google Play (Android) or if certain features you want to access on the go are unavailable on our mobile application.&#x20;

### PWA on iOS&#x20;

1. **Open Chrome**: Launch the Chrome app on your iOS device.
2. **Navigate to Blue**: Enter the URL of the Blue web application (https://app.blue.cc)  in Chrome's address bar.
3. **Access the Share Menu**: Once the Blue web application has loaded, tap the Share icon (the rectangle with an upward arrow) at the bottom of the screen.
4. **Add to Home Screen**: In the Share menu, scroll down and select "Add to Home Screen." This will allow you to install the Blue PWA.
5. **Name Your PWA**: You’ll be prompted to name the shortcut before adding it to your home screen. You might want to name it ‘Blue’ for easy identification.
6. **Complete the Installation**: Tap "Add" in the upper right corner of the screen. The Blue PWA icon will now appear on your home screen, similar to any other app icon.
7. **Launch Blue PWA**: Tap the Blue icon from your home screen to open the PWA and use Blue like a native app.

### PWA on Android&#x20;

1. **Open Chrome**: Launch the Chrome app on your Android device.
2. **Go to Blue**: Type the Blue web application's URL (https://app.blue.cc) into Chrome's address bar.
3. **Access the Menu**: Tap the three dots in the upper right corner of the browser to open the menu.
4. **Install the App**: Look for the “Install” option in the menu. This could also appear as “Add to Home screen” or an icon representing a downward arrow inside a box.
5. **Confirm Installation**: A prompt will appear asking if you want to install the app. Confirm by tapping “Install” or “Add.”
6. **Open the Blue PWA**: Once installed, the Blue icon will appear on your home screen or in the app drawer, just like a native application.
7. **Use Blue**: Simply tap on the Blue icon to use the app as you would on a desktop, but with the added benefits of a mobile experience.
