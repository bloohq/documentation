# Custom Domains

You can run Blue on your domain name. This means that instead of this URL structure:

https://app.blue.cc/company/{your-company}/projects

You can run Blue on a domain such as _https://projects.your-domain.com_   or even https://your-domain.com. &#x20;

This is good because it can reinforce your brand instead of ours, give you a memorable login URL, and help Blue bypass strict corporate firewalls.

It's important to note that Blue is still hosted on our infrastructure; only the domain is changing.&#x20;

## Set Up Custom Domain

Configuring your domain is not currently automatic; it requires our team to update configurations on our side.

### 1. Create a CName Record

Firstly, create a CNAME record on your domain  (e.g. `app.yourcompany.com`) and point it to `blue-app.onrender.com`&#x20;

We have conveniently gathered instructions on how to do this for common DNS (Domain Name Service) systems:

<details>

<summary><strong>Godaddy</strong></summary>





1. Log in to your GoDaddy account, and open your product.
2. Navigate to the Domain Control Center.
3. On the Domain Settings page, find and select your domain name.
4. Scroll down to the Additional Settings section, and select Manage DNS.
5. On the DNS Management page, scroll down to the bottom of the Records section, and click Add.
6. In the Type drop-down menu, select _CNAME_, and complete the following required fields:
   * Host - The host name or prefix for the CNAME. You can use a period (.), but not as the first or last character. Consecutive periods (..) are not allowed. The host cannot exceed 25 characters or be the @ symbol.
   * Points to - Set this to `blue-app.onrender.com`
   * TTL - Specify the amount of time the server must cache information for.
7. Click Save.

</details>

<details>

<summary>Bluehost</summary>

1. Log in to your BlueHost account.
2. Click the Domains menu and then click Zone Editor. A new page appears.
3. Navigate to the Add DNS Record section.
4. In the Host Record field, enter the subdomain of the address you want to use for your custom domain.
5. In the Type drop-down menu, select the CNAME option.
6. In the Points To section, enter `blue-app.onrender.com`&#x20;
7. Click Add Record.

</details>

<details>

<summary>Cloudflare</summary>

1. Log in to your Cloudflare account.
2. In the drop-down menu, locate and select your domain.
3. Click the DNS Settings tab.
4. Click "Add Record:
5. Select the CNAME option.&#x20;
6. Set "Name" to your subdomain
7. Set "Target" to `blue-app.onrender.com`
8. Set the Cloudflare Proxy Toggle icon to a grey cloud.

</details>

<details>

<summary>Domain.com</summary>

1. Log in to your Domain.com account.
2. Locate your domain and click Manage.
3. On the menu on the left, click DNS and Nameservers.
4. Click Add DNS record to add the CNAME record.
5. Set target to `blue-app.onrender.com`
6. Click Update DNS to complete the process.

</details>

<details>

<summary>Dreamhost</summary>

1. Log in to your DreamHost account.
2. On the left of the page, click the Domains link.
3. In the Domains drop-down menu, select Manage Domains.
4. Locate your domain and click the DNS link under your domain.
5. Scroll down to the Add a custom DNS record to (your custom domain) section and edit the following fields:
   * Name - type the subdomain you want to use
   * Type - select the CNAME option.
   * Value - enter `blue-app.onrender.com`
6. Click Add Record Now! to add the CNAME record.

</details>

<details>

<summary>Google Domains</summary>

1. Log in to your Google Domains account.
2. Find and click your domain name.
3. On the menu on the left, click DNS.
4. Navigate to the Custom Resource Records section.
5. In the Name field, enter the subdomain of the address you want to use for your custom domain.
6. In the Type drop-down menu, select the CNAME option.
7. In the Data field, enter `blue-app.onrender.com`
8. Click Add to complete the process.

</details>

<details>

<summary>Hostgator</summary>

1. Log in to your HostGator account.
2. On the left-hand side of the page, click CPanel.
3. Scroll down to the Domains section, and click Zone Editor.
4. Find your domain and click the CName Record link next to the domain.
5. In the Add a CName Record dialog box, complete the following empty fields:
   * Name - enter your chosen subdomain/domain name.&#x20;
   * CNAME - enter `blue-app.onrender.com`&#x20;
6. Click Add a CName Record. A message from HostGator appears, stating that you have successfully added a new CNAME to your HostGator account.

</details>

<details>

<summary>NameCheap</summary>

1. Log in to your NameCheap account.
2. In the Account drop-down menu, select Dashboard.
3. Click Domain List and find your domain.
4. Click Manage next to your domain.
5. Click Advanced DNS and navigate to the Host Records section.
6. Click Add New Record.
7. In the Type drop-down menu, select _CNAME Record_ and enter your subdomain as the host.
8. Then enter `blue-app.onrender.com`
9. Click Save all changes to complete the process.

</details>

{% hint style="info" %}
**Not sure how to setup your CNAME configuration?**

[**Book a free support call,** ](https://app.onecal.io/b/blue/support)and our team will help you set it up!
{% endhint %}

### 2. Submit Domain Activation Request&#x20;

The next step is to let us know that you have done step 1 so we can activate your custom domain on our systems. You can fill in the following form:&#x20;

{% embed url="https://forms.blue.cc/f/cl5gwzy1i72436aap9ixsktfzn" %}

We typically activate your domain name on Blue within 24-48 hours.  We will email you if we notice that the configuration is not quite right.&#x20;

## Full White Label

Our vision is to eventually enable Blue customers to have a fully white-labelled version of Blue. This means that every mention of Blue will completely disappear and be replaced by your brand.&#x20;

Specifically, this means:&#x20;

* Your logo entirely replacing Blue branding.&#x20;
* Ability to set a custom colour theme to Blue.
* Notification emails are from your domain instead of Blue.&#x20;

## FAQs

<details>

<summary><strong>Are custom domains free?</strong></summary>

Yes, every customer that has an active monthly or yearly subscription can benefit from using Blue on their domain name.  Note that you will still need to register a domain name if you do not already have one.&#x20;

</details>

<details>

<summary><strong>Can I have multiple custom domains?</strong></summary>

Yes, each account can have up to 3 custom domains. If you need more, please reach out to [help@blue.cc ](mailto:help@blue.cc)

</details>

\
