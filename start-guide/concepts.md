# Concepts

Blue is an extremely powerful platform, once you understand the core concepts.&#x20;

We have an ambitious aim: **to build a platform that can tackle any project or process imaginable**.&#x20;

## Organisations.

A Blue organisation is a top-level container for all projects, lists, records and other features. One user can be part of multiple Blue organisations and seamlessly switch between them.&#x20;

For convenience, the notifications bell works _across_ organisations, so you do not have to switch organisations to see if anything new has happened or if you have been mentioned.  &#x20;

Unlike other tools where you have to log in to a specific organisation via a URL, with Blue, you can log in, and all organisations you are part of will be shown.&#x20;

## Projects

&#x20;The key advantage of using projects to manage your work vs tactics such as email and group chats is that you end up with a clear record of everything that has happened, and if individuals in the project change, new team members can easily see what happened when — and why.&#x20;

Projects in Blue form the fundamental framework for organizing users and data, effectively segmenting different work areas, and collaborating within the platform.&#x20;

Each project in Blue can vary significantly in nature and duration, accommodating various organizational needs.  You can easily archive projects once complete and then are frozen in time, so you can easily go back months or years later to find specific information or download files.&#x20;

**Each Blue project is entirely self-contained.**&#x20;

Only users specifically invited to the project can see the data and collaborate. A Blue account can have an unlimited number of projects.&#x20;

There are two ways of thinking about projects in Blue:

1. **Projects:** The typical project has clear start and end dates, often focused on specific goals or events. Examples include an event launch, a branding overhaul, or a website design project. Such projects are typically goal-oriented, with defined objectives and timelines. They will often be [archived](../projects/archiving-projects.md) once the project is complete.
2. **Processes:** On the other hand, Blue also adeptly handles long-term, continuous processes. These can include activities like customer service operations, ticket-based helpdesks, sales tracking, recruitment processes, and more. These projects are characterized by their ongoing nature, lacking a definitive end date but requiring consistent management and organization.

We've written in-depth about this:

{% embed url="https://www.blue.cc/resources/projects-processes" %}

## Lists

We believe that everything is a process and that good processes lead to predictably good outcomes.

&#x20;While the idea of business processes can sound intimidating, we think this should be simple! The best way to create processes in Blue is to use a mix of [**Lists**](../records/lists.md) and [Automations](../automations/)**.**&#x20;

Each list you create in Blue represents a distinct step in your process. Let's take a look at a few examples:

<details>

<summary>Sales </summary>

* **Unqualified Prospect**: Initial identification of potential clients who may have a need for the product.
* **Qualification**: Assessing the prospect's fit and interest level to ensure they align with the product offering.
* **Writing Proposal**: Developing a tailored proposal that outlines the value proposition and specifics of the product.
* **Proposal Sent**: Sending the completed proposal to the prospect for review and consideration.
* **Final Negotiations**: Engaging in discussions to finalize terms, address concerns, and reach a mutual agreement.
* **Closed Won**: Successfully securing a deal with the prospect, leading to a new customer relationship.
* **Closed Lost**: Acknowledging the prospect's decision to not proceed with the offer at this time.

</details>

<details>

<summary>Customer Onboarding for SaaS</summary>

* **Initial Contact**: Establishing the first point of communication with the new customer.
* **Product Demonstration**: Showcasing the product's features and capabilities to the customer.
* **Free Trial Activation**: Setting up a temporary, no-cost access to the product for the customer to explore.
* **Feedback Gathering**: Collecting initial impressions and suggestions from the customer during or after the trial period.
* **Subscription Discussion**: Discussing various subscription options and finalizing the customer's choice.
* **Onboarding Complete**: Completing all steps necessary to fully integrate the customer into using the product.
* **Follow-up for Future Enhancements**: Maintaining ongoing communication for support and updates about new features.

</details>

<details>

<summary>Software Product Development</summary>

* **Ideas**: Gathering and brainstorming innovative concepts for potential product development.
* **Backlog (Long Term)**: Compiling and prioritizing ideas and features for future exploration and implementation.
* **Spec & Design**: Detailing specifications and designing the product architecture and user experience.
* **Backlog (Short Term)**: Organizing and scheduling immediate tasks and features for the upcoming development cycle.
* **In Progress**: Actively developing the product, including coding and initial testing of features.
* **Code Review**: Evaluating and refining the code for quality, performance, and adherence to standards.
* **Dev Environment**: Testing the product in a development environment to identify and fix issues and for availability to wider team.
* **Beta Environment**: Deploying the product in a beta environment for real-world user testing and feedback with selected customers.&#x20;
* **Shipped**: Officially releasing the fully developed and tested product to all customers

</details>

<details>

<summary>Hiring </summary>

* **Received Applications**: Collecting and organizing applications from potential candidates.
* **Screening Candidates**: Reviewing applications to shortlist candidates based on qualifications and fit.
* **Initial Interview**: Conducting preliminary interviews to assess the candidates' suitability and interest.
* **Skill Assessment**: Evaluating the candidates' specific skills through tests or practical assignments.
* **Final Interview**: Engaging in in-depth interviews with top candidates, often including team or higher management.
* **Offer Extension**: Presenting the selected candidate with a formal job offer.
* **Candidate Onboarding**: Integrating the new hire into the company through orientation and training sessions.

</details>

As these examples show, this gives a bird' s-eye view of the entire process, with various records flowing through it.&#x20;

In each example, records represent different concepts:

1. In the Sales, records signify a prospect.
2. In the Customer Onboarding for SaaS, records signify a customer.
3. In the Software Product Development, records signify a feature.
4. In Hiring, records signify a candidate. &#x20;

You can then use Automations to make something happen during specific steps in the process. The most common trigger for automations is when a record moves to a specific step in the process. You can then have Blue automatically do a variety of actions. Here are a few examples:

1. Create an SOP checklist:  Blue can create pre-set checklists based on your SOPs
2. Assign someone: Assign a specific individual to take responsibility for a task or record within your process.
3. Set a relative due date: Automatically establish a due date for a task relative to a particular step or event in your process.
4. Tag: Apply relevant tags to records to help with organization and categorization.
5. [**Send a custom email notification:**](../automations/actions/email-automations.md) When specific conditions are met within your process, trigger the automated sending of a personalized email notification.

## Records

[**Records**](../projects/records.md) are the fundamental building blocks that form the basis of the system's functionality and data organization. Records are highly versatile and can be renamed to suit your specific needs, allowing them to represent a wide range of entities. Whether it's tracking customer interactions, counting sales records, managing project milestones, or cataloguing inventory items, records in Blue can be adapted to encapsulate virtually any type of information you require. This flexibility ensures that Blue can be tailored to fit diverse business processes and operational needs, making records a powerful tool for organizing and managing critical data.

One key advantage of using records in Blue is that they serve as a single, centralized repository for all relevant information. Blue becomes your go-to source for accurate and up-to-date information by capturing data within records.&#x20;

This eliminates searching for necessary information through scattered documents, spreadsheets, or emails. **Having a single source of truth ensures data consistency, reduces duplication, and streamlines communication among team members.**

## Fields

Records can be easily customized to capture the specific data points relevant to your project or process. Blue allows you to create custom fields to store additional information beyond the standard fields like title, assignee, date, and tags. Custom fields can be of various types, such as text, number, dropdown, or even reference to other records, enabling you to create a structured data model that aligns perfectly with your requirements.

### Default Fields

All records contain the following fields:

* **Name:** This is the title or short description that identifies the record. It helps to quickly recognize and distinguish between different records in a project.
* **Assignee:** The person or team member who is responsible for managing or completing the work associated with this record. Assigning a record ensures clarity on ownership and accountability.
* **Date:** This field allows you to set due dates, start dates, or any other relevant timeline information for the record. Tracking dates helps with scheduling, prioritization, and monitoring the record's progress.
* **Tags:** These are labels or categories that can be applied to records. Tagging records enables easier filtering, grouping, and organization based on attributes like priority, type, or department.
* **Dependencies:** Dependencies define relationships between records, establishing a hierarchy or order in which they should be completed. For example, you can mark one record as "blocking" another, meaning the blocked record cannot be finished until the blocking one is done.
* **Description:** This free-form text field lets you add detailed information, context, or instructions related to the record. The description helps comprehensively explain the record's purpose, requirements, or other relevant details.

### Custom Fields&#x20;

Blue offers the ability to add custom fields to your records. These fields let you capture specific important data for your project or business. Custom fields allow you to store any information, such as sales numbers, stock details, or project updates, ensuring that your records match your unique needs. By setting up your data structure, you can tailor Blue's records to serve your business requirements better.

{% content-ref url="../custom-fields/" %}
[custom-fields](../custom-fields/)
{% endcontent-ref %}

## Views

In addition to the records themselves, Blue provides a variety of views that allow you to visualize and interact with your project data in different ways. These views offer unique perspectives and capabilities to suit the diverse needs of your team and workflows.

* [**Board View**](../views/board.md)**:** The default kanban-style board allows you to structure your process by organizing records into lists representing each stage or step.
* [**Calendar View**](../views/calendar.md)**:** This calendar-based perspective makes tracking due dates, schedules, and temporal relationships across your records easy.
* [**Database View**](../views/database.md)**:** This view presents your records in an interactive spreadsheet-style table, enabling powerful filtering, sorting, and bulk editing capabilities for managing large datasets.
* [**Timeline View**](../views/timeline-and-gantt-charts.md)**:** The Gantt chart-style timeline gives you a visual overview of your records displayed chronologically, making it simple to track schedules, deadlines, and dependencies.
* [**Map View:**](../views/map.md) This geographic view plots records with location data onto an interactive map, unlocking unique insights based on spatial relationships and patterns.
* [**List View**](../views/list.md)**:** Providing a streamlined checklist interface, the List View simplifies progressive tracking through multi-step processes.

This variety of views allows you to choose the most suitable visualization for your specific project needs, whether that's kanban-style workflow management, calendar-based scheduling, or spreadsheet-like data analysis.

## User Roles

Blue's user management system is designed to provide granular control over access and permissions within each project. Users can be assigned to different roles, each with its own set of capabilities and restrictions.&#x20;

{% hint style="info" %}
**Each project is completely private; only users within the project can access the data inside a project.**&#x20;
{% endhint %}

* [**Project Administrators:**](../user-management/roles/project-administrator.md) This is the highest-level role, granting full control over the project's settings, data, and user management. Project Admins can create custom roles, assign permissions, and have unrestricted access.
* [**Team Members:** ](../user-management/roles/team-member.md)Users with the Team Member role have wide-ranging abilities to contribute to the project, including creating records, editing details, and managing the workflow. However, they are limited in actions like deleting the project or accessing administrative settings.
* [**Clients:** ](../user-management/roles/client.md)The Client role is designed for external stakeholders, such as customers or partners, who must provide feedback and stay informed on project progress. Clients can view records and add comments but cannot make structural changes to the project.
* [**Comment-Only:**](../user-management/roles/comment-only.md) This role allows users to participate in discussions by adding comments, but they cannot create, edit, or delete any records or project elements.
* [**View-Only:** ](../user-management/roles/view-only.md)Users with the View-Only role can see all project data, but they are restricted from making any modifications or contributing to the workflow.

In addition to the pre-defined user roles, Blue allows Project Administrators to create [**Custom User Roles**](../user-management/roles/custom-user-roles.md) that can be tailored to their organisation's or project's specific needs. This enables fine-grained control over permissions and access levels.

By leveraging custom user roles, Project Administrators can create a tailored permissions structure that aligns with their organisation's unique requirements, workflows, and team dynamics. This flexibility ensures that every user operates within a secure and appropriate environment, contributing to the project's success.

* **Granular Permissions:** When creating a custom role, Project Administrators can pick and choose which actions and features the role can access. This includes the ability to view, edit, or delete records, as well as manage specific project elements like lists, custom fields, and automations.
* **Consistent Access Across Users:** Custom roles can be assigned to multiple team members, ensuring that a group of users has the same level of permissions and capabilities within the project. This helps maintain consistency and clarity around each user's responsibilities.
* **Restricted Record Visibility:** A powerful feature of custom roles is the ability to limit a user's visibility to only the records they are assigned to. This streamlines workflows and reduces cognitive load by ensuring users only see the information relevant to their work.
* **Controlled Collaboration:** Custom roles also determine a user's ability to invite new members to the project. This control helps Project Administrators govern who can expand the team and maintain oversight of the project's access levels.

By leveraging custom user roles, Project Administrators can create a tailored permissions structure that aligns with their organisation's unique requirements, workflows, and team dynamics. This flexibility ensures that every user operates within a secure and appropriate environment, contributing to the project's success.

## **Get Started with Blue**

Now that you have a solid understanding of Blue's core concepts and capabilities, we invite you to experience the power of our platform firsthand. Blue offers a 7-day free trial, allowing you to explore all the features without any commitment or credit card required.

During the trial period, you can set up your organization, create projects, customize workflows, and see how Blue can transform how you manage your work. Our team is also available to provide any assistance you may need - reach out to us at [**help@blue.cc**](mailto:help@blue.cc) or [**book a support call.**](https://app.onecal.io/b/blue/support)

When you're ready to continue using Blue, you can choose from our flexible monthly or annual subscription plans,  at just $7 per user per month. We're confident that once you experience the efficiency and insights Blue can bring to your operations, it will become an indispensable tool for your organization.

[**Sign up for a free trial.** ](https://app.blue.cc)

\


\


\


\


&#x20;
