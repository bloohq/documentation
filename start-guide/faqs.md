# FAQs

<details>

<summary><strong>What are the key benefits of using Blue?</strong></summary>

Blue offers robust features to help teams organize work and collaborate effectively. Key benefits include customizable workflows, intelligent automation, real-time communication, and powerful analytics.

</details>

<details>

<summary><strong>How much does Blue cost?</strong></summary>



Blue has one plan with all the features.&#x20;

* Monthly plan: **$7/month/user.**&#x20;
* Yearly plan: **$70/year/user (so you get two months free)**

These users roles are paid:

* [Project Administrator](../user-management/roles/project-administrator.md)
* [Team Member](../user-management/roles/team-member.md)
* [Custom User Roles](../user-management/roles/custom-user-roles.md)



And these user roles are free:&#x20;

* [Clients](../user-management/roles/client.md)
* [Comment Only](../user-management/roles/comment-only.md)
* [View Only](../user-management/roles/view-only.md)

</details>

<details>

<summary><strong>How does the Blue free trial work?</strong></summary>

The free trial provides full access to all Blue features for 7 days. No credit card is required to start. If you wish to continue, you can upgrade to our paid plan at the end of the free trial. You can always email us at [help@blue.cc](mailto:help@blue.cc) if you need to extend your trial.

</details>

<details>

<summary><strong>What integrations does Blue support?</strong></summary>

Blue has 5,000+ integrations via [Pabbly Connect ](../integrations/pabbly-connect.md)and [Zapier](../integrations/zapier.md). You can find a [list of our integrations here.](https://www.blue.cc/platform/integrations)

We also have a [full API for custom integrations](../integrations/api.md) and granular [Webhooks. ](../integrations/webhooks.md)

</details>

<details>

<summary><strong>Can I import my existing data into Blue?</strong></summary>

Yes, Blue supports [bulk data import from CSV files](../integrations/csv-import.md). This allows you to migrate up to 200,000 records simultaneously and is perfect for migration from manual Excel workflows or other work management systems.  You can also [book a support call](https://app.onecal.io/b/blue/support) if you would like help with importing your data.

</details>

<details>

<summary><strong>Does Blue have Mobile Apps?</strong></summary>

Yes, [native iOS and Android apps ](download-apps.md)are available to access Blue on the go. Core functionality like tasks, comments, and calendars sync across devices.

</details>

<details>

<summary><strong>How do permissions work in Blue?</strong></summary>

Permissions are set at the project level, so users can have different access levels across projects based on their roles. [Blue has a powerful custom user roles system](../user-management/roles/custom-user-roles.md) that allows you to define the precise access for each user group in each project.&#x20;

</details>

<details>

<summary><strong>Can I customize the interface and workflows in Blue?</strong></summary>

Yes, you can completely customize fields, views, automation rules, forms, and more to match your work requirements. You can choose the colour of the interface and upload your company logo. We also allow you to have a custom domain.&#x20;

</details>

<details>

<summary><strong>Is Blue available in other languages besides English?</strong></summary>

Yes, Blue is available in 24 languages.

Arabic (ar), Bengali (bn), Chinese (zh), Czech (cz), English (en), French (fr), Georgian (ka), German (de), Hindi (hi), Hungarian (hu), Indonesian (id), Italian (it), Japanese (ja), Khmer (km), Korean (ko), Latvian (lv), Marathi (mr), Portuguese (pt), Romanian (ro), Russian (ru), Spanish (es), Swedish (se), Thai (th), and Vietnamese (vi).&#x20;

</details>

<details>

<summary><strong>What support options are available?</strong></summary>

* **Email support:** Simply email us at [**help@blue.cc**](mailto:help@blue.cc)
* **Live chat:** We offer live chat support Mondays to Fridays.
* **Phone Support:** You can book a [free 30-minute support call ](https://app.onecal.io/b/blue/support)anytime.&#x20;
* **Online Documentation:** This is what you're reading right now!&#x20;

</details>

{% hint style="info" %}
**Need more help?**

We offer paid support packages for large-scale imports, full-team onboarding, and regular training. Email us at [**help@blue.cc**](mailto:help@blue.cc) to learn more.
{% endhint %}

<details>

<summary><strong>Does Blue integrate with my SSO provider?</strong></summary>

Not yet. Blue only support email login with magic links — we don't use any passwords for security reasons.&#x20;

</details>

<details>

<summary><strong>Where do I submit feature requests for Blue?</strong></summary>

We'd love to hear your feature ideas! You can email us directly at [**help@blue.cc**](mailto:help@blue.cc) or [join our community forums. ](https://ask.blue.cc)

</details>

<details>

<summary><strong>What analytics does Blue provide?</strong></summary>

Blue offers real-time [dashboards](../dashboards/) and cross-project reporting to analyze progress, productivity, and trends.

</details>

<details>

<summary><strong>Can I export my data from Blue?</strong></summary>

Yes, you can export all your Blue data anytime via the [API](../integrations/api.md) or [bulk CSV Export.](../integrations/csv-export.md) You own your data, and it is always accessible.

</details>

<details>

<summary><strong>What file storage limits exist in Blue?</strong></summary>

Blue offers unlimited storage for all your project files and documents. [Fair use policies apply.](https://www.blue.cc/legal/terms)

</details>







