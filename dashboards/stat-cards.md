# Stat Cards

Stat cards display a real-time calculated value from one or more projects. This is great for keeping an eye on key statistics.

<figure><img src="../../.gitbook/assets/stats cards.png" alt=""><figcaption><p>An example of stat cards related to sales</p></figcaption></figure>

## Creating Stat Cards

### Set Title

When creating a stat card, the first thing to do is set a title. Titling your stat cards is an essential step for clarity and usability. The title should encapsulate the critical metric or insight the stat card represents, such as "Monthly Sales," "Open Issues," or "Weekly Active Users."&#x20;

<figure><img src="../../.gitbook/assets/stat card title.png" alt=""><figcaption><p>A well-defined title guides users at a glance, allowing them to quickly understand the data context and relevance.</p></figcaption></figure>

The title you set will then be shown on the top of the stat cards:

<figure><img src="../../.gitbook/assets/titles stats cards.png" alt=""><figcaption></figcaption></figure>

### Create Values

You can create multiple values per stat card, each representing a dimension or metric that is important to your project. This multi-value feature allows for a comprehensive dashboard that reflects project performance in real-time.

Values on stat cards can be derived from a specific project, enabling you to do the following:

* **Record Counts**: Calculate the total number of records within a project, giving you a quick overview of the volume of tasks or entries.
* **Custom Field Calculations**: Perform calculations based on data contained within custom fields. For instance, you might sum up values from a "Project Price" field to gauge overall budgetary figures.

To ensure that the values on your stat cards are as relevant and targeted as possible, Blue offers robust filtering options — [any filter ](../views/filters.md)in any project view is available for use!&#x20;

### Add Logic

In Blue, the logic functionality within stat cards is the engine for calculating and presenting data-driven insights. Logic allows you to harness the power of data through customized formulas and operations, transforming raw project data into actionable information.

Begin by referencing specific values within your project by typing '**{**'. This action will bring up a list of available values that you can include in your logic formula.

You can enhance your logic with various functions by typing '='. Blue provides a suite of functions such as:

* **AVERAGE**: Calculate the mean of a set of numbers.
* **AVERAGEA**: Determine the average, treating text as zero.
* **COUNT**: Count the number of entries in a number field.
* **COUNTA**: Count the number of non-empty values.
* **MAX**: Find the maximum value in a set.
* **MIN**: Identify the minimum value in a set.
* **MINUS**: Subtract one value from another.
* **MULTIPLY**: Multiply two values.
* **SUM**: Add up a series of numbers.

<figure><img src="../../.gitbook/assets/functions blue.png" alt=""><figcaption><p>Easily write custom logic using functions</p></figcaption></figure>

These functions are essential tools in data analysis, enabling you to summarize and analyze project metrics effectively. Whether you're looking to pinpoint maximum sales, calculate average task durations, or sum up budget allocations, the logic in Blue makes it straightforward. &#x20;

**By simplifying complex calculations and presenting them clearly, Blue transforms project data into actionable insights, aiding in strategic decision-making.**



### Configure Display

The final step to creating a stats card is deciding how to display the data.&#x20;

Stat cards come with three display options:

1. **Number:** Opt for a straightforward numeric display, with the added functionality to set the precision level. This means you can dictate how many decimal places are shown, allowing for precision that matches the detail level required for your data analysis.
2. **Currency:** Select this option to represent values in monetary terms. It is ideal for financial metrics such as budget tracking, revenue, costs, or other financial statistics. The currency format will apply appropriate local formatting and currency symbols.
3. **Percentage:** Use this format to express values as percentages, perfect for showing completion rates, growth metrics, or other proportional data. This option automatically converts the calculated value into a percentage format, providing an instant understanding of ratios and shares.

<figure><img src="../../.gitbook/assets/stat card display options.png" alt=""><figcaption><p>The display options for stat cards</p></figcaption></figure>

