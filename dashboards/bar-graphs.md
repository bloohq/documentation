# Bar Graphs

Bar graphs provide a helpful way to analyze and compare values across categories of data visually. The height of each bar represents the magnitude of values within that category.

### Creating a Bar Graph

#### Set Title

Start by setting a descriptive title for your bar graph. Clearly indicate what data is being charted.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.54.49@2x.png" alt=""><figcaption></figcaption></figure>

### Choose Bar Creation Type

Decide between manual bar creation or automatic:

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.55.30@2x.png" alt=""><figcaption></figcaption></figure>

### **Manual**

Customize each bar fully, including its data source, label, color and logic. Allows precision but takes more effort.

#### Select Category Field

Next, choose the field that contains the categories to represent as bars. Common options include region, product line, priority status etc. This field determines the number of bars.

#### Pick Value Field

Then, select the numeric field to drive bar heights. Typical choices include revenue, budget allocation, count of records etc. Values from this field will dictate bar length.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.56.23@2x.png" alt=""><figcaption></figcaption></figure>

#### Filter (Optional)

Apply filters to narrow analysis to a subset of data if desired. Any existing project view filters can be added.

#### Customize Visuals and Analyze

Tailor visual settings like colors, intervals and labels. Then derive insights by comparing performance across categories.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.59.58@2x.png" alt=""><figcaption></figcaption></figure>

### **Automatic**

A streamlined process where title, categories and values are sufficient to generate the full bar graph with just a few clicks.&#x20;

You can also adjust automatic bars based on your needs and add any filter necessary.&#x20;

Bar charts transform project data into intuitive visual analyses focused on comparing metric values categorically in Blue.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 14.01.28@2x.png" alt=""><figcaption></figcaption></figure>

