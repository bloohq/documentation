# Pie Charts

Pie charts in Blue provide an intuitive circular graph to visualize proportional data across categorized records. These graphs illustrate relative magnitudes and relationships within datasets using colored, segmented slices.\


## Creating Pie chart&#x20;

### Set Title

When creating a pie chart, the first thing to do is set a title to your pie then pulling data for your slices value. Individual slices can contain stacked subgroups, enabling multi-layer analysis in one chart. For instance, stack regional and channel divisions within each product slice.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.17.19@2x.png" alt=""><figcaption></figcaption></figure>

### Create Values or Multiple Values.

You can create multiple slices per chart that will build up your circle. Each slice must pull data from other projects' custom fields to make sense. If you're selecting data from [custom field "Currency" ](../custom-fields/currency.md)the rest of the slices for this should be pulling out from the same data. This allows you to compare the same data across projects in one pie chart.

Values on Pie Chart can be derived from a specific project, enabling you to do the following:

* Analyze sales composition across regions, channels, markets etc.
* Breakdown task or issue distribution by category, priority, assignee etc.
* Understand workflow stage progression and bottlenecks
* Benchmark revenue or profit ratios across products, campaigns, sectors etc.
* Track diversity and inclusion metrics across teams and levels

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.19.03@2x.png" alt=""><figcaption></figcaption></figure>

To ensure that the values on your Pie Chart are as relevant and targeted as possible, Blue offers robust filtering options — [any filter ](../views/filters.md)in any project view is available for use!&#x20;

### Add Logic

In Blue, the logic functionality within stat cards is the engine for calculating and presenting data-driven insights. Logic allows you to harness the power of data through customized formulas and operations, transforming raw project data into actionable information.

Begin by referencing specific values within your project by typing '**{**'. This action will bring up a list of available values that you can include in your logic formula.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.19.03@2x.png" alt=""><figcaption></figcaption></figure>

You can enhance your logic with various functions by typing '='. Blue provides a suite of functions such as:

* **AVERAGE**: Calculate the mean of a set of numbers.
* **AVERAGEA**: Determine the average, treating text as zero.
* **COUNT**: Count the number of entries in a number field.
* **COUNTA**: Count the number of non-empty values.
* **MAX**: Find the maximum value in a set.
* **MIN**: Identify the minimum value in a set.
* **MINUS**: Subtract one value from another.
* **MULTIPLY**: Multiply two values.
* **SUM**: Add up a series of numbers.

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.21.00@2x.png" alt=""><figcaption></figcaption></figure>



{% hint style="success" %}
You can choose the colour for each slice of the pie chart.&#x20;
{% endhint %}

These functions are essential tools in data analysis, enabling you to summarize and analyze project metrics effectively. Whether you're looking to pinpoint maximum sales, calculate average task durations, or sum up budget allocations, the logic in Blue makes it straightforward. &#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 13.21.42@2x.png" alt=""><figcaption></figcaption></figure>

Pie charts transform project data into actionable and visually intuitive insights about proportional contributions, progress, and differences across categorized records in Blue.

{% hint style="info" %}
Bar charts have automatic creation, which will soon be coming to pie charts.&#x20;
{% endhint %}
