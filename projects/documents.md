# Documents

Documents in Blue provide flexible spaces for planning, collaboration, and knowledge management. Docs function as collaborative rich text documents where teams can outline ideas, create specifications, manage meeting notes, build internal wikis, and more.

With real-time editing abilities, simple formatting tools, and tight integration with other Blue features, documents enable intuitive content creation and uninterrupted teamwork.

<figure><img src="../../.gitbook/assets/docs list.png" alt=""><figcaption><p>List of documents within a project</p></figcaption></figure>

### Creating Documents

To create a new document:

1. Click on the "Docs" tab
2. Press the "+ Create Docs" button
3. Enter a title for your document
4. Press enter to generate a blank document

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/new doc.png" alt=""><figcaption><p>Creating a new document in Blue</p></figcaption></figure>

</div>

### Editing Documents

The rich text editor empowers effortless formatting and content creation.&#x20;

You can apply formatting like bold and italics through keyboard shortcuts by using the /slash command - `/bold`, `/italic`&#x20;

<figure><img src="../../.gitbook/assets/docs new slash command.png" alt=""><figcaption><p>Type "/" to open commands in the text editor</p></figcaption></figure>

Like anywhere in Blue where you can drop comments, you can @mention team members to notify them or jump to their cursor location.

<figure><img src="../../.gitbook/assets/mentions docs.png" alt=""><figcaption><p>Easily @mention other users within documents and see who is currently viewing the document.</p></figcaption></figure>

You can use "@@" to mention a specific record, and then users can click on that link to open the record within the document view. This is extremely useful because it avoids context-switching for anyone reading your document.

<figure><img src="../../.gitbook/assets/mention search records in docs.png" alt=""><figcaption><p>Use "@@" to search for any records within the project</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/WxgrjK8w.gif" alt=""><figcaption><p>Click on any mentioned record to instantly open it</p></figcaption></figure>



* Embed images via drag-and-drop or the `/attachment` command
* Headers, checklists, highlights, and more



### Collaborative Editing

Documents facilitate seamless co-editing and teamwork:

* View the updates of other editors in real-time
* Click on user profile icons on the right sidebar to instantly jump to their position within the docs
* Live preview edits as they are typed
* Changes are automatically saved - no manual saving required

### Use Cases

#### Planning Documents

The document editor is ideal for crafting strategic plans aligned to project objectives and milestones. Documents can outline scope, requirements, resources, budgets, timelines and more. Real-time collaboration allows planners to co-author while retaining version histories.

#### Design Specifications

For creative projects, documents enable teams to align on detailed specifications, asset requirements, content plans, style guides and other design elements. Documents centralize this information for easy reference, while change tracking captures iterative development.

#### Client Briefs

Client needs and expectations can be consolidated in shareable documents. Briefs clarify project parameters, success metrics, target audiences, messaging and required deliverables. Documents keep stakeholders aligned while retaining an accessible audit trail of agreed objectives.

#### Internal Wiki Pages

Wikis and internal documentation facilitate access to institutional knowledge. Documents reduce duplication by centralizing process documentation, training materials, policies, instructions and reference information.

#### Meeting Agendas and Notes

Prep materials and records of discussions help keep meetings structured and outcomes actionable. Agendas outline talking points, while notes capture attendance, decisions and next steps in an easily accessible system of record.

