# Introduction

## How Projects Work

Projects in Blue form the fundamental framework for organizing users and data, effectively segmenting different work areas and collaboration within the platform. Each project in Blue can vary significantly in nature and duration, accommodating various organizational needs.

Each project in Blue is entirely self-contained. Only users that are specifically invited into the project can see the data and collaborate.  You can have an unlimited number of projects within a Blue account.&#x20;

There are two ways of thinking about projects in Blue:

1. **Projects:** The typical project has clear start and end dates, often focused on specific goals or events. Examples include an event launch, a branding overhaul, or a website design project. Such projects are typically goal-oriented, with defined objectives and timelines. They will often be [archived](archiving-projects.md) once the project is complete.
2. **Processes:** On the other hand, Blue also adeptly handles long-term, continuous processes. These can include activities like customer service operations, ticket-based helpdesks, sales tracking, recruitment processes, and more. These projects are characterized by their ongoing nature, lacking a definitive end date but requiring consistent management and organization.

## Project Settings

[Project Administrators](../user-management/roles/project-administrator.md) will see a settings icon next to the project name in the top bar.

<figure><img src="../.gitbook/assets/project settings.png" alt=""><figcaption><p>Only Project Administrators will see this icon</p></figcaption></figure>

There are multiple settings available:

* **Edit project settings:** You can change the project name, write a project description, add a project photo or icon, and also change the name of [Records](../records/introduction.md) to something else (i.e. Task, Sale)
* **Set Icon & Color:** You can choose an icon and colour for the project; this will be shown in the sidebar to all users who are in the project.&#x20;
* [**Convert to Template:** ](templates.md)This allows the project to be converted into a reusable template.&#x20;
* [**Copy**](copying-projects.md)**:** Allows you to duplicate the project
* [**Archive**](archiving-projects.md)**:** Archive the project for future reference, turning it into a read-only project.&#x20;
* [**Import**](../integrations/csv-import.md) **&** [**Export**](../integrations/csv-export.md)**:** Enables import and exporting of records from the project&#x20;
* **Delete:** Enables the permanent deletion of the project.&#x20;



