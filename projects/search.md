# Search

Search can deliver lightning-fast, search-as-you-type results across tens of millions of items within milliseconds, making it easier than ever to find the information you need.

### Accessing Search&#x20;

The new search bar is placed in the top navigation bar for easy access. However, we've also added keyboard shortcuts to make your search experience even more faster:

1. **On Mac**: Press **cmd+K**
2. **On Windows**: Press **ctrl+K**

These shortcuts allow you to quickly pull up the search function from anywhere in Blue, without having to move your cursor.

<figure><img src="../.gitbook/assets/1715073384222_CleanShot 2024-05-07 at 16.15.47@2x_01HX95DZXKMD71NFD8JEZW5704.png" alt=""><figcaption></figcaption></figure>

### Expanded Search Experience

For a more immersive search experience, you can press the **Tab** key after using the keyboard shortcuts. This will expand the search into a full-screen mode, giving you more space to view.&#x20;

### Comprehensive Search

The Search Function can search through:

* Records
* Comments within records
* Documents
* Files

This means that whether you're looking for a specific client record, a key decision made in a project comment, an important document, or a crucial file,  Search can find it for you in seconds.

### Quick Record Navigation

One of the standout features of Search is its ability to act as a quick switch for moving between records. By typing in the name or any part of a record, you can instantly jump to that record, regardless of which project or list it's in.&#x20;

