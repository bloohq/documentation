# Records

Records are the fundamental building blocks that form the basis of the system's functionality and data organization. Whether it's tracking customer interactions, counting sales records, managing project milestones, or cataloguing inventory items, records in Blue can be adapted to encapsulate virtually any information you require. This flexibility ensures that Blue can be tailored to fit diverse business processes and operational needs, making them a powerful tool for organizing and managing critical data.

## Renaming Records&#x20;

Records are highly versatile and can be renamed to suit your specific needs. You can easily rename your records by going to Project Settings and changing them to whatever you prefer, such as your tasks, assignments, or any other name. This change will apply across the project.

Some examples of record names you can use:

* Tasks / Task
* Assignments / Assignment
* Steps / Step
* Issues / Issue
* Tickets / Ticket
* Leads / Lead
* Deals / Deal
* Cases / Case
* Opportunities / Opportunity
* Clients / Client
* Products / Product
* Orders / Order

Make sure to add your desired record name's singular and plural forms. This way, the system will recognize and display the appropriate term based on the context.

By customizing record names, you can align the terminology with your industry, business processes, or personal preferences, making the platform more intuitive and user-friendly for your team.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-13 at 14.07.11@2x.png" alt=""><figcaption></figcaption></figure>

Records can represent a wide range of entities, such as:

* Customer interactions
* Sales records
* Project milestones
* Inventory items

This flexibility ensures that Blue can be tailored to fit diverse business processes and operational needs, making them a powerful tool for organizing and managing critical data.

<div data-full-width="true">

<figure><img src="../.gitbook/assets/blue record.png" alt=""><figcaption><p>An example of a record in Blue</p></figcaption></figure>

</div>

We cover records in detail in their own section:

{% content-ref url="../records/" %}
[records](../records/)
{% endcontent-ref %}
