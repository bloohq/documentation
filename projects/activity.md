# Activity

Activity in Blue provides transparency by recording all actions across projects. There are three levels of activity tracking:

1. **Organisational Activity** - Logs everything happening across all projects company-wide.
2. **Project Activity** - Logs actions within a specific project.
3. **Record Activity —** Provides a granular changelog of the record.&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/project and company activity.png" alt=""><figcaption><p>Company and Project Activity </p></figcaption></figure>

</div>



Activity delivers a complete audit trail of activities across the organization. It enables understanding of what is happening company-wide without constant status meetings.

#### Timeline Filtering

<figure><img src="../.gitbook/assets/RC9p1GnU.gif" alt=""><figcaption><p>Example of filtering timeline by Assignee</p></figcaption></figure>

Activities can be filtered by:

**User** filter, which allows you to view activities related to one or more selected users. Here's what the User filter encompasses:

This filter displays activities associated with the selected user(s), including:

* Activities performed on records directly assigned to the user(s).
* New comments made by the user(s).
* New records created by the user(s).
* Changes made to custom fields by the user(s).
* Any other modifications or updates made by the user(s).
* The user(s) has been mentioned&#x20;

By applying the User filter, you can quickly isolate and review the actions and changes made by specific individuals, making it easier to track their contributions and activities within the organization.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-03-13 at 11.11.53@2x.png" alt="" width="563"><figcaption></figcaption></figure>

**Tag**  This filter displays activities related to records that have been tagged with one or more specific tags.&#x20;

By selecting certain tags, you can view a timeline of activities performed on records associated with those tags. This filter is particularly useful when you want to focus on activities pertaining to specific projects, categories, or areas of interest identified by the tags.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-14 at 17.12.24@2x (1).png" alt=""><figcaption></figcaption></figure>

**Date** This filter allows you to specify a date range, and it displays activities that occurred within that chosen time period.&#x20;

By setting a start and end date, you can narrow down the timeline to focus on activities that took place during a specific period, such as a particular week, month, or any custom date range.&#x20;

This filter is useful for reviewing historical activities, tracking progress over a defined timeline, or analyzing activities within a specific timeframe.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-13 at 11.20.30@2x.png" alt=""><figcaption></figcaption></figure>



#### Unread Highlighting

Unread activities are highlighted in blue, enabling easy identification of new actions. Clicking on them displays details of the activity.

As an uneditable log, Company Activity provides a reliable record of all user actions. It facilitates awareness and transparency between all teams and projects.

####
