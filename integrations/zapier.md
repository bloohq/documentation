# Zapier

Zapier is an integration platform that helps connect different web apps and automate workflows.

**Use Cases**

Here are some of the most popular ways Blue is integrated with other apps via Zapier:

_**Sync Blue Records to Google Sheets**_

As Blue records are created or updated, automatically append them as new rows in a Google Sheet for centralized reporting.

_**Send Order Confirmation from Blue records**_&#x20;

When you select certain custom field on Blue, automatically send out email to confirm order with your customer.&#x20;

_**Add Blue Comments to Slack**_

Get real-time Blue notifications posted to the right Slack channels so your team is aware of important record updates.

### Key Benefits

**Automation Across Tools**: Zaps created in Zapier can automatically send and receive data between Blue and thousands of other apps. This eliminates manual work moving data between systems.

**Real-Time Integrations**: Using Blue's webhook triggers, Zaps react instantly to events in Blue like a new record, comment added, status change etc. This enables real-time synchronization and process automation.

**Easy to Build and Maintain**: Zaps provide a user-friendly, no-code way to integrate tools. No development work needed. And they keep working with no ongoing maintenance.

### **Set Up Webhook in Blue**

{% embed url="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FpM2FqesGO6kVeA9G2fUc%2Fuploads%2FLhP6Dfwvkgc3hBI8wHd4%2Fwebhook.mp4?alt=media&token=7c815e65-8bb2-4d49-bcf2-c5f65279697d" %}

1. Log in to your Blue account under profile setting
2. Go to the profile settings and click "Webhooks"
3. Click "Add Webhook"
4. Give the webhook a name and enter the Zapier webhook URL
5. Choose which events should trigger the webhook
6. Click "Create Webhook"

### **Create Zap in Zapier**

1. Log in to your Zapier account
2. Click "Make a Zap"
3. For the first step, search for and select the "Webhooks by Zapier" trigger app
4. Configure it to accept payloads from your Blue webhook URL
5. For the second step, search and select the destination app to send data to
6. Map the JSON payload fields to the input fields of the destination app
7. Turn on the Zap

Now when the specified events occur in Blue, the webhook payload will be sent to Zapier which automatically passes the data to your integrated app per the Zap settings.
