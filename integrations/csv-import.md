# CSV Import

The import tool in Blue allows you to migrate and integrate data from other systems into your Blue projects. It can handle up to 200,000 items at a time.&#x20;

Blue only allows to import of CSV (Comma Separated Value) files. If you have an Excel file, [you can convert it to a CSV file at the following link.](https://cloudconvert.com/xls-to-csv)

## Importing CSV

Importing CSV files in Blue is a straightforward process. Each column in your CSV file corresponds to a default or [custom field](../custom-fields/) in Blue.&#x20;

To start, you must be the Project Administrator of the project where you want to import data. Click on the project settings icon (gear icon) and select "Import"

<figure><img src="../.gitbook/assets/import.png" alt=""><figcaption><p>The Import Feature is available under Project Settings for Project Administrators only</p></figcaption></figure>

In the screenshot above, you can also see another option called "Download CSV Template". This is extremely useful if you have _already_ created your [custom fields ](../custom-fields/)in your Blue project because you can download a CSV file that includes columns for all of your custom fields! This is great when you want to ensure that you have the precise structure for your CSV before importing.&#x20;

Next, simply drag and drop your CSV file into Blue:

<figure><img src="../.gitbook/assets/import drag and drop.png" alt=""><figcaption><p>Drag and drop to upload your data</p></figcaption></figure>

Depending on your file size, this may take a few seconds for you to upload and for Blue to analyse. Once Blue has analysed the data, you will see a preview of the first ten rows of your data:&#x20;

<figure><img src="../.gitbook/assets/import preview.png" alt=""><figcaption></figcaption></figure>

You can then map each column in your CSV to a field in Blue.

<figure><img src="../.gitbook/assets/import mapping.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
**Feeling Stuck? Get help!**

We understand that no matter how easy we make it, importing data can sometimes be tedious, especially if it's your first time.  If you want our team to assist you in uploading your data and matching your fields, simply email us at [**help@blue.cc** ](mailto:help@blue.cc)
{% endhint %}



<figure><img src="../.gitbook/assets/MhTJ7QKt.gif" alt=""><figcaption><p>Easily map column to fields</p></figcaption></figure>

Once the mapping is complete, click "Import" and your data will be imported. The typical import speed in Blue is 10,000 to 15,000 records per minute.&#x20;

You can also watch this video guide for a step-by-step tutorial on importing and exporting CSV files:\


{% embed url="https://www.youtube.com/watch?v=mbDCnhxVdso" %}

### Key Benefits

* **Migration made easy:** Quickly transfer data from other tools like Excel, Airtable, Asana, etc. into Blue.
* **Integration capabilities**: Ongoing imports from other data sources like CRMs to always keep project data up to date
* **Customization:** Map imported data to custom fields for complete flexibility, matching your structure needs
* **Seamless adoption:** Import helps new teams get set up quickly by moving existing data into Blue to build on

The import function streamlines data migration and integration into Blue, saving manual work while enabling customization. It's a valuable onboarding tool for new users that facilitates regular data imports for seamless app ecosystem interoperability.
