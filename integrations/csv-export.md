# CSV Export

The export tool allows you to download data from your Blue projects into a CSV file for storage and analysis outside Blue.

## **To export project data:**

1. In the project you want to export, click the settings icon (gear)
2. Select "Export"
3. Click "Export" to trigger the export process on the Blue Servers.
4. Once complete, you will receive an email with a link to download the exported CSV file

{% hint style="info" %}
**How long does it take to receive the email with the data?**

Typically, this will arrive within a few seconds, but it can depend on how many other export processes are running from other customers and the size of your data. For datasets that are over 100,000 items, this may take a few minutes.&#x20;
{% endhint %}

<figure><img src="../.gitbook/assets/export.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Note that the export feature only exports your records data. Files within the [Files tab ](../projects/file-management.md)have to be downloaded manually in bulk.&#x20;
{% endhint %}

### Export with Active Filters

If you have already applied filters to your project, you can easily export only the filtered records by using the "Export with Active Filters" option. This way, you won't have to worry about manually filtering the exported data, as it will automatically include only the records that match your current filter settings.

Here's how you can export with active filters:

1. Apply the filters to your project by using the filter options available, such as assignees, tags, or advanced filters.
2. Once you have the filtered view you want to export, go to the project setting and click on  Import & Export.&#x20;
3. In the export options menu, select "Export with Active Filters."
4. Once your export ready, you will receive the data in your email.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-04 at 14.20.09@2x.png" alt=""><figcaption></figcaption></figure>



### Key Benefits

* **Data portability:** Download projects out of Blue when needed for backup or external analysis
* [**Trigger via API**](api.md)**:** You can use third-party systems to automatically trigger Blue exports regularly to meet your data backup requirements.&#x20;
* **Stakeholder accessibility**: Share exported CSVs easily with clients, managers and others outside Blue.

Export delivers data accessibility and flexibility to match various potential integration and reporting needs. Combined with [import](csv-import.md), it provides complete control over your Blue data.
