# Calendar Sync

Blue allows you to sync calendar data with external calendars, including:

* Google Calendar
* Outlook&#x20;
* iCal (Apple)

This means that calendar events from Blue can be shown in your personal or work calendar.&#x20;

{% hint style="info" %}
Note that the Calendar Sync works one-way: from Blue to your calendar, not vice versa.&#x20;
{% endhint %}

There are three types of calendars in Blue that can be synced in Blue

1. **Personal Calendar:** All records that have a date that are assigned to you across all your projects
2. **Project Calendars:** All records that have a date in a specific project.&#x20;
3. **Company Calendar:** All records with a date in all projects you belong to.

<figure><img src="../.gitbook/assets/calendars.png" alt=""><figcaption><p>A visual guide of where you can access the three types of caledar syncs in Blue. </p></figcaption></figure>

At the top right of any calendar, you will find a "Sync Calendar" button:

<figure><img src="../.gitbook/assets/sync calendar.png" alt=""><figcaption><p>The Sync Calendar Button</p></figcaption></figure>

When you press the Sync Calendar button, you will find a link that you can click on to copy.

<figure><img src="../.gitbook/assets/blue calendar sync link.png" alt=""><figcaption><p>The Blue Calendar Sync URL</p></figcaption></figure>

Add a new calendar to your personal or work calendar. Then, add a new calendar to your personal or work calendar. Choose the "From URL" option and paste the Blue sync URL.

You can find specific instructions for major platforms:

* [Google Calendar Instructions](https://support.google.com/calendar/answer/37100?hl=en\&co=GENIE.Platform%3DDesktop)
* [Outlook Instructions](https://support.microsoft.com/en-au/office/import-or-subscribe-to-a-calendar-in-outlook-com-or-outlook-on-the-web-cff1429c-5af6-41ec-a5b4-74f2c278e98c)
* [iCal (Apple) Instructions ](https://support.apple.com/en-gb/guide/calendar/icl1022/mac)

The Blue calendar will now show as an available calendar to toggle on. Turn on the toggle to view this calendar within your calendar platform's interface. Now, whenever you add, edit or remove events in Blue's calendar, these changes will automatically sync to your connected calendar.

Syncing to external calendars keeps your Blue events integrated directly into your daily workflow and schedule across devices. This ensures you have complete visibility into upcoming work without context switching.
