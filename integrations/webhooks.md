# Webhooks

A webhook is an automated message sent from one application to another when something happens. For example, when a new record is created in Blue, a webhook can be configured to send data about that record to another system.

Blue webhooks are triggered instantly, within milliseconds of the event occurring. This real-time nature makes them invaluable for integrating Blue with other business systems.

{% embed url="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FpM2FqesGO6kVeA9G2fUc%2Fuploads%2FLhP6Dfwvkgc3hBI8wHd4%2Fwebhook.mp4?alt=media&token=7c815e65-8bb2-4d49-bcf2-c5f65279697d" %}

**Webhooks**

* Real-time data syncing across systems
* Ability to respond and take action on Blue events
* No need to repeatedly poll for changes; pushed instantly
* Create custom integrations and workflows

**Unprecedented Control**

Blue webhooks give you fine-grained control over:

* Exactly which events should trigger the webhook and from which project
* Filters to narrow down the scope of records
* Signature validation for enhanced security

**Creating a Webhook**

In Blue, Project Administrators can create webhooks under project settings. You specify:

* Webhook name
* Payload URL to send data
* Optional secret for signature

**Triggers**

Webhooks can be triggered by a wide variety of Blue events, including:

* Record created/updated/deleted
* Comment added/edited
* Assignee changed
* Checklist status changed
* Custom field value changed
* And many more

**Webhook Payload**

When triggered, Blue sends a JSON payload containing all details of the event to your specified URL.

Here is an example payload:

```json
{
  "event": "RECORD_CREATED",
  "record": {
    "id": "1234",
    "title": "New record"
    ...
  },
  "company": {
    "id": "5678"  
  },
  "project": {
    "id": "91011"
  },
  "user": {
    "id": "1213" 
  }
}
```

**Signature**

To validate payloads are genuinely from Blue, an optional signature can be checked. Similar to OAuth, the signature is created by hashing the payload contents with your chosen secret key.

**Sample Validation in Node.js**

```javascript
const crypto = require('crypto');
const key = 'secret'; 
const body = {...webhookPayload} 
const hash = crypto.createHmac('sha256', key).update(body);
const signature = hash.digest('hex');
```

**Additional Integrations**

Beyond custom webhooks, Blue also offers integrations with tools like [Zapier](zapier.md) and [Pabbly](pabbly-connect.md) Connect to simplify connecting Blue data with hundreds of popular apps.

<details>

<summary>Example Webhook Response</summary>

{% code fullWidth="true" %}
```json
{

  "event": "TODO_CREATED",

  "webhook": {

    "id": "clb4qaopy0000vt24mlp28v03",

    "uid": "edb5b5d51dc9461a952160dbe72153b4",

    "name": "Testing webhook",

    "url": "https://httpdump.app/dumps/7cc66eda-8e83-4039-ba49-4c36db14d508",

    "secret": "",

    "status": "HEALTHY",

    "enabled": true,

    "metadata": {

      "events": [

        "TODO_CREATED",

        "TODO_DELETED",

        "TODO_MOVED",

        "TODO_NAME_CHANGED",

        "TODO_CHECKLIST_CREATED",

        "TODO_CHECKLIST_NAME_CHANGED",

        "TODO_CHECKLIST_DELETED",

        "TODO_CHECKLIST_ITEM_CREATED",

        "TODO_LIST_CREATED",

        "TODO_LIST_DELETED",

        "TODO_LIST_NAME_CHANGED",

        "COMMENT_CREATED",

        "COMMENT_DELETED",

        "COMMENT_UPDATED"

      ],

      "projectIds": [

        "clakt90vu000bvt64hhffqleq",

        "claulnu1y0000vtrcjurjgds4",

        "clb4gay0y000qvtzs8smaays8"

      ]

    },

    "createdById": "claksl2ye0000vtas0h0ln71e",

    "createdAt": "2022-12-01T07:01:25.014Z",

    "updatedAt": "2022-12-01T11:23:00.661Z"

  },

  "currentValue": {

    "id": "clb4zna6x0000vt1swuwso25f",

    "uid": "11021cb836f34732b31e5ebd5dd5eb27",

    "title": "GGGG",

    "html": null,

    "text": null,

    "position": "32767.5",

    "archived": false,

    "done": false,

    "startedAt": null,

    "duedAt": null,

    "timezone": null,

    "createdAt": "2022-12-01T11:23:09.000Z",

    "updatedAt": "2022-12-01T11:23:09.241Z",

    "createdById": "claksl2ye0000vtas0h0ln71e",

    "todoListId": "clakt9v5t000kvt64f563l90l",

    "todoCustomFields": [],

    "todoUsers": [],

    "todoTags": [],

    "todoList": {

      "id": "clakt9v5t000kvt64f563l90l",

      "uid": "df0e931e6e8f4fc2905ece9da167d2d3",

      "createdAt": "2022-11-17T08:29:22.000Z",

      "updatedAt": "2022-11-17T08:29:22.050Z",

      "title": "ETEST",

      "position": 131070,

      "createdById": "claksl2ye0000vtas0h0ln71e",

      "projectId": "clakt90vu000bvt64hhffqleq",

      "project": {

        "id": "clakt90vu000bvt64hhffqleq",

        "uid": "d7db469a8908496f8e5f5e68030fe4c8",

        "slug": "teest",

        "name": "TEEST",

        "description": "",

        "archived": false,

        "createdAt": "2022-11-17T08:28:43.000Z",

        "updatedAt": "2022-11-17T08:28:42.811Z",

        "isTemplate": false,

        "isOfficialTemplate": false,

        "category": "GENERAL",

        "companyId": "clakt8jud0002vt64205yyj10",

        "imageId": null,

        "hideEmailFromRoles": null,

        "company": {

          "id": "clakt8jud0002vt64205yyj10",

          "uid": "400eb3f8238c486caf40957618be9662",

          "slug": "rogue",

          "name": "Rogue",

          "description": null,

          "createdAt": "2022-11-17T08:28:21.000Z",

          "updatedAt": "2022-11-17T08:28:20.726Z",

          "freeTrialExpiredAt": "2022-12-01T08:28:15.000Z",

          "freeTrialStartedAt": "2022-11-17T08:28:15.000Z",

          "subscribedAt": null,

          "imageId": null,

          "subscriptionPlanId": "clakt8jue0003vt64e40q2prj",

          "freeTrialExtendedById": null,

          "freeTrialExtendedAt": null

        }

      }

    }

  }

}
```
{% endcode %}

</details>

{% hint style="info" %}
**Developer Support**

If you have any questions, please contact us at [help@blue.cc](mailto:help@blue.cc), and we assist. We also offer professional services for integration support.&#x20;
{% endhint %}
