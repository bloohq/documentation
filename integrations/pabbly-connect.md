# Pabbly Connect

[Pabbly Connect](https://payments.pabbly.com/api/affurl/RVYZ07kQyUZ0Z1HUKZ1m/zp2GoChM1JcZ0dQBo?target=nMfdbf0I90K3UdJn) allows you to connect Blue with many popular apps like Google Calendar, Stripe, MailChimp, and more. This enables the automation of workflows by sending data between tools.

There are endless possibilities for integrating Blue using Pabbly Connect. Here are the key steps:

### Access Your Pabbly Dashboard

1. Log into your [Pabbly Dashboard](https://connect.pabbly.com/workflows)
2. Click the "+ Create Workflow" button.
3. Name your workflow.
4. Select Blue as the destination app.

### Connect to Your Blue Account

1. Generate an [API token](api.md) in your Blue profile.
2. In Pabbly, enter your:
   * Token ID
   * Secret Key
   * Company URL (e.g. `https://app.blue.cc/company/mycompany`)
   * Project ID (e.g. `myproject`)&#x20;

{% hint style="danger" %}
_Copy your Secret Key immediately, as it will not be shown again._&#x20;
{% endhint %}

### Create a Test Integration

1. Select a trigger app and event (e.g. new Google Calendar event).
2. Send a test request.
3. Check Blue to ensure the test data was received into your specified project.

