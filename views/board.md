# Board

The Board is the default view in Blue, providing a flexible kanban-style project board. Lists represent different stages or steps in a process, enabling users to map out workflows visually.

The Board View is commonly used for:

* Project management — Mapping out project stages from backlog to completion
* Sales pipelines — Tracking deal stages from lead to closed
* Hiring processes — Visualizing applicant stages from sourced to hired

Play with our interactive walkthrough, or scroll down to learn more.

{% @arcade/embed flowId="0TjfEKEzmNb6iTRqNsh1" url="https://app.arcade.software/share/0TjfEKEzmNb6iTRqNsh1" fullWidth="true" %}

### Lists as Process Steps

Lists on the board represent major steps in a process. Typical configurations involve stages progressing left to right, from early to late stages. List names clearly indicate the specific stage.

Records can be dragged between lists to advance them through different process steps.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-30 at 16.06.48@2x.png" alt=""><figcaption></figcaption></figure>

### Record Cards

Record cards provide key details like title, assignee(s), due date, tags, and description. Cards can be dragged to reorder them within a list.

Users can quickly create records by clicking "+ Add Record" at the top or bottom of any list.&#x20;

{% hint style="info" %}
You can also create multiple records simultaneously by copying and pasting a list of bullet points or a column of cells from Excel or Google Sheets.&#x20;
{% endhint %}

Users can right-click on a record card to access additional options:

* **Open in New Tab** - Opens the full record details in a new browser tab
* **Rename** - Edits the record name
* **Mark as Complete** - Marks the record status as complete
* **Assign** - Assigns the record to a user
* **Tag** - Adds tags to the record
* **Move** - Moves the record to a different list
* **Set Color of Card** - Changes the record card's color
* **Copy** - Duplicates the record
* **Copy URL** - Copies a link to the record
* **Move to Top** - Moves the record to the top of its list
* **Delete** - Deletes the record

<figure><img src="../.gitbook/assets/right click record card.png" alt=""><figcaption><p>Right click on a card to quickly access various options</p></figcaption></figure>

Right-clicking on record cards provides quick access to management options, enabling users to modify records directly from the Board View.

### Bulk Actions

Lists offer bulk actions like tagging all records, assigning records, marking complete/incomplete, or moving records between lists. This enables managing groups of records efficiently. See our dedicated page on lists for more information:&#x20;

{% content-ref url="list.md" %}
[list.md](list.md)
{% endcontent-ref %}

### Filters

Filters allow focusing on subsets of records based on criteria like assignee, tag, due date, etc. Filters apply only to visible record cards, not the entire lists. See our dedicated page on filters for more information:&#x20;

{% content-ref url="filters.md" %}
[filters.md](filters.md)
{% endcontent-ref %}

