# List

The List View provides a streamlined checklist interface for managing records. This linear perspective simplifies progressive tracking through multi-step processes.

### Record on List

* **Checklist Format**: Records are displayed in a convenient list, with checkboxes to mark completion.
* **Infinite Scroll**: Scroll endlessly through lengthy lists across thousands of records.
* **Sorting**: Sort by record name, date, assignee, and more to focus on relevant items.
* **Filtering**: Use [filters](filters.md) to zero in on records by timeline, assignee, tag, and any custom field.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-22 at 15.08.28@2x.png" alt=""><figcaption></figcaption></figure>

**Benefits**

* **Task Management** - Review and check off outstanding todos and action items in second.&#x20;
* **Prioritization** - Focus effort on high-priority records via filtering and sorting.
* **Auditing** - Review sequences of events in chronological order.

**Use Cases**

* Managing sequential checklists - e.g. hiring pipelines, account onboarding.
* Processing lists - e.g. purchase orders, service tickets, tasks.
* Checking off project to-dos across departments and teams.
* Reviewing records in date/time order - e.g. audit trails, histories.

With customizable sorting, filtering, and actions, the List View simplifies record management through sequential tracking.
