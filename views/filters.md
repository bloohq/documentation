# Filters

Filters allow you to narrow down the records shown in a view based on criteria you specify. They are located between the automation and tag buttons at the top right of the board. Filters do not impact what other users see - they only affect your view.

### Basic Filters

In the filters panel, you will see some quick filter categories:

* **Tags:** Select one or more tags to only show records with those tags.
* **Assignees:** Select one or more assignees to only show records assigned to them.
* **Due Date:** Filter by due date ranges like due today, overdue, etc.
* **Records:** Hide Records that are completed, not completed

<figure><img src="../.gitbook/assets/CleanShot 2024-01-22 at 15.10.26@2x.png" alt=""><figcaption></figcaption></figure>

### Advanced Filters

To access advanced filters, navigate to the board view of your project. Click on the filter icon at the top right (it looks like a funnel). This will open the filters panel and  allows you to set filters based on custom fields.

Click the "+" icon to add filter criteria. You can select the custom field, operator (Equals, Not Equals, Contains, etc.) and the value to filter for.

Use the AND/OR buttons to string multiple filter criteria together in one filter.



<figure><img src="../.gitbook/assets/CleanShot 2024-01-22 at 15.12.12@2x.png" alt=""><figcaption></figcaption></figure>

### Stacking Filtered Records

Once you apply filters using the Quick Filters or Advanced Filter, it will instantly filter the records shown on your project view. You can add multiple filters to narrow down the results further.

The currently active filters will be shown at the top, so you know what filters are applied.

### Persistence of Filters&#x20;

Any filters you set will remain active even if you navigate to another view and come back to the board view later. So you do not have to reapply filters each time.
