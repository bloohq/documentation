# Map

The Map View visually plots records containing location data onto an interactive world map. This geographic perspective delivers unique insights and analysis opportunities.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.33.25@2x.png" alt=""><figcaption></figcaption></figure>

## Pin & Track

On the record:&#x20;

* **Pin on the go:** Use the location icon to pin your current location instantly.&#x20;
* **Location Tracking**: The Map leverages the [Location/Map custom field](../custom-fields/location-map.md) to pull in latitude and longitude data associated with records.
* **Visual Identification**: Map pins are color-coded based on the record's primary tag, enabling intuitive recognition.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.28.59@2x.png" alt=""><figcaption></figcaption></figure>

On the Map:&#x20;

* **Detail Popups**: Clicking a pin reveals a popup with the record's name, project, list, tags, and assignees. Click again to open the full record.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.34.31@2x.png" alt=""><figcaption></figcaption></figure>

* **Navigation Controls**: Zoom, pan, and snap to your current location.
* **Persistent Filters**: All [filters](filters.md) applied in other views are maintained on the Map.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.35.32@2x.png" alt=""><figcaption></figcaption></figure>

## **Map in Dark Mode**

Works seamlessly with system-level dark mode settings.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-11 at 10.38.38@2x.png" alt=""><figcaption></figcaption></figure>

**Use Cases**

The Map view unlocks unique perspectives across use cases:&#x20;

* Plot customer, partner or asset locations
* Visualize service regions, sales territories or market areas
* Track shipment origins and destinations
* Map event venues, job sites or inventory warehouses
* Identify facility locations for operations analysis
* Monitor field employee positions
* Pinpoint geographical assets and points of interest

With interactive navigation and built-in location tracking, the Map View surfaces new dimensions within your data.

\
