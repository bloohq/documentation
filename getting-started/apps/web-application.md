# Web Application

Blue is available as a web application that works on all major web browsers. You can access Blue by going to [https://app.blue.cc](https://app.blue.cc)

{% hint style="info" %}
**Access Beta**

If you would like to try the latest features of Blue, [you can access Blue Beta by visiting this link.](https://beta.app.blue.cc) You will see all of your regular data, but with additional features that are in Beta [as noted in our release notes. ](https://blue.cc/releases)


{% endhint %}
