# Progressive Web Application

Progressive Web Apps (PWAs) are a type of application software delivered through the web, built using standard web technologies including HTML, CSS, and JavaScript. They offer a high-quality user experience that is remarkably similar to native applications.  The Blue PWA combines the convenience and accessibility of a web application with the robust features and performance of a native app, providing a seamless and efficient way to manage your projects and tasks directly from your mobile browser.&#x20;

PWAs are great if you do not have access to the App Store (iOS) or Google Play (Android) or if certain features you want to access on the go are unavailable on our mobile application.&#x20;

## PWA on iOS&#x20;

1. **Open Chrome**: Launch the Chrome app on your iOS device.
2. **Navigate to Blue**: Enter the URL of the Blue web application (https://app.blue.cc)  in Chrome's address bar.
3. **Access the Share Menu**: Once the Blue web application has loaded, tap the Share icon (the rectangle with an upward arrow) at the bottom of the screen.
4. **Add to Home Screen**: In the Share menu, scroll down and select "Add to Home Screen." This will allow you to install the Blue PWA.
5. **Name Your PWA**: You’ll be prompted to name the shortcut before adding it to your home screen. You might want to name it ‘Blue’ for easy identification.
6. **Complete the Installation**: Tap "Add" in the upper right corner of the screen. The Blue PWA icon will now appear on your home screen, similar to any other app icon.
7. **Launch Blue PWA**: Tap the Blue icon from your home screen to open the PWA and use Blue like a native app.

## PWA on Android&#x20;

1. **Open Chrome**: Launch the Chrome app on your Android device.
2. **Go to Blue**: Type the Blue web application's URL (https://app.blue.cc) into Chrome's address bar.
3. **Access the Menu**: Tap the three dots in the upper right corner of the browser to open the menu.
4. **Install the App**: Look for the “Install” option in the menu. This could also appear as “Add to Home screen” or an icon representing a downward arrow inside a box.
5. **Confirm Installation**: A prompt will appear asking if you want to install the app. Confirm by tapping “Install” or “Add.”
6. **Open the Blue PWA**: Once installed, the Blue icon will appear on your home screen or in the app drawer, just like a native application.
7. **Use Blue**: Simply tap on the Blue icon to use the app as you would on a desktop, but with the added benefits of a mobile experience.

\


\
