# iPad Application

Blue has a specific iPad application that is optimised to take advantage of the tablet's large screen real estate.&#x20;

[You can download the Blue iPad app here. ](https://apps.apple.com/app/blue-teamwork/id6443870775)
