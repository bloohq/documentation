---
description: Blue is available as a desktop application for Windows, Mac, and Linux.
---

# Desktop Application

## Benefits

The benefits of installing Blue as a desktop application are:

* Faster performance&#x20;
* There is no browser interface around the Blue interface
* Save Blue to Mac Dock or as a Windows desktop shortcut
* True Fullscreen Mode
* Native desktop notifications&#x20;
* Automatic updates&#x20;

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/blue desktop app.png" alt=""><figcaption><p>An immersive experience with the Blue Desktop Application</p></figcaption></figure>

</div>

## Requirements

To install Blue as a desktop application, you'll need a Chromium-based browser such as:

* [Google Chrome](https://www.google.com/chrome/)
* [Brave](https://brave.com/)&#x20;
* [Microsoft Edge](https://www.microsoft.com/edge)
* [Opera](https://www.opera.com/)
* [Vivaldi](https://vivaldi.com/)
* [Chromium](https://www.chromium.org/)&#x20;

## Installation Instructions

Once you have installed any of the above browsers, simply[ log in to Blue](https://app.blue.cc) as usual. You will then notice that there is an install icon on the right-hand side of the URL Address Bar:

<figure><img src="../../.gitbook/assets/desktop install.png" alt=""><figcaption><p>Desktop app install icon</p></figcaption></figure>

Click the install icon in your browser's address bar to install the Blue desktop application. This icon is typically represented by a small computer or plus symbol.&#x20;

Once you click on it, you will be prompted through a straightforward installation process. Follow these prompts carefully to ensure that the Blue application is correctly installed on your computer. After the installation is complete, you'll have the advantage of accessing Blue directly from your computer, bypassing the need to open your web browser. This direct access streamlines your workflow and enhances the overall user experience.

You can also watch this how-to video for instructions:

{% embed url="https://www.youtube.com/watch?v=TZye8hnk4ac" %}

## **Adding to Mac Dock**&#x20;

For Mac users, once the Blue application is installed, you can add it to your Dock. This provides you with quick and easy access to Blue, right from your Dock. To do this, open the Blue application and right-click (or control-click) on its icon in the Dock. From the menu that appears, select "Options," and then choose "Keep in Dock." This will ensure that the Blue icon remains in your Dock even after you close the application, allowing for faster and more convenient access in the future.

## Windows Shortcut

Windows users can also benefit from quick access by creating a shortcut for the Blue application on their desktop. After installing Blue, navigate to your system's location where the application is installed. Right-click on the Blue application icon and select "Create shortcut." Then, choose to place this shortcut on your desktop. This creates an easily accessible icon on your desktop, enabling you to launch Blue with a double-click without navigating through your system's menu or file explorer.
