# Mobile Applications

Blue has native iOS and Android applications, and these can be downloaded via the following link:

* [iOS](https://apps.apple.com/app/blue-teamwork/id6443870775)
* [Android](https://play.google.com/store/apps/details?id=io.bloo.android)

The mobile applications are designed for work on the go and do not have the full functionality of the web applications. You can review and reply to all comments and see the boards and calendars.

Features that are not available on the mobile applications:

* [Dashboards](../../platform/dashboards/)
* [Database View ](../../platform/views/database.md)
* [List View](../../platform/views/list.md)
* [Map View](../../platform/views/map.md)
* [Timeline / Gantt Charts](../../platform/views/timeline-and-gantt-charts.md)
* [CSV Import](../../platform/integrations/csv-import.md)
* [CSV Export ](../../platform/integrations/csv-export.md)
