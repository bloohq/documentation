# How to Create Processes

* Think of your step by step approach using lists
* Consider categorization using tags
* Custom data structure using custom fields
* Automate work using automations&#x20;
* Dashboards and reporting using dashboards&#x20;
