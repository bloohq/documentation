# Billing

{% hint style="info" %}
[**You can access the Blue billing portal here**](https://billing.blue.cc/p/login/14k7w17SddUW0yk288) to view historical payments, download invoices, update payment details, and change plans.
{% endhint %}

## Plan

Blue has one plan with all the features.&#x20;

* Monthly plan: **$7/month/user.**&#x20;
* Yearly plan: **$70/year/user (so you get two months free)**

## Paid vs Free User Roles

Not all user types in Blue count towards your subscription cost. The table below gives an overview of the different roles:

| User Type                                                               | Paid/Free |
| ----------------------------------------------------------------------- | --------- |
| [Project Administrator](user-management/roles/project-administrator.md) | Paid      |
| [Team Member](user-management/roles/team-member.md)                     | Paid      |
| [Custom User Role](user-management/roles/custom-user-roles.md)          | Paid      |
| [Clients](user-management/roles/client.md)                              | Free      |
| [Comment Only](user-management/roles/comment-only.md)                   | Free      |
| [View Only](user-management/roles/view-only.md)                         | Free      |

## 7-Day Free Trial

When you sign up for Blue, you'll start with a 7-day free trial to test all of our features. During this trial period, you won't be charged anything and don't need to provide a credit card. Once your free trial ends, you can choose between the monthly or yearly payment options.

You can email us at [**help@blue.cc**](mailto:help@blue.cc) if you need to extend your trial.

The only restriction on the free plan is that you can only invite 20 users to your account. This is done to prevent spam and abuse of our systems. Once you have an active subscription, you can invite as many users as you wish.&#x20;

## How Billing Works

### Monthly Payment

When you sign up for the monthly plan, you'll be charged immediately, but only for the remaining days in the current month. This is called a prorated amount. For example, if you start your subscription on the 10th of the month, you'll only pay for the days from the 10th to the end of that month instead of the full monthly price.

Then, on the 1st of every month after that, Blue will charge you the regular monthly price based on how many paid users you have in your account. So, if you have 5 paid users, your monthly bill will be $35 (5 users x $7 per user).&#x20;

### Yearly Payment

When you sign up for the yearly payment plan, you'll be charged a prorated amount for the remaining days in the current month. This means you'll only pay for the days between the end of your trial and the end of that month rather than the full monthly cost.

For example, if your free trial ends on the 10th of the month and you choose the yearly plan, you'll be charged for the days between the 11th and the end of that month. This prorated payment ensures that your yearly subscription begins on the 1st of the following month.

From then on, you'll be billed once per year on the 1st of the month. The yearly payment covers all 12 months; you won't have to worry about monthly charges. You'll get a reminder before each yearly payment, so you can review your account and make any necessary changes.

You'll save money by choosing the yearly payment option compared to paying monthly. You'll get two months free, essentially paying for 10 months while receiving 12 months of service. This option is great for teams planning to use Blue long-term and want to simplify their billing process.

### Adding Users to Plans

When you add a new user to your Blue account, you'll be billed for that user based on your current billing cycle. For monthly plans, if you add a user in the middle of your billing cycle, you'll be charged a prorated amount for that user until the next billing date. For example, if you add a user on the 15th of the month, you'll be billed $3.50 (half the monthly rate) for that user until the 1st of the following month, when your next billing cycle begins.&#x20;

Similarly, for yearly plans, if you add a user during your annual billing cycle, you'll be charged a prorated amount for that user until your next annual billing date. For instance, if you add a user 6 months into your yearly plan, you'll be billed for the remaining 6 months of the year at a prorated rate.

### Removing users from plans

If you need to remove a user from your Blue account, any unused portion of their subscription will be credited to your future invoices. For monthly plans, if you remove a user before the end of your billing cycle, you'll receive a credit for the remaining days in that cycle. For example, if you remove a user 10 days into your 30-day billing cycle, you'll receive a credit of $4.67 for the remaining 20 days, which will be applied to your next invoice. This is calculated by dividing the monthly rate of $7 by 30 days to get a daily rate of $0.233, then multiplying that daily rate by the 20 unused days.

For yearly plans, if you remove a user before your annual billing cycle ends, you'll receive a credit for the remaining months of their subscription. For instance, if you remove a user 3 months into your yearly plan, you'll receive a credit for the remaining 9 months of their subscription, which will be applied to your next annual invoice. To illustrate, if your yearly plan costs $70 per user, removing a user after 3 months would result in a credit of $52.50. This is calculated by dividing the annual rate of $70 by 12 months to get a monthly rate of $5.83, then multiplying that monthly rate by the 9 unused months.

Blue's billing system is designed to ensure that you only pay for the services you use, even if your team's needs change over time. By prorating charges for added users and crediting unused subscription time for removed users, Blue helps you manage your costs effectively and avoid paying for resources you don't need. This flexibility allows you to scale your team up or down as required without worrying about wasting money on unused subscriptions or being locked into a plan that no longer suits your needs.



