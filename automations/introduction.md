# Introduction

Automations are one of the most powerful features of Blue, and yet they have been purposefully designed to be easy to use and create. Unlike most project management systems, Blue does not limit the number of automations or how many times they can be triggered within a month.&#x20;

Use our interactive guide below or scroll down to read more.

{% @arcade/embed flowId="oDtJdrmOjMMPdda020E9" url="https://app.arcade.software/share/oDtJdrmOjMMPdda020E9" fullWidth="true" %}

## How Automations Work

All automations follow a similar pattern:

**`When`**` ``something happens,`` `**`Then`**` ``do something else`

* [**Triggers ("When"):** ](triggers.md)A trigger is an event that sets off an automation. For instance, a trigger could be the completion of a record, a change in a record's assignee, or a change in a custom field. When this predefined condition is met, the automation is activated.
* [**Actions ("Then")**:](actions/) Once triggered, an automation performs a specific action. Actions can range from sending notifications and updating record fields to creating new tasks or changing the status of a record. Actions can be chained.&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/automations then when.png" alt=""><figcaption></figcaption></figure>

</div>

You can create multiple automations within one project to help you keep work moving forwards. You can also pause automations and resume them later by using the green toggle.&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/automations.png" alt=""><figcaption><p>A list of automations in a project</p></figcaption></figure>

</div>

Each automation is tailored to a specific project, primarily influencing a single record within that project. This focused approach ensures that automations are relevant and effective in their designated context.

While most automations are project-specific, Blue also provides a unique automation that can move or copy a record from one project to another. This feature is especially useful for workflows that span multiple projects or departments.

<div data-full-width="true">

<figure><img src="../.gitbook/assets/automation move record.png" alt=""><figcaption></figcaption></figure>

</div>



Blue also supports conditional automations, where multiple triggers must be met for the automation to activate. This feature allows for more complex and nuanced workflow automation and routing, as it can account for a broader range of scenarios before executing an action. For instance, an automation could be set to trigger only when a task reaches a certain process step and has a specific tag. This level of specificity ensures that automations are not only triggered by relevant events but also under the right conditions, enhancing the precision and relevance of automated workflows.

## User Permissions

The creation and setup of automations are exclusively managed by [Project Administrators](../user-management/roles/project-administrator.md). This role-based approach ensures that automations are thoughtfully crafted and aligned with the project's goals and strategies. However, to maintain transparency and collective awareness within the team, all roles can view which automations are active. This visibility allows everyone in the project to understand the automated processes in place, fostering a shared understanding of how the project’s workflow is enhanced and streamlined by these automations.

{% hint style="danger" %}
**Infinite Loops**

One automation in Blue cannot trigger another automation. This safeguard is crucial to avoid [infinite loops ](https://www.wikipedia.com/en/Infinite\_loop)of automations, ensuring stable and predictable operation of the system. In a future release, we plan to enable the chaining of automations by having an infinite-loop detection system.&#x20;
{% endhint %}





\
