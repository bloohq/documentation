# Email Automations

Email automations allow you to automatically send emails when certain triggers occur, using data stored within your Blue records. For example, sending a confirmation email when a new record is created or notifying a manager when a task becomes overdue.

### Configuration&#x20;

Getting started with email automations is straightforward. When you first create an automation, we recommend sending a test email to yourself or a colleague. This allows you to validate that the automation is functioning before rolling out.\


To create an email automation:

* Set the trigger, such as&#x20;
  * A New Record is Created
  * A Tag is added to a Record&#x20;
  * A Record is Moved to Another List&#x20;

<figure><img src="../../.gitbook/assets/CleanShot 2024-02-06 at 14.37.49@2x.png" alt=""><figcaption></figcaption></figure>

Once you've set up for when something happen Then "Send Email"&#x20;

<figure><img src="../../.gitbook/assets/CleanShot 2024-02-06 at 14.39.14@2x.png" alt=""><figcaption></figcaption></figure>

Now it's time to Configure the email details:&#x20;

<figure><img src="../../.gitbook/assets/CleanShot 2024-02-06 at 14.40.07@2x.png" alt=""><figcaption></figcaption></figure>

* From name and reply-to address so recipients can respond.
* To address (can be static or pulled dynamically from an [Email custom field](../../custom-fields/email.md))
* Enter your own email address or a test email in the recipient field. If you want to loop in others, you can also add CC or BCC email addresses.&#x20;

<figure><img src="../../.gitbook/assets/CleanShot 2024-02-06 at 14.53.44@2x.png" alt=""><figcaption></figcaption></figure>

Next, customize the email content using merge tags:

* Email subject and body content using custom field for dynamic content using merge tags.
* Click inside the email body, then insert merge tags referencing record names, custom fields etc. using the {curly bracket} syntax.
* Insert file attachments by drag-and-drop or using the attachment icon. Files from File custom fields may automatically attach if under 10MB.

<figure><img src="../../.gitbook/assets/CleanShot 2024-02-06 at 14.52.44@2x.png" alt=""><figcaption></figcaption></figure>

Once you finish customizing the template, confirm creation of the automation. When triggered, it will now send personalized emails leveraging your record data.

{% hint style="warning" %}
Almost all data can be used except for comment, activity and checklists.
{% endhint %}

### Automation result&#x20;



### Use Cases

**Send confirmation email when client submits request via intake form**

_**Automation trigger**_&#x20;

When a new record is created through forms -> Send Email

The Forms should submission should also have an email field so that an automated email will send a proper respond to the client. &#x20;

**Notify assignee when new high priority task is created**

_**Automation trigger**_&#x20;

When a tag "Priority"  is added to a record  -> Send Email&#x20;

On the Email template the receiver should be {Assignee} so that an automated email will be sent to the assigned user.&#x20;

**Send survey to customer after support ticket is marked resolved**

_**Automation trigger**_&#x20;

When a record is marked completed, under the 'Done' List   -> Send Email

Include customer email in the custom field, with the issue in detail so that you can configure the templates with set custom fields allowing customer to receive detailed explanation on the issue and how it has been resolved.&#x20;

**Automate recruitment program by sending confirmation emails**

_**Automation trigger**_&#x20;

When an application has been submitted through Forms   -> Send Email

Include application email in the forms so that it automatically capture that data and send a Thank you email response when records is created under 'Received' list.&#x20;

### Key Benefits

* **Personalized Communication**: Emails can leverage custom field data from records using merge tags, enabling dynamic, tailored messaging.
* **Automatic Notifications**: Takes manual work out of sending repetitive email updates and reminders.
* **Structured Data-Driven Workflows**: Automated emails can move projects forward by triggering actions based on record data.

