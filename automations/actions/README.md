# Actions

Once triggered, an automation performs a specific action. Actions can range from sending notifications and updating record fields to creating new tasks or changing the status of a record. Actions can be chained.&#x20;

These are all the possible automation actions in Blue. Note that not all actions are available for all triggers.&#x20;

* **Move Record to Another List**: This action relocates a record to a different list within the same project, often used to indicate a change in the record’s stage or status.
* **Copy Record to Project**: Duplicates the selected record into another project, useful for sharing information across different workflows or teams. If you have custom fields that have the same name and type in the receiving project, the data in those custom fields will be transfered seamlessly.&#x20;
* **Mark the Record as Completed**: Changes the status of a record to 'complete', signifying the conclusion or fulfilment of its associated task or process.
* **Mark the Record as Incomplete**: This option changes a record's status from 'complete' to 'incomplete', indicating that the task or process requires further attention or action.
* **Assign Someone**: Adds a specific user as the assignee to a record, delegating responsibility for that task or item. The individual will receive a notification.&#x20;
* **Unassign Someone**: Removes the current assignee from a record, indicating a change in responsibility or task ownership.
* **Change Due Date**: Alters the due date of a record, which is useful for adjusting timelines and managing deadlines.
* **Remove Due Date**: Clears the existing due date from a record, which can indicate a shift in priority or scheduling.
* **Add Tags**: Appends new tags to a record, aiding in categorization and enhancing the ability to filter and organize tasks.
* **Remove Tags**: Deletes existing tags from a record, reflecting changes in task attributes or categorization.
* **Mark All Checklist Items as Complete**: Automatically checks off all items in a record’s checklist, useful for acknowledging the completion of all sub-tasks.
* **Create Checklists**: Adds a new checklist to a record, facilitating the breakdown of tasks into smaller, manageable sub-tasks.
* **Send Email**: Triggers the sending of an email, which can contain any information from the record using {merge} tags.&#x20;
* **Add a Custom Field**: Inserts data into a custom field to a record, allowing for the addition of tailored information specific to the task or process.
* **Remove a Custom Field**: Removes data from an existing custom field from a record, simplifying the data or adjusting the record’s information structure.

{% hint style="info" %}
**Section under development**

Please be aware that this section is currently a work in progress. We are actively enhancing and updating its content to provide you with the most comprehensive and useful information. We appreciate your patience and understanding as we work to improve this resource. Stay tuned for more detailed and refined content coming soon!
{% endhint %}

\


