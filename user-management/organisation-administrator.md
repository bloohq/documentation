# Organisation Administrator

As the organization owner in Blue, you have a special admin role that gives you additional privileges and responsibilities across your entire organization. You have full control and oversight over the following areas:

**Organization Settings**

To edit the core organization settings, click on your profile picture in the top right corner and select "Company Settings" from the dropdown menu.

Here you can update:

* **Name**: The  name of your organization
* **URL**: The  URL slug that users access i.e, app.blue.cc/company/blue
* **Icon**: The icon image displayed in the sidebar
* **Color**: The theme color for your organization to Blue, Pink, Orange, Yellow, Black, Purple, etc.&#x20;
* **Map Settings**: allow visibility to company map&#x20;
* **Delete Company:** all data will be erased&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-01-30 at 16.04.01@2x.png" alt="" width="563"><figcaption></figcaption></figure>

**User Management**

**Remove Users** If someone leaves your company, you can fully remove them from the organisation by going to organisation settings and selecting the People tab.

Their access will be revoked. All their comments and files will still be visible across the projects. Of course, there's a confirmation prompt before deleting a member, so that you won't accidentally remove them.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-08-09 at 09.56.41@2x.png" alt=""><figcaption></figcaption></figure>

**Billing & Plans**

You have exclusive access to billing and subscriptions as the organization admin. Under your organization setting, select **Billing** to manage payment details and plans.

Specific functions include:

* Update credit card details
* Edit your subscription plan to monthly/yearly
* Cancel the subscription

<figure><img src="../.gitbook/assets/CleanShot 2024-01-26 at 10.28.34@2x.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
If you want your finance or admin team to receive invoices from Blue, please email us at help@blue.cc and we can ensure that are in CC for all billing emails.&#x20;
{% endhint %}

### Additional Support

If you need any other help with your admin privileges, please reach out to help@blue.cc. We're always happy to assist organisation administrators to make sure you get the most out of Blue.
