# @mentions

@mentions in Blue provide a way to directly notify other users of relevance to a task or discussion. By using the @ symbol followed by someone's name, you can tag them in a comment or record a description.

<figure><img src="../.gitbook/assets/file-ubCAXsVYYF.gif" alt=""><figcaption><p>Easily @mention team members in any text area</p></figcaption></figure>

This triggers a notification for that user, alerting them of the mention. It brings their attention precisely where it is needed.

@mentions can be used to:

* **Delegate Tasks**: Assign work or responsibilities by @mentioning the owner.
* **Ask Questions**: Seek answers or input from experts by @mentioning them in your query.
* **Give Recognition**: Show appreciation by @mentioning teammates who have done great work.
* **Clarify Ownership**: Document who is responsible when @mentioning the owner in notes.
* **Notify Stakeholders**: Keep clients or managers in the loop by @mentioning them in updates.

{% hint style="info" %}
You can only @mention people who are in that particular project. If a person is not in the project, they will not appear in the drop-down suggestion list of people you can @mention.
{% endhint %}



## Group @mentions

You can easily @mention groups of users based on their assigned roles or @mention everyone involved in a project. This feature ensures that relevant members receive timely notifications and stay informed about important updates or discussions.

To @mention users based on their roles, you can use the following options:

**Predefined Roles:**

* @Admins: Mention all users with administrative privileges.
* @Team Members: Mention all users who are members to the project.
* @Clients: Mention all clients associated with the project.
* @View Only: Mention users who have view-only access to the project.
* @Comment Only: Mention users who can only leave comments within the project.
* @everyone: Mention everyone regarding their roles within the project.&#x20;

**Custom Roles:**

* @Custom User Roles: Mention users assigned to specific custom roles you've defined within the project.

When you @mention a group by their role, all users within that selected role will receive notifications through their configured channels (e.g., email or push notifications). Additionally, the @mention will appear in their dedicated "@mention" box within Blue, ensuring they don't miss any important updates or conversations.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-13 at 14.13.29@2x.png" alt=""><figcaption></figcaption></figure>

