# Notifications

Notifications are a critical part of modern collaboration. They ensure you stay informed on key updates from coworkers, clients, and project stakeholders.

However, notification overload can be counterproductive. That's why Blue puts you in control over your notifications through easy configuration options. You choose when, where, and what notifications you receive. This empowers you to tune notifications to your priorities for a focused, productive workflow.

### Receiving Notifications

Blue offers notifications through:

* **Email**
* **Desktop & Mobile Push:** Enable browser/app notifications

### Customizing Notifications

Under your notification setting, you can customize notifications in two ways:

**1. Per Project**

For each project you join, choose which project want you to get notified.&#x20;

**2. System-wide**

Across all projects, configure types of platform notifications like:

* @Mentions
* New task assignments
* Added to new project&#x20;
* New Task
* Reminders
* Changes in task you assigned to&#x20;
* Comment updates
* Overdue tasks
* Completed tasks
* Edits to assigned tasks
* New Chat&#x20;
* Adding new members&#x20;

By selectively enabling notifications types per project, you hone in on updates that are most relevant amidst your various initiatives. This tunes out noise while retaining visibility on critical events.

Through flexible configuration options, Blue puts you in control over your notifications. You decide what requires your attention across projects - reducing distraction and boosting productivity.
