# Profile Settings

Your profile allows you to customize your experience in Blue. Here are the various settings you can configure:

### Overview

You can access your profile by clicking on your profile picture/initials in the top right corner of Blue.

The key settings you can customize are:

* Name and email
* Profile picture
* Color theme
* Date format
* Birthday

<figure><img src="../.gitbook/assets/CleanShot 2024-01-10 at 16.34.44@2x (1).png" alt=""><figcaption><p>Profile setting</p></figcaption></figure>

Let's explore each of these:

### Name and Email

You can update your name and email address from your profile. Your name will appear across Blue and your email is used for notifications.

### Profile Picture

Adding a profile picture makes it easier for your team to recognize you. You can upload any image you like, crop it, and set it as your profile picture.

### Color Theme

Match the color theme to your branding by choosing from preset options like Blue, Pink, Green, Purple, Orange, Red and Black. This color will be reflected in highlights and accents across the platform.

### Date Format

Set your preferred date format - you can choose between US date style MM/DD/YYYY or the worldwide style DD/MM/YYYY.

<figure><img src="../.gitbook/assets/unnamed (1).png" alt=""><figcaption></figcaption></figure>

### Birthday

Add your birthday so your Blue team can celebrate your special day! Your birthday will appear on your profile page.

### Custom Timezone&#x20;

Set a custom timezone based on your HQ location, this will ensure all due dates and schedules align seamlessly with your colleagues. This preferences is crucial especially for team all over the world or a remote office.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-02 at 17.36.42@2x (1).png" alt=""><figcaption></figcaption></figure>
