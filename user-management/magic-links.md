# Magic Links

Blue offers one-time passwords (OTPs) and magic login links for added security and convenience instead of the traditional email and password login method.&#x20;

### Why Use OTPs and Magic Links?

OTPs and magic links provide two major benefits:

**Enhanced Security**

OTPs and magic links provide an extra layer of protection by eliminating the need to enter an actual password. Your password cannot be phished or spied upon when logging in. There is also a tendency for many users to share passwords across many different services. The problem is that when one service has a security breach, this creates security breaches in all other services.&#x20;

**Convenience**

Needing to remember long, complex passwords can be a hassle. OTPs and magic links allow swift, simple access without hunting for passwords.

### How Do They Work?

**One-Time Passwords**

When OTP login is enabled, Blue will prompt you for a unique 4-digit code instead of your password. This code is sent to your email and is only valid for one login attempt.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-12 at 11.54.36@2x.png" alt=""><figcaption></figcaption></figure>

**Magic Login Links**

Magic links are unique login URLs emailed to you. Clicking the link will instantly log you into your Blue account without needing your password. The link expires after a single use.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-12 at 11.54.58@2x.png" alt=""><figcaption></figcaption></figure>
