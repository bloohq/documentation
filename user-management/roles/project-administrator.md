# Project Administrator

The Project Administrator is the highest-privilege role within a Blue project similar to Project owner. Project Admins have complete control over the project and its settings.

**Permissions**

Project Admins can:

* Edit all project settings
* Add/remove project members
* Assign user [roles](./) and [permissions](custom-user-roles.md)
* [Import](../../integrations/csv-import.md) and [export](../../integrations/csv-export.md) project data
* Delete project
* Create [custom fields](../../custom-fields/)
* Create custom roles
* Setting [Automation](../../automations/)

Essentially they have unlimited permissions within the project.

**Use Cases**

Typical users assigned as Project Admin:

* Project managers
* Team leaders
* IT managers
* Business owners

Anyone who needs to fully configure the workflow or manage members.

**Assigning the Role**

When inviting members, the Project Admin role can be assigned.

Existing members can also be promoted to Project Admin. But there must always be at least one Project Admin in a project.

**Removing the Role**

To remove Project Admin permissions from a member, they must first be demoted to a more restricted custom role or standard role.
