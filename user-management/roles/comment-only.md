# Comment Only

In Blue, "Comment-Only Users" are a unique type of user role designed to participate in project discussions while having restricted access to other functionalities. This role is ideal for those who need to contribute to conversations and provide feedback without the need for full project access.

## **Role and Permissions**

A Comment-Only User in Blue is primarily focused on communication within the project. Their permissions include:

* **Adding Comments**: They can actively participate in discussions by adding comments to existing records or tasks.
* **Reading Access**: They have the ability to read all the comments and view the details within the project, keeping them informed and engaged.

## **Limitations**

While Comment-Only Users can engage in discussions, their access is limited in several ways:

* **No Editing Capabilities**: They cannot create or edit records or tasks.
* **Restricted Access**: Their access is confined to viewing and commenting, without the ability to alter project settings, add or modify records.&#x20;

**Ideal Use Cases**

This role is particularly useful in scenarios such as:

* **Client Feedback**: Allowing clients to provide input on specific tasks or projects without giving them full access.
* **External Collaboration**: Engaging with external partners or stakeholders who need to comment on ongoing work but do not require full project access.
* **Review and Approval Processes**: Facilitating feedback loops where input from various team members is required, but not full project participation.

## **Setup**

To set up a Comment-Only User in Blue, a Project Administrator needs to:

1. Go to the 'People' tab in the project settings.
2. Add the individual as a user and assign them the 'Comment-Only' role.

**Conclusion**

Comment-Only Users are an essential part of Blue’s collaborative environment, allowing for controlled participation in discussions and feedback processes. By integrating these users into your project, you can ensure that all necessary parties are involved in the conversation while maintaining the integrity and security of the project’s data and workflows.
