# Team Member

The Team Members role in Blue provides users more access compared to a basic Client User, while still restricting certain sensitive actions. As the name suggests, it is ideal for actual team members collaborating within a project.

**Permissions**

Team Members can:

* Add other Team Members
* Create new projects
* Make project templates from existing projects
* Have a personal project for task management
* Add, edit, mark complete, and delete most items in a project

**Restrictions**

However, Team Members are limited in that they cannot:

* Delete projects
* Access admin and billing settings
* Upgrade account plan
* Access other user's personal projects
* Delete other user's items&#x20;

**Use Cases**

The Team Member role empowers actual team members to collaborate while limiting destructive permissions:

* Contributing to shared project work
* Creating and managing their own tasks
* Building new projects using existing templates
* Populating personal task manager
* Commenting and editing most items

**Setup**

To grant the Team Member role:

1. Go to People tab in project
2. Add team member
3. Enter their email and sent invite!&#x20;
