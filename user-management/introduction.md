# Introduction

User permissions in Blue are set at the project level, ensuring tight security and appropriate access for each project. This means users can have different roles in different projects - for example, someone could be a [Project Administrator ](roles/project-administrator.md)with full access in one project, but only have [view-only permissions ](roles/view-only.md)in another. This approach keeps each project secure and accessible only to those who need it.

Each project in Blue features a 'People' tab, which displays all the participants involved along with their respective roles in the project. Within this tab, [Project Administrators](roles/project-administrator.md), [Team Members](roles/team-member.md), and [Clients](roles/client.md) can invite new people to the project, but only up to their own level of permissions. Additionally, [Project Administrators](roles/project-administrator.md) have the exclusive authority to remove individuals from the project as needed.

Dashboards are managed at the company level, and the creator of a dashboard has full control over who can view or edit it. They can designate specific users as either viewers or editors. Viewers have access to the dashboard, while editors have the additional ability to make changes to it. Anyone with access to a dashboard can share the dashboard with others, extending collaboration and ensuring that the right team members have access to these valuable insights and data visualizations.





