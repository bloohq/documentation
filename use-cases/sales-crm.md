# Sales CRM

Blue provides a robust set of tools to manage your sales workflow, giving you a powerful CRM system tailored to your needs.

### Sales Pipeline Management

Leverage Blue's **lists** to model your sales funnel by creating stages. As deals progress, sales reps can drag & drop records between lists to advance them down the pipeline. This will also provides entire team visibility into sales pipeline and key client information

#### Sales Flow&#x20;

* Lead
* Qualified Lead&#x20;
* In Contact&#x20;
* Follow Up&#x20;
* Closed Won&#x20;
* Closed Lost

<figure><img src="../.gitbook/assets/CleanShot 2024-04-08 at 13.10.06@2x.png" alt=""><figcaption></figcaption></figure>

### Centralized Contact Database

Create a Contact custom field to store all your prospect and customer information. All contacts become easily accessible records. Switch between boards and database for viewing leads at a glance.&#x20;

#### Storing sales database&#x20;

* **Single Select Field:** Contact Person &#x20;
* **Phone Number Field:**  Phone number &#x20;
* **Email Field:** Contact Email &#x20;
* **Currency Field:** Client's budget&#x20;
* **Files Field:** For contract, signed documents, client preferences.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-08 at 14.24.09@2x.png" alt=""><figcaption></figcaption></figure>

### Activity Tracking & Team Communication

Log all sales activities like calls, emails, and meetings via the  comments on contact records. Follow up between team members to stay up to date to what the latest client's update.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-08 at 14.29.49@2x.png" alt=""><figcaption></figcaption></figure>

### Email Integration

Sync email automatically into associated contact records. Email right from Blue using custom contact fields like {Email} and {FirstName} in the body.&#x20;

Set triggers and send out email automation to communicate with clients for any quotation updates, informing clients on members that are active on the proposal.&#x20;

* When a record is moved to "**Follow up"** list then send email to client.&#x20;
* When a record is moved to **"Closed Won"** list then assign member to email client for aftercare.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-04-08 at 14.33.17@2x.png" alt=""><figcaption></figcaption></figure>

### Reporting & Analytics

Build real-time sales reports on the dashboards to track deal volume, sales cycle analytics, win/loss metrics, activity frequency, and more using filters for analysis.

With custom fields tailored to your sales data, selectively shared access to confidential deal information, automation to drive productivity, and advanced reporting from multiple projects, Blue provides a complete sales CRM environment enhancing your workflow.
