# Recruitment

Finding and hiring top talent can be a complex, multi-step process. Blue provides powerful tools to manage applicants and openings in one platform.

### Central Job Board

Use Blue as a  job board by listing all open positions as records. Capture key details like responsibilities, requirements, department, location, and salary in custom fields.

### Application Workflow

Model your hiring funnel in Blue using Lists for stages like :

* Applied
* Reviewing
* Interview
* Offer Made
* Hired

<figure><img src="../.gitbook/assets/CleanShot 2024-02-19 at 12.47.33@2x.png" alt=""><figcaption></figcaption></figure>

### Online Applications

Create a simple Blue form linked to your careers page. Candidate submissions automatically generate applicant records—no manual data entry is needed. Then, automated emails will be sent out thanking the candidates for applying.&#x20;

**Setting Up the Blue Form:**

* Navigate to the 'Forms' section within your Blue "Recruitment" Project.
* Select 'Create New Form' and drag and drop any necessary field to customize the form fields to capture essential information from candidates, such as name, contact information, resume, and cover letter.
  * **Single Select Field:** Name&#x20;
  * **Number Field:**  Age&#x20;
  * **File Field:** Cover letters&#x20;
  * **Email & Phone Field:** References&#x20;
* Embed the form on your careers page by copying the provided HTML code into your website's backend.

<figure><img src="../.gitbook/assets/CleanShot 2024-02-19 at 12.48.45@2x.png" alt="" width="563"><figcaption></figcaption></figure>

**Configuring Automated Thank-You Emails:**

* Go to the automation setting on Blue
* When a new records is created(New forms subnmitted) then send email&#x20;
* Edit your email template
* Customize the email template to convey your gratitude and include any additional information you wish to share with applicants (e.g., next steps, timeline for the hiring process).

### Records Management

Create tasks for scheduling interviews, making calls, or checking candidate references and assigning them to the appropriate recruiting team members.



**Record Creation:**

* Click on the 'Create Record' button or receive the new records from forms submission
* Specify the task type (e.g., Interview Scheduling, Candidate Call, Reference Check) in the list.&#x20;

**Detailed Instructions:**

* In the record description, provide comprehensive details about the recrod, including the candidate's name, contact information, and any relevant documents or links.
* For interviews, include the preferred time frame, format (in-person, video call, etc.), and any specific questions or topics to cover.

**Assigning Tasks:**

* Select the appropriate team member to receive and conduct the interview
* Once you've assigned the records, the member will receive a notification on their mobile/email.&#x20;

**Monitoring Progress:**

* Utilize the platform's tracking capabilities to monitor task completion and follow-up as necessary.
* Team members will update records (e.g., In Progress, Completed) and add notes or outcomes for transparency and record-keeping.\


<figure><img src="../.gitbook/assets/CleanShot 2024-02-19 at 12.53.06@2x.png" alt=""><figcaption></figcaption></figure>

### Candidate Profiles

Store all documents, notes, and activity from outreach to offers within each candidate's record in Blue. A centralized profile keeps candidate communication organized.

<figure><img src="../.gitbook/assets/CleanShot 2024-02-19 at 12.51.59@2x.png" alt=""><figcaption></figcaption></figure>

### Hiring Analytics

Gain insights with real-time dashboard tracking for metrics like applications per opening, interview-to-hire ratio, vacancy duration, and more to optimize recruiting.

With process automation, built-in workflows, and analytics, Blue enables your team to scale hiring and demonstrate clear ROI.

<figure><img src="../.gitbook/assets/CleanShot 2024-02-28 at 10.53.58@2x.png" alt=""><figcaption></figcaption></figure>
