# Unique ID

The Unique ID field automatically generates a unique alphanumeric identifier for each record.&#x20;

This brings immense value by creating persistent keys that consistently identify each record. Even if a record is deleted, its ID will never be duplicated.

Potential uses include:

* Associating external systems to records via unique codes
* Creating audit trails by tracking ID references
* Searching for records easily in the platform via the ID, even if the record name changes

<figure><img src="../../.gitbook/assets/UniqueID .png" alt=""><figcaption></figcaption></figure>

Unique IDs essentially establish immutable "social security numbers" for each record. This permanence and specificity enables powerful integrations, analytics, and identification capabilities.

<figure><img src="../../.gitbook/assets/unique id database table.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
**Total Unique IDs Available**&#x20;

There are 36 possibilities for each of the 25 characters in a Blue Unique ID:

* 26 uppercase letters (A-Z)
* 10 digits (0-9)



This means that there are a total of 8.08×10^35 possibilities or, to be more accurate: 808,281,277,464,764,060,643,139,600,456,536,293,376

\
**To put this into comparison, for every grain of sand on Earth (7.5\*10^18), there are tens of quadrillions (1.08×10&17) of Unique IDs!**
{% endhint %}



Project administrators can configure a custom prefix for all Unique IDs.

<figure><img src="../../.gitbook/assets/UniqueID prefix.png" alt=""><figcaption><p>Project Administrators can set an optional prefix for all Unique IDs</p></figcaption></figure>

\
