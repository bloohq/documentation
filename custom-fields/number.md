# Number

The Number custom field allows capturing numeric data values within configured minimum and maximum boundaries.

<figure><img src="../../.gitbook/assets/number custom field.png" alt=""><figcaption></figcaption></figure>

[Project Administrators](../user-management/roles/project-administrator.md) can define the allowed range when setting up the Number custom field. The system will validate entries to ensure they are within the permitted minimum and maximum values.
