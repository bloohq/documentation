# Star Rating

The Star Rating custom field allows capturing ratings and scores in Blue records using a familiar star-based rating system. This provides an intuitive way to collect and visualize evaluative data within projects. The universally recognizable star format makes collecting ratings intuitive for respondents while allowing nuanced scoring.

<figure><img src="../../.gitbook/assets/star rating custom field.png" alt=""><figcaption><p>Use Star Ratings custom field to rate records</p></figcaption></figure>

Star ratings provide an intuitive format for gathering subjective metrics across many scenarios. In customer service, they enable capturing satisfaction with support experiences, product quality, and overall brand perception. For human resources, they facilitate employee feedback initiatives to evaluate managers, company culture, policies, and internal processes. Star ratings are also ideal for performance management by allowing both employees and managers to score output, completed deliverables, and achievement of key performance indicators. Even in recruiting, star ratings can standardize how applicants, interviews, and new hires are graded and ranked. Beyond internal use cases, they are familiar to external users for scoring products, businesses, destinations, and services for review platforms. With a universally recognizable visual format and the ability to specify a nuanced range through partial stars, the custom field empowers granular ratings tailored to diverse evaluation needs.

## Configuration

Administrators can configure the number of stars in the rating scale, with up to 15 stars supported. Partial stars, displayed as halves or quarters, are also allowed for fine-grained ratings.

<figure><img src="../../.gitbook/assets/star rating custom field setting.png" alt=""><figcaption><p>Star Ratings can be set from 1 to 15</p></figcaption></figure>

The average rating can be calculated across records and charted in [Dashboards](../dashboards/), providing high-level insights.

<figure><img src="../../.gitbook/assets/star rating database.png" alt=""><figcaption><p>Star Ratings can provide a quick and visual way to view quality </p></figcaption></figure>
