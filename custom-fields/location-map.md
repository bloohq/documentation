# Location / Map

The Location / Map custom field allows you to capture geographic coordinates and display locations visually on a map within records in Blue. This is useful for associating records with real-world places and spaces.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/map custom field .png" alt=""><figcaption></figcaption></figure>

</div>

The Location / Map custom field is useful for a variety of use cases, including:

* Mapping customer or client locations for sales and service regions
* Pinpointing event venues, job sites, or asset locations for field operations
* Tracking shipment origins and destinations in supply chain workflows
* Mapping real estate listings for properties available for sale or rent
* Identifying hospital, clinic, or care home addresses in healthcare systems
* Locating equipment deployment sites for installation and maintenance workflows
* Showing branch or facility locations for internal operations and analysis
* Displaying resource deposits, land plots, or other geographic assets
* Associating records with geographical regions, sales territories, service areas, etc.

### Adding Locations

When adding a location, users can either:

* Manually enter latitude and longitude coordinates
* Search for an address or location name, which will auto-populate the coordinates
* Click on the map to drop a pin and select the desired location

\
Records containing a location can be viewed collectively on the [Map view](https://claude.ai/views/map.md) in Blue. On the Map, the pin color for each record corresponds to the color of its primary tag.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/map view.png" alt=""><figcaption></figcaption></figure>

</div>

