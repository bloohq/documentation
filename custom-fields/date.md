# Date

The Date custom field enables you to associate key dates and times with records in Blue. This can help track important events related to each record, like start dates, due dates, completion times, and more.

<figure><img src="../../.gitbook/assets/blue date field.png" alt=""><figcaption><p>Select precise dates and time ranges </p></figcaption></figure>

Using the interactive date picker, you can easily select exact dates, times, and ranges — with support for past, present, or future dates.

{% hint style="warning" %}
Blue currently support only dd/mm/yyyy format, but we are working on making a user option to support US dates (mm/dd/yyyy)
{% endhint %}

Because multiple Date fields can be added to each record, you can capture every significant date related to a record. For example, in a sales context, you may use separate Date fields to track the first contact date, product demo date, proposal sent date, contract signed date, and payment received date. This provides clarity into the progression of deals from initial outreach to close.

Once added, dates can be filtered across projects using Blue's advanced capabilities, enabling date based reporting. You can also optionally display custom Date fields on project calendars for visual timeline representation [and sync this data to your personal or work calendar. ](../integrations/calendar-sync.md)
