# Multiple Line Text

The Multiple Line Text field allows users to enter longer, multi-paragraph text content into records in Blue. It provides:

* No character restrictions, unlimited text length
* Support for formatting like bold, italics, hyperlinks
* Line breaks, spacing, text alignment

<figure><img src="../.gitbook/assets/CleanShot 2024-01-22 at 15.17.35@2x.png" alt=""><figcaption></figcaption></figure>

**Use Cases**

This field is ideal for situations like:

* Descriptions - product specs, task details, project scopes
* Notes & instructions
* Messages - updates, explanations
* Content - articles, blogs, guides
* Summaries & reports
* Email content
* Feedback & reviews

Essentially, any type of longer text content can be entered.

