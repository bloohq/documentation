# File

The File custom field enables uploading and attaching files directly to records in Blue. Virtually any file type is supported, including documents, spreadsheets, images, PDFs, and more.

<figure><img src="../../.gitbook/assets/blue file custom field.png" alt=""><figcaption><p>Structured file uploads ensure that everyone has visibility on the related files </p></figcaption></figure>

Each file field allows uploading multiple files, with each file up to 5GB in size. Uploaded files, such as images, PDFs, and Word docs, can be previewed if the file type is supported. All project users can download the attached files to access relevant record information and content.

<figure><img src="../../.gitbook/assets/blue file custom field preview.png" alt=""><figcaption><p>Users can preview and download files in the File custom field</p></figcaption></figure>

Uploaded files are shared project-wide under the ['Files' tab](../projects/file-management.md). However, custom user roles with restricted record access will not see those files. The project Files tab supports folder creation to organise files, but files cannot be sorted into these folders from within the custom field itself.

{% hint style="danger" %}
File version history is not maintained — new uploads do not replace existing files. Files are also not included in CSV export reports.
{% endhint %}

## Forms Integration

The File custom field [seamlessly integrates with Forms](../projects/forms.md), enabling external users to upload files when submitting information. For example, candidates can attach CVs and cover letters to job application forms, customers can provide images and examples for creative project requests, or clients can submit paperwork and documentation through client intake forms. By including File custom fields on forms, external stakeholders can provide important files and assets to enrich the records generated from form submissions. This delivers the benefit of collected content without requiring external users to have accounts within Blue.

## API File Access

Like any other system file, files uploaded via the File custom field can be accessed and managed through the [Blue API.](../integrations/api.md) When files are attached to records using the File field, those files become available through the standard file-related API calls. Developers can leverage the full suite of API capabilities to programmatically download, upload, organize, and integrate files from the File custom field. Whether automatically processing uploaded assets or appending metadata, the API enables seamless integration by providing unified access to all user-added files in Blue.

By attaching frequently used documents and assets, File fields eliminate searching through multiple locations to find the content linked to a record. This delivers quick, centralized access to the files you need.

\
