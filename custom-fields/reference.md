# Reference

Reference custom fields allow you to create relationships between records across different projects in Blue. A Reference field provides a dropdown populated with records from a chosen source project, enabling cross-project connections. Reference custom fields work alongside [Lookup custom fields](lookup.md) to bring information from one project into another. This is extremely powerful because it unlocks many use cases within Blue.

Try it with our interactive demo or scroll down to read more.

{% @arcade/embed flowId="Z2IO9SIb4K0bxA0ssrKh" url="https://app.arcade.software/share/Z2IO9SIb4K0bxA0ssrKh" fullWidth="true" %}

Let's review an example:

{% hint style="info" %}
ACME Company uses Blue's Reference and Lookup custom fields to create an interconnected data ecosystem across separate Customer, Sales, and Inventory projects. Customer records in the Customers project are linked via Reference to sales transactions in the Sales project, enabling [Lookup custom fields](lookup.md) to bring in associated customer details like phone number and account status directly into each sales record; additionally, the inventory items sold can display in the sales record through a Lookup field referencing Quantity Sold data from the Inventory project; finally, inventory withdrawals are connected to relevant sales via a Reference field in Inventory pointing back to the Sales records, providing full visibility into which sale triggered the inventory removal. By leveraging References and Lookups, ACME links customers, sales, and inventory into an integrated 360-degree view across projects.
{% endhint %}

## Configuration

When creating a Reference field, the [Project Administrator](../user-management/roles/project-administrator.md) selects the specific project that will provide the list of reference records.

Next, they configure whether the field will allow:

* Single select: Choose one reference record
* Multi-select: Choose multiple reference records

Finally, a filter can be set to allow users to select only records that match the filter. &#x20;

<figure><img src="../.gitbook/assets/reference custom field.png" alt=""><figcaption><p>Setting up a Reference custom field</p></figcaption></figure>

Once the Reference custom field has been set, users can select a specific record from the dropdown menu:

<figure><img src="../.gitbook/assets/reference drop down select.png" alt=""><figcaption><p>Selecting records from another project</p></figcaption></figure>

## Track record referenced.&#x20;

Sometimes when you have records referenced in multiple projects, you want to instantly see where they've been referenced all in one place. You can do that by setting up a 'Referenced By' custom field.&#x20;

This allows you to see where your record has been referenced in different projects so that you have a trace of where it has been referenced. With just one click, you will be taken to that specific project instantly.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-07-01 at 14.41.48@2x.png" alt=""><figcaption></figcaption></figure>

The referenced by field will automatically populate with all the records to show where it has been referenced

<figure><img src="../.gitbook/assets/CleanShot 2024-07-01 at 13.45.12@2x (1).png" alt=""><figcaption></figcaption></figure>

This feature is particularly useful when you want to see your client contact list being referenced across multiple projects and providing a quick and easy way to track record usage across projects.&#x20;



To remove this referenced by from your record:&#x20;

1. Go to "Manage Fields"
2. Click the red bin "Referenced By" field to delete.&#x20;
