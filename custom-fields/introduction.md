# Introduction

Custom fields are a way to create a custom data structure within Blue that perfectly matches your organisational requirements.  By creating custom fields for your structured data instead of using comments or descriptions, you leverage the full power of Blue as a work platform. The structured data can become your one source of truth.&#x20;

Our [API](../integrations/api.md) and [Webhooks](../integrations/webhooks.md) fully support custom fields, so by using custom fields in Blue, you are creating a structured data API that perfectly matches your data.&#x20;

Use our interactive guide below to learn more, or scroll down and read the details.

{% @arcade/embed flowId="C8hCmccWGfwEdwfAbaKx" url="https://app.arcade.software/share/C8hCmccWGfwEdwfAbaKx" fullWidth="true" %}

## Key Points

By default, each record in Blue comes with specific fields:

* Record Name
* [Assignee](../records/assignees.md)
* [Date](../records/dates.md)
* [Tag](../records/tags.md)
* [Dependencies](../records/dependencies.md)&#x20;

But you can choose from numerous different additional fields that you can add to all records within a project. For instance, this could be a currency custom field for "Sales Price" in a sales project.

<div data-full-width="true">

<figure><img src="../.gitbook/assets/blue custom fields.png" alt=""><figcaption><p>Each custom field is an additional column in your project database</p></figcaption></figure>

</div>

Custom fields are tightly integrated into Blue. Using the data contained within each custom field, you can [build work intake forms](../projects/forms.md), [import](../integrations/csv-import.md) and [export](../integrations/csv-export.md) data,  [filter data](../views/filters.md), and [create reporting dashboards](../dashboards/).&#x20;

Custom fields can be subdivided into two main types:

* **Editable Custom Fields:** These are custom fields where users can input data into the field. For instance, an [Email custom field ](email.md)allows users to input an email address, or a [Single Select custom field](single-select.md) allows users to choose from one option in the provided dropdown.&#x20;
* **Read-Only Custom Fields:** These are fields that only display data. For instance, the [Unique ID custom field ](unique-id.md)displays a randomly generated unique identifier for your record, and the [Formula custom field](formula.md) displays the result of a calculation based on values from other custom fields.&#x20;

## Creating Custom Fields

Only [Project Administrators](../user-management/roles/project-administrator.md) can create custom fields to ensure that the integrity of the data structure of the project is kept intact. You can create and manage custom fields by opening a record in the project and clicking on "Add More Fields"&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/add fields.png" alt=""><figcaption><p>Use the "Add More Fields" button within a record to add custom fields to a project. </p></figcaption></figure>

</div>

{% hint style="info" %}
**Custom Fields Are Project-Wide**

When you create a new custom field, that custom field is available in all records within the specific project. Each record can then have different values for that custom field. For instance, the record "Customer 1" may have a value of $2000 for the "Sales Price" custom field, while the record "Customer 2" has a value of $15,000 for the "Sales Price" custom field.&#x20;

Utilizing custom fields across a project significantly enhances data consistency and management efficiency. By standardizing the information structure, custom fields ensure that all records are uniformly organized, making data tracking and analysis more streamlined and reliable.
{% endhint %}

## Deleting Custom Fields

To delete a custom field, open any record and click on "Add More Fields. " You will be presented with a list of custom fields in your project. Next to each custom field, there will be a red bin icon to delete that custom field.

<figure><img src="../.gitbook/assets/delete custom field (1).png" alt=""><figcaption><p>The delete icon for custom fields</p></figcaption></figure>

You can click on this icon to delete the custom fields from the project. You'll have a confirmation message, and then you can confirm and delete the custom field.&#x20;

{% hint style="danger" %}
**Deleting Custom Fields is Project-Wide and Permanent**&#x20;

When you delete a custom field, this deletes the custom field from all records within that project. All the data contained within that custom field will be permanently deleted and is not recoverable.&#x20;
{% endhint %}

## User Permissions

One of the most powerful features of Blue is that we provide row-level permissions for custom fields. This allows you to set which user groups can view or edit individual custom fields. [This can be set up by creating a custom user role.](../user-management/roles/custom-user-roles.md)

<div data-full-width="true">

<figure><img src="../.gitbook/assets/custom field user roles.png" alt=""><figcaption></figcaption></figure>

</div>

