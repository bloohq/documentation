# Lookup

Lookup custom fields allow you to import data from records in other projects, creating one-way visibility. While [Reference custom fields](reference.md) establish connections across projects, Lookups use those connections to surface selected data.

Lookup custom fields are always read-only and are connected to a specific Reference custom field. When a user selects one or more records using a Reference custom field, the Lookup custom field will show data from those records.&#x20;

Lookups can show data such as:

* **Created at:** When the referenced record was created.&#x20;
* **Updated at:** The last date and time when the referenced record was updated in any way.
* **Due Date:** The reference record's start/due date and time.
* **Description:** The description field of the referenced record.
* **List:** Shows the list that the referenced record belongs to.
* **Tag:** Any tags that the referenced record is tagged with.
* **Assignee:** Any assignees to the referenced record.
* **Any Custom Field:** Any supported custom field from the referenced record.

<figure><img src="../.gitbook/assets/lookup list.png" alt=""><figcaption><p>Lookup fields have many options available</p></figcaption></figure>

## Lookup a Lookup&#x20;

You can create custom fields to store and reference data across multiple different projects. These custom fields are called "Lookup" fields.

Let's consider three projects:

* **Project A**
* **Project B**
* **Project C**

Suppose Project B contains some critical information or resources that are relevant to both Project A and Project C.

Instead of duplicating the information from Project B into Project A and Project C, follow these steps:

* In Project A, create a "Reference" custom field to establish a connection with the relevant record from Project B.
  * This Reference custom field allows you to select and link to the specific record in Project B that you want to reference and lookup.
* Once the Reference field is set up in Project A, create a "Lookup" custom field in Project C.
  * This Lookup custom field will be connected to the Reference custom field in Project A.
  * When you select records using the Reference custom field in Project A, the Lookup custom field in Project C will automatically display relevant data from those referenced records in Project B.

## Benefits of using Lookup a Lookup:

* You don't need to directly link Project C to Project B.
* Project C can leverage the connection established between Project A and Project B through the Reference and Lookup fields.
* Any changes or updates made to the relevant records in Project B will automatically reflect in the Lookup custom field data displayed in Project C.
* No need for manual updates or duplication of information across projects.

