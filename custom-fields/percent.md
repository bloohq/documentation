# Percent

The Percent custom field allows capturing and calculating percentage values within Blue records. This enables progress tracking, performance measurement, and data visualization based on percentages.

<figure><img src="../../.gitbook/assets/custom field percentage.png" alt=""><figcaption></figcaption></figure>

The Percent custom field comes with robust validation to support accurate data. When entering values, it checks that the number is within the standard 0-100% range expected for percentages. This prevents invalid data from being introduced.

A key feature is the flexible configuration of decimal precision. You can set the field to capture percentages to as many decimal places as required based on your needs. This level of customization enables percentage-based data tracking at whatever level of granularity your use case requires.

To simplify calculations, the Percent field integrates directly with [Formula custom fields.](formula.md) This makes automating progress bars, ratios, rates, and other common percentage-based metrics easy. The calculations happen behind the scenes, displaying the output percent value.

When exporting data via [CSV](../integrations/csv-export.md) or [API](../integrations/api.md) integrations, the field maintains its formatted percentage display, including any decimals. This ensures the Percent data is exported accurately while retaining user-configured precision and formatting.
