# Single Select

The Single Select custom field allows you to create a dropdown list of options that users can choose from when inputting data into records. You can define up to 10,000 options with labels and colors.

<figure><img src="../.gitbook/assets/blue single select custom field.png" alt=""><figcaption><p>Creating a Single Select custom field</p></figcaption></figure>

&#x20;

{% hint style="info" %}
Adding options one-by-one can become tedious for select dropdowns. Fortunately, options can be added in bulk by copying and pasting from a bulleted/line-separated list or spreadsheet column. Copy and paste the list of choices into the options creation dialogue in Blue. This will automatically create an option for each line or bullet point, streamlining configuration.
{% endhint %}

A search-as-you-type interface makes finding the ideal selection easy, even with thousands of options.

<figure><img src="../.gitbook/assets/blue single select search.png" alt=""><figcaption><p>Searc-as-you-type to select an option</p></figcaption></figure>

After creation, project administrators can still edit the list of options as needed, including reordering, additions, or removals.

Since only one option can be selected per record, this field works well for categorical data like product types, status labels, appraisal grades, risk levels, and more.

<figure><img src="../.gitbook/assets/blue single selects.png" alt=""><figcaption><p>Create your own custom options to map your project or process perfectly</p></figcaption></figure>

The Single Select field delivers a tailored dropdown designed for fast selection from extensive predefined options. Defining customized options and labels aligns the field closely with your workflows and terminology. These dropdowns integrate tightly across Blue. [Project Administrators](../user-management/roles/project-administrator.md) can restrict view and edit access by [user role](../user-management/roles/custom-user-roles.md), external [forms](../projects/forms.md) can leverage Single Selects for standardized input, and [dashboards](../dashboards/) can filter by Select values. Whether collecting categorical data or standardizing terminology, the Single Select field provides an optimized experience for managing customized dropdowns aligned with your business needs.
