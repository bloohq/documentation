# URL / Link

The URL/Link custom field allows you to store links and web addresses within your records in Blue. When a URL is entered into this field, the system will validate that it is a properly structured link.

<figure><img src="../../.gitbook/assets/blue url custom field.png" alt=""><figcaption><p>The URL/Link custom field in Blue</p></figcaption></figure>

Each record can contain one link, stored in a single URL/Link custom field. The link is clickable, enabling you to navigate directly to the web page or resource by clicking on it. For convenience, it will automatically open in a new browser tab.

<figure><img src="../../.gitbook/assets/blue url open new tab.png" alt=""><figcaption><p>You can open URLs in a new tabi in one click</p></figcaption></figure>

This field is extremely useful for linking to cloud-based documents like Google Docs, Word files, Excel spreadsheets, etc. Storing these links allows one-click access to relevant files associated with each record.

The URL/Link field is great for additional use cases such as:

* Storing customer or partner websites
* Capturing links associated with records
* Creating references to webpages

With its streamlined interface and built-in link capabilities, the URL/Link custom field enables you to enrich records with contextual web resources and access them instantly with a click.

\
