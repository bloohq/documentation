# Phone Number

The Phone Number custom field allows you to store numeric phone numbers for records in your Blue projects. This can help centralize important contact information for clients, vendors, or other entities in your records.

<figure><img src="../../.gitbook/assets/blue phone number active.png" alt=""><figcaption></figcaption></figure>

When adding a phone number, the field will automatically validate the length based on the expected format for the country. For example, a US number will be validated to 10 digits. The numbers are stored without any formatting, like dashes or parentheses.

The phone number field also supports extensions. When adding an extension, there is a dropdown menu with all countries and their expected extension formats. You can search this dropdown by extension number or country name to quickly find and select the appropriate extension format. This allows you to set the extension format manually based on the specific country.

<figure><img src="../../.gitbook/assets/phone number custom field.png" alt=""><figcaption><p>Search for country-codes right within Blue</p></figcaption></figure>

A key feature of the Phone Number field is built-in click-to-call functionality. Hovering over a stored phone number will reveal a clickable phone icon. Clicking this icon will automatically dial the number using your default calling application or service on your device. This seamless experience enables you to initiate calls to your connections directly from within Blue.

<figure><img src="../../.gitbook/assets/blue call button.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Each record can store one phone number. The field does not differentiate between mobile and landline numbers. The focus is on capturing the primary number for that particular entity or contact.
{% endhint %}

The Phone Number field delivers an optimized experience for easily storing and accessing important contact information from your records. Automated validation, click-to-call, and a streamlined single-number design enable you to connect with your contacts in just a click.
