# Known Issues

This is a list of known issues that we are currently fixing. This is updated every Monday. The internal references are links to specific issues within Blue's internal product tracker.&#x20;

* When creating a project template, the project name sometimes appears three times in the sidebar until Blue is refreshed. ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clr7abdg909o6x41v7d7nv3si))
* Cannot move records that have an active dependency to another project ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clqyji8xx0135ou1v46gbmfbj))
* Unable to delete active chat ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clqyryljr00iuq41v6b10n51e))
* Search results date display is corrupted ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clrjbpe1o00snre1va8s9kraj))
* Forms in copied projects cannot receive submissions ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clrrleix00cvere1vn0yo1ehw))
* Bar Charts do not follow list order from projects ([internal reference](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clrtkx29h0ifqre1v5tnru9ru))
* &#x20;Issue with Disabling Required Checkbox Field Across Multiple Forms for Template-Based Projects ([internal reference)](https://beta.app.blue.cc/company/blue/projects/product-28/todo/clsfj3mej0laabniml0sqtykv)

