# Duration

The Duration custom field captures the time between two specified events, enabling you to track durations in your records.

The Duration field is extremely valuable for tracking business processes and workflow time intervals. Some examples include:

* Monitoring service ticket or request resolution time for internal SLAs
* Calculating sales cycle length from prospecting to closed deal
* Tracking order processing and fulfilment timelines
* Measuring manufacturing throughput speed from order to delivery
* Determining patient wait times between intake and doctor visits
* Analyzing campaign lead nurturing time from prospect to customer
* Tracking employee onboarding duration from job offer to full productivity

The Duration field enables process optimization, benchmarking, and reporting by quantifying the time between key events. It brings clarity to workflows by highlighting inefficiencies and progress over time. Whether improving customer service, shortening sales cycles, or streamlining operations, the Duration field delivers configurable time tracking.

<figure><img src="../../.gitbook/assets/time duration custom field.png" alt=""><figcaption><p>Flexibility configure Start and End events </p></figcaption></figure>
