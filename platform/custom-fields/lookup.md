# Lookup

Lookup custom fields allow you to import data from records in other projects, creating one-way visibility. While [Reference custom fields](reference.md) establish connections across projects, Lookups use those connections to surface selected data.

Lookup custom fields are always read-only and are connected to a specific Reference custom field. When a user selects one or more records using a Reference custom field, the Lookup custom field will show data from those records.&#x20;

Lookups can show data such as:

* **Created at:** When the referenced record was created.&#x20;
* **Updated at:** The last date and time when the referenced record was updated in any way.
* **Due Date:** The reference record's start/due date and time.
* **Description:** The description field of the referenced record.
* **List:** Shows the list that the referenced record belongs to.
* **Tag:** Any tags that the referenced record is tagged with.
* **Assignee:** Any assignees to the referenced record.
* **Any Custom Field:** Any supported custom field from the referenced record.

<figure><img src="../../.gitbook/assets/lookup list.png" alt=""><figcaption><p>Lookup fields have many options available</p></figcaption></figure>

