# Email

The Email custom field lets you store email addresses within your Blue records. Adding an email will validate the format to ensure a properly structured email is entered.

<figure><img src="../../.gitbook/assets/blue email custom field.png" alt=""><figcaption><p>The email custom field in Blue</p></figcaption></figure>

Each record can contain one email address stored in a single Email custom field. There is functionality to click on the stored email to automatically open your default email application, allowing you to compose emails to the associated contacts swiftly.

<figure><img src="../../.gitbook/assets/blue email send.png" alt=""><figcaption><p>Click to open your default email software</p></figcaption></figure>

The Email field seamlessly integrates with [Blue's Email Automation capabilities](../automations/actions/email-automations.md). Email workflows can be triggered, and information from records can be merged into emails using the {merge} tag syntax.&#x20;

When creating an Email custom field, you can name it contextually for your specific needs, such as "Support Email" or "Sales Email." However, only one field can be used per record.

With automated validation, one-click email access, and workflow integration, the Email field lets you store and connect with important contacts directly from relevant records in Blue.
