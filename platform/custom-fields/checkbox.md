# Checkbox

The Checkbox custom field provides a checkbox that can be used for a variety of purposes, including approvals or [to trigger an automation](../automations/)

<figure><img src="../../.gitbook/assets/checkbox custom field.png" alt=""><figcaption><p>Using Checkbox custom fields for approvals </p></figcaption></figure>

Approvals are a specifically good use case for the Checkbox custom field because of Blue's audit capabilities. Each time the checkbox is ticked or unticked, Blue logs the action in a permanent manner:

<figure><img src="../../.gitbook/assets/approval history.png" alt=""><figcaption><p>Blue logs the person, action, and time for every change.</p></figcaption></figure>

