# Country

The Country custom field enables you to associate records in Blue with specific countries. It provides a dropdown list containing every recognized country in the world, allowing you to select the relevant country for each record.

<figure><img src="../../.gitbook/assets/blue country field.png" alt=""><figcaption><p>The Country custom field will show a dropdown of all recognised countries</p></figcaption></figure>

The dropdown menu displays the country name and national flag icon, providing clear visual identification. You can select multiple countries per record if needed.

<figure><img src="../../.gitbook/assets/blue country selection.png" alt=""><figcaption><p>Users can select multiple countries at the same time</p></figcaption></figure>

To quickly locate a country, start typing the name - the dropdown will instantly filter to matching countries in a search-as-you-type interface. This makes finding even lesser-known country names fast and easy.

<figure><img src="../../.gitbook/assets/blue country field search.png" alt=""><figcaption><p>Instantly search for any country </p></figcaption></figure>

The full list is organized alphabetically for scanning ease. Once selected, the chosen country or countries will be displayed using the national flag icons for ongoing visual reference.

Potential uses for the Country field include:

* Capturing customer, partner or vendor locations
* Associating records with related geographical regions
* Filtering data by country for analysis and reporting
* Tracking international shipping destinations

With robust support for all global countries and a refined searchable interface, the Country dropdown field enriches records with relevant geographical data.
