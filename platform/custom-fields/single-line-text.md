# Single Line Text

The Single Line Text field enables you to add unformatted text strings to your records in Blue. No character limit exists, so you can enter text of any length into this field. However, the text is constrained to a single line and does not support formatting options like bold, italics, or links.

<figure><img src="../../.gitbook/assets/single line custom field.png" alt=""><figcaption><p>A flexible way to capture structured text data</p></figcaption></figure>

The text field is highly flexible, allowing entry of both letters and numbers, including special characters. There are no restrictions on which characters can be input. The system does not apply any validations or masking to the user-entered text.

The Single Line Text field is great for capturing all kinds of information, including names, descriptions, model numbers, SKUs, addresses, notes,  IDs, and more. You can use this flexible field to store names of clients, vendors or contacts, indicate inventory codes and identifiers,  make general short notes about records,  or store unique IDs for database relationships and lookups. The single line text field enables you to efficiently enter any important text-based information related to your records in Blue.

You can leverage Blue's [advanced record filtering capabilities](../views/filters.md) to find and filter records containing specific text within the Single Line Text field to locate matching records.

With its simple single-line design, unfettered text capacity, and integration with filtering, the Single Line Text field delivers an optimized experience for capturing unstructured data. It provides the flexibility to store any information as needed on a per-record basis.

