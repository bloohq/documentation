# Multiple Select

The Multiple Select custom field functions identically to the [Single Select custom field](single-select.md), with one key distinction — the ability to make multiple selections per record.

The Multiple Select custom field allows you to create a dropdown list of options that users can choose from when inputting data into records. You can define up to 10,000 options with labels and colors.

<figure><img src="../../.gitbook/assets/multiple select color option.png" alt=""><figcaption><p>Choosing colors for options within a Multiple Select custom field</p></figcaption></figure>

Allowing multiple selections is ideal for capturing:

* Taxonomies - Classifying records into multiple categories (i.e. tagging records with all relevant labels)
* Composite Data - Combining options to describe complex records (i.e. selecting all applicable product properties)
* Audit Trails - Tracking changes over time (i.e. logging status updates to a record)

{% hint style="info" %}
Adding options one-by-one can become tedious for select dropdowns. Fortunately, options can be added in bulk by copying and pasting from a bulleted/line-separated list or spreadsheet column. Just copy the list of choices and paste them into the options creation dialogue in Blue. This will automatically create an option for each line or bullet point, streamlining configuration.
{% endhint %}

The flexibility of multiple selections expands how customizable dropdowns can structure project data.

A search-as-you-type interface makes finding the ideal selection easy, even with thousands of options.

<figure><img src="../../.gitbook/assets/multiple select search.png" alt=""><figcaption></figcaption></figure>



After creation, project administrators can still edit the list of options as needed, including reordering, additions, or removals.

The Multiple Select field delivers a tailored dropdown designed for fast selection from extensive predefined options. Defining customized options and labels aligns the field closely with your workflows and terminology. These dropdowns integrate tightly across Blue. [Project Administrators](../user-management/roles/project-administrator.md) can restrict view and edit access by [user role](../user-management/roles/custom-user-roles.md), external [forms](../projects/forms.md) can leverage Multiple Selects for standardized input, and [dashboards](../dashboards/) can filter by Select values. Whether collecting categorical data or standardizing terminology, the Multiple Select field provides an optimized experience for managing customized dropdowns aligned with your business needs.
