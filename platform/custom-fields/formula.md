# Formula

The Formula custom field allows you to create calculations that derive new values and insights from your existing record data. To build a formula, you can reference values from other fields in the same record using {curly bracket} notation.

<figure><img src="../../.gitbook/assets/blue formulas.png" alt=""><figcaption><p>Use Formula fields to automatically calculate results from other fields</p></figcaption></figure>

Formulas can use data from the following custom fields:

* [Currency](currency.md)
* [Star Rating](star-rating.md)
* [Number](number.md)
* [Percent](percent.md)

## Supported Operations

You can then perform operations on these values using functions:

* **AVERAGE**: Calculate the mean of a set of numbers.
* **AVERAGEA**: Determine the average, treating text as zero.
* **COUNT**: Count the number of entries in a number field.
* **COUNTA**: Count the number of non-empty values.
* **MAX**: Find the maximum value in a set.
* **MIN**: Identify the minimum value in a set.
* **MINUS**: Subtract one value from another.
* **MULTIPLY**: Multiply two values.
* **SUM**: Add up a series of numbers

<figure><img src="../../.gitbook/assets/blue formula fields.png" alt=""><figcaption><p>Example: Creating a profit calculation based on sales price and cost</p></figcaption></figure>

your formulas' complexity or number of operationsThere are no restrictions on the complexity or number of operations in your formulas. They will validate correctly as long as the syntax is valid. Formulas only reference fields within the same record, not external data.

### Configure Display

The final step to creating a formula custom field is deciding how to display the data.&#x20;

Formula fields come with three display options:

1. **Number:** Opt for a straightforward numeric display, with the added functionality to set the precision level. This means you can dictate how many decimal places are shown, allowing for precision that matches the detail level required for your data analysis.
2. **Currency:** Select this option to represent values in monetary terms. It is ideal for financial metrics such as budget tracking, revenue, costs, or other financial statistics. The currency format will apply appropriate local formatting and currency symbols with s[upport for numerous global currencies. ](currency.md)
3. **Percentage:** Use this format to express values as percentages, perfect for showing completion rates, growth metrics, or other proportional data. This option automatically converts the calculated value into a percentage format, providing an instant understanding of ratios and shares.

<figure><img src="../../.gitbook/assets/stat card display options.png" alt=""><figcaption><p>Display options for formula fields</p></figcaption></figure>

Users can view the underlying formula logic by hovering over the field.&#x20;

<figure><img src="../../.gitbook/assets/blue formula hover.png" alt=""><figcaption></figcaption></figure>

While editable formulas will update results in real time if referenced values change, exported data will only contain the calculated formula results rather than the formulas themselves.\


\
