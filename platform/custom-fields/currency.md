# Currency

The Currency custom field allows you to store monetary values and amounts within your records in Blue.&#x20;

When creating a new currency custom field, you can choose the currency:

<figure><img src="../../.gitbook/assets/blue select currency.png" alt=""><figcaption><p>You can select from numerous global currencies when setting up the Currency custom field</p></figcaption></figure>



### The full list of supported currencies:

* UAE Dirham (د.إ)
* Australian Dollar (A$)
* Brazilian Real (R$)
* Canadian Dollar (C$)
* Swiss Franc (CHF)
* Chilean Peso (CLP)
* Colombian Peso (COP)
* Yuan (¥)
* Costa Rican Colón (₡)
* Czech Koruna (Kč)
* Danish Krone (kr)
* Egyptian Pound (E£)
* Euro (€)
* Hong Kong Dollar (HK$)
* Hungarian Forint (Ft)
* Indian Rupee (₹)
* Indonesian Rupiah (Rp)
* Israeli New Shekel (₪)
* Yen (¥)
* Cambodian Riel (៛)
* South Korean Won (₩)
* Malagasy Ariary (Ar)
* Mexican Peso (MX$)
* Malaysian Ringgit (RM)
* New Zealand Dollar (NZ$)
* Philippine Peso (₱)
* Romanian Leu (lei)
* Russian Ruble (₽)
* Polish Zloty (zł)
* Saudi Riyal (SR)
* Swedish Krona (kr)
* Singapore Dollar (S$)
* Thai Baht (฿)
* Turkish Lira (₺)
* New Taiwan Dollar (NT$)
* US Dollar ($)
* Vietnamese Dong (₫)
* Pound Sterling (£)
* Bitcoin (₿)
* Ethereum (Ξ)
* Pakistani Rupee (₨)
* Nigerian Naira (₦)
* Norwegian Krone (kr)
* South African Rand (R)
* Zambian Kwacha (ZK)

{% hint style="info" %}
If your local currency is not supported, please contact [**help@blue.cc**](mailto:help@blue.cc)**,** and we will gladly implement it.&#x20;
{% endhint %}

<figure><img src="../../.gitbook/assets/blue currency.png" alt=""><figcaption><p>Store financial information in the currency field</p></figcaption></figure>

When adding currency values, the field will format the number according to local conventions and display the relevant currency symbol. For example, $1,000.50 for US Dollars or €1.500,25 for Euros.

The currency field validates entries to ensure only valid currency amounts are captured. Once added to a project, the field will store amounts in the singular designated currency type. It does not provide automated currency conversion between multiple denominations.

This field delivers an optimized experience for tracking monetary data associated with records. The automated formatting removes the complexity of manually entering currency values in local formats, while validation protects data integrity.

Potential uses for the currency field include:

* Capturing transaction amounts
* Storing project budgets
* Tracking invoice totals
* Recording payment amounts
* Calculating revenue

With robust support for global currency formats and streamlined data entry, the Currency custom field enables you to centralize key financial information within your records in Blue.

{% hint style="info" %}
The currency custom field works extremely well with the[ dashboard functionality](../dashboards/), which you can use to report on project budgets, sales, and any other financial information.&#x20;
{% endhint %}

\
