# Reference

Reference custom fields allow you to create relationships between records across different projects in Blue. A Reference field provides a dropdown populated with records from a chosen source project, enabling cross-project connections. Reference custom fields work alongside [Lookup custom fields](lookup.md) to bring information from one project into another. This is extremely powerful because it unlocks many use cases within Blue.

Let's review an example:

{% hint style="info" %}
ACME Company uses Blue's Reference and Lookup custom fields to create an interconnected data ecosystem across separate Customer, Sales, and Inventory projects. Customer records in the Customers project are linked via Reference to sales transactions in the Sales project, enabling [Lookup custom fields](lookup.md) to bring in associated customer details like phone number and account status directly into each sales record; additionally, the inventory items sold can display in the sales record through a Lookup field referencing Quantity Sold data from the Inventory project; finally, inventory withdrawals are connected to relevant sales via a Reference field in Inventory pointing back to the Sales records, providing full visibility into which sale triggered the inventory removal. By leveraging References and Lookups, ACME links customers, sales, and inventory into an integrated 360-degree view across projects.
{% endhint %}

## Configuration

When creating a Reference field, the [Project Administrator](../user-management/roles/project-administrator.md) selects the specific project that will provide the list of reference records.

Next, they configure whether the field will allow:

* Single select: Choose one reference record
* Multi-select: Choose multiple reference records

Finally, a filter can be set to allow users to select only records that match the filter. &#x20;

<figure><img src="../../.gitbook/assets/reference custom field.png" alt=""><figcaption><p>Setting up a Reference custom field</p></figcaption></figure>

Once the Reference custom field has been set, users can select a specific record from the dropdown menu:

<figure><img src="../../.gitbook/assets/reference drop down select.png" alt=""><figcaption><p>Selecting records from another project</p></figcaption></figure>
