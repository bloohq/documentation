# Calendar

The Calendar view provides a visual overview of your records in a calendar format. This view makes it easy to track due dates, schedules, and temporal relationships across your records.

### Key Features

#### **Weekday Toggle**&#x20;

Turn the display of weekends on or off. Having them toggled off gives more visual space.

<figure><img src="../../.gitbook/assets/H3RQmPsp.gif" alt=""><figcaption></figcaption></figure>

#### **Persistent Filters**&#x20;

Any [filters](filters.md) you set will remain applied as you navigate across views.

<figure><img src="../../.gitbook/assets/mqDdAmF7.gif" alt=""><figcaption></figcaption></figure>

#### **Sync External Calendars**&#x20;

You can sync your Blue calendar with external calendars using supported integrations.

<figure><img src="../../.gitbook/assets/calendar sync.png" alt=""><figcaption><p>Click to copy the calendar sync URL and add it to your personal calendar</p></figcaption></figure>

You can read more about calendar sync here:

{% content-ref url="../integrations/calendar-sync.md" %}
[calendar-sync.md](../integrations/calendar-sync.md)
{% endcontent-ref %}



### Navigating Records on Calendar &#x20;

#### **Add New Dated Records**&#x20;

Click on any timeslot or date on the calendar to quickly create a new dated record. Input the details and the record will be scheduled at that time. For instance, click on next Thursday 25th to instantly schedule a "Client Call" record.

<figure><img src="../../.gitbook/assets/new calendar record.png" alt=""><figcaption></figcaption></figure>

#### **Drag & Drop Scheduling**&#x20;

Easily reschedule records by dragging them to different dates/times slots. Lengthen or shorten the duration of records by dragging the bottom edge. For example, drag a "Team Meeting" record to move it to Tuesday at 2 PM or extend a "Project Deadline" record by a few days.

### **Today & Current Date**&#x20;

Instantly jump to the current date and records scheduled for today by clicking the Today button. This makes keeping track of impending deadlines and appointments simple.

<figure><img src="../../.gitbook/assets/today calendar button.png" alt=""><figcaption><p>Press "Today" to instantly get taken back to the right month</p></figcaption></figure>

#### **Full Calendar Navigation**&#x20;

Change months and years or pick specific dates to jump to using the built-in calendar widget. Click the previous/next arrows to navigate year-by-year.  Use this full calendar control to view past records or schedule items far into the future.

<figure><img src="../../.gitbook/assets/calendar date navigation.png" alt=""><figcaption><p>Navigate years with ease</p></figcaption></figure>

**Click to Open**&#x20;

Click on any record to open and edit its details.



The Calendar provides a great high-level temporal view of your workflow. Combined with Blue's other views and records features, you get maximum flexibility in tracking and scheduling your projects!
