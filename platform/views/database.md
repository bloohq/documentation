# Database

The Database View presents your records in a spreadsheet-style interactive table. This view is ideal for managing large datasets, enabling powerful filtering, sorting, and editing capabilities.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/view database.png" alt=""><figcaption></figcaption></figure>

</div>

###

### **Grouping**&#x20;

Drag column headers to group records by specific properties.

<figure><img src="../../.gitbook/assets/database list group.png" alt=""><figcaption></figcaption></figure>

Once you have dragged a column to the header, you will see that your record database will be grouped:

<figure><img src="../../.gitbook/assets/database list grouping.png" alt=""><figcaption></figcaption></figure>



You can also multi-group using multiple columns:

<figure><img src="../../.gitbook/assets/eGqOsjr5.gif" alt=""><figcaption><p>Easily group by multiple columns to arrange your data</p></figcaption></figure>



### **Interactive Columns**&#x20;

Each column represents a custom field, with the ability to instantly edit data within cells. Rearrange column placement via drag-and-drop.

### **Infinite Scroll**&#x20;

* The database table enables infinite vertical scrolling to manage vast datasets spanning tens or hundreds of thousands of records.

### **Easily Access Record Details**&#x20;

* Right-click any record to open a details pane showing the full record information. This allows drilling down without having to open the full record.

### **Bulk Actions**&#x20;

If it's a number/formula field you can instantly see the total sum.&#x20;



### **Column Management**

You can click on a column header to sort all your records by that column:

<figure><img src="../../.gitbook/assets/FNS80EGv.gif" alt=""><figcaption><p>Sort by any column, and even multiple columns!</p></figcaption></figure>

You can also click on the settings icon on each column header to get various options presented to you:

<figure><img src="../../.gitbook/assets/manage columns database.png" alt=""><figcaption></figcaption></figure>

* **Pin Column:** You can chose to pin the column either to the right or left. This keeps the column always in few as you scroll horizontally.
* **Autosize This Column:** Ensures that the column width is enough to show all the data in the cells contained in that column
* **Autosize All Columns:** Same as _"Autosize This Column"_ but applies to _all_ columns
* **Group by Assignees:** Group all records by this column, [as discussed earlier in this guide.](database.md#grouping)

## Show/hide columns

You can easily decide which columns to hide and show when you are viewing the database:

<figure><img src="../../.gitbook/assets/oSOig5aK.gif" alt=""><figcaption></figcaption></figure>

{% hint style="danger" %}
Any changes you make to the view of the database only apply to your user. Other users will see the default database view unless they make their own changes to the view.&#x20;
{% endhint %}

With spreadsheet-style interactions, grouping, filtering, and editing tools, the Database View delivers powerful data management and analysis capabilities to handle even your largest datasets.

