# Map

* Maps give you a pin like of all records that have location&#x20;
* Map uses Custom location custom fields (link)
* Works in dark mode
* Filters work
* Primary tag links to pin color
* Click to get details, click agian to open record
* Zoom in and Out
* Go to your own location (but need to give access to location).
* Privacy notice — Blue does not store your location after your session
* Shift and drag to zoom in

The Map View visually plots records containing location data onto an interactive world map. This geographic perspective delivers unique insights and analysis opportunities.

**Key Features**

* **Location Tracking**: The Map leverages the [Location/Map custom field](../custom-fields/) to pull in latitude and longitude data associated with records.
* **Visual Identification**: Map pins are color-coded based on the record's primary tag, enabling intuitive recognition.
* **Detail Popups**: Clicking a pin reveals a popup with the record's name, project, list, tags, and assignees. Click again to open the full record.
* **Navigation Controls**: Zoom, pan, and snap to your current location.
* **Persistent Filters**: All [filters](filters.md) applied in other views are maintained on the Map.
* **Dark Mode Support**: Works seamlessly with system-level dark mode settings.

**Use Cases**

The Map view unlocks unique perspectives across use cases:

* Plot customer, partner or asset locations
* Visualize service regions, sales territories or market areas
* Track shipment origins and destinations
* Map event venues, job sites or inventory warehouses
* Identify facility locations for operations analysis
* Monitor field employee positions
* Pinpoint geographical assets and points of interest

With interactive navigation and built-in location tracking, the Map View surfaces new dimensions within your data.

\
