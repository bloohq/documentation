# @mentions

@mentions in Blue provide a way to directly notify other users of relevance to a task or discussion. By using the @ symbol followed by someone's name, you can tag them in a comment or record a description.

<figure><img src="../../.gitbook/assets/file-ubCAXsVYYF.gif" alt=""><figcaption><p>Easily @mention team members in any text area</p></figcaption></figure>

This triggers a notification for that user, alerting them of the mention. It brings their attention precisely where it is needed.

@mentions can be used to:

* **Delegate Tasks**: Assign work or responsibilities by @mentioning the owner.
* **Ask Questions**: Seek answers or input from experts by @mentioning them in your query.
* **Give Recognition**: Show appreciation by @mentioning teammates who have done great work.
* **Clarify Ownership**: Document who is responsible when @mentioning the owner in notes.
* **Notify Stakeholders**: Keep clients or managers in the loop by @mentioning them in updates.

{% hint style="info" %}
You can only @mention people who are in that particular project. If a person is not in the project, they will not appear in the drop-down suggestion list of people you can @mention.
{% endhint %}
