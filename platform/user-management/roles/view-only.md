# View Only

## **Introduction**

"View-Only Users" in Blue play a crucial role in projects where controlled access is required. This user type is designed for individuals who need to stay informed about the project's progress and status but do not require the ability to make changes or contribute to discussions.

**Role and Permissions**

View-Only Users are granted limited access that includes:

* **Read-Only Access**: They can view all records, tasks, and discussions within the project but cannot add, edit, or delete any content.
* **Observation and Monitoring**: Their role is primarily observational, allowing them to monitor project progress and understand the status of various tasks and milestones.

## **Limitations**

The restrictions for View-Only Users are significant to maintain the project’s security and integrity:

* **No Commenting or Editing**: Unlike Comment-Only Users, View-Only Users cannot participate in discussions or comment on tasks.
* **Restricted from Making Changes**: They cannot alter project settings, create new records, modify existing tasks, or access confidential project data beyond what is visible.

## **Ideal Use Cases**

This user role is particularly beneficial in situations such as:

* **Stakeholder Updates**: Allowing stakeholders to track the progress of projects they are interested in without the ability to interfere in the operational aspects.
* **Client Oversight**: Providing clients with the opportunity to monitor project developments while safeguarding the project from any unauthorized changes.
* **Audit and Compliance**: Enabling auditors or compliance officers to review project activities and histories without the risk of altering any information.

## **Setup**

To assign a View-Only User role in Blue, a Project Administrator should:

1. Navigate to the 'People' tab within the project settings.
2. Add the individual as a user, selecting the 'View-Only' role from the available options.

## **Conclusion**

View-Only Users are a vital component in maintaining transparency and accountability in Blue while protecting the project from unintended alterations. Their inclusion is key for scenarios requiring oversight and monitoring by parties not directly involved in the project’s active management or discussions.
