# Client

In Blue, "Client Users" are a unique type of user role designed to client, vendors in project  while having restricted access to other functionalities. This role is ideal for clients or vendors that needs contribute to conversations and provide feedback without the ability to create new projects or delete other members item but can still invite the same or below role access.&#x20;

## **Role and Permissions**

A Client User in Blue is primarily focused on feedback/review within the project. Their permissions include:

* **Adding members:** Client can invite other client roles or below (comment or view only)&#x20;
* **Adding Comments**: They can actively participate in discussions by adding comments to existing records or tasks.
* **Reading Access**: They have the ability to read all the comments and view the details within the project, keeping them informed and engaged.

## **Limitations**

While Client Users can engage in discussions, their access is limited in several ways:

* **No Deleting Capabilities**: They cannot delete  tasks.
* **Restricted Access**: Their access is confined to viewing and commenting, without the ability to alter project settings, add or modify records.&#x20;

**Ideal Use Cases**

This role is particularly useful in scenarios such as:

* **Client Feedback**: Allowing clients to provide input on specific tasks or projects without giving them full access.
* **External Collaboration**: Engaging with external partners or stakeholders who need to comment on ongoing work but do not require full project access.
* **Review and Approval Processes**: Facilitating feedback loops where input from various team members is required, but not full project participation.

## **Setup**

To set up a Client User in Blue, a Project Administrator needs to:

1. Go to the 'People' tab in the project settings.
2. Add the individual as a user and assign them the 'Client' role.

**Conclusion**

Client Users are an essential part of Blue’s collaborative environment, allowing for controlled participation in discussions and feedback processes. By integrating these users into your project, you can ensure that all necessary parties are involved in the conversation while maintaining the integrity and security of the project’s data and workflows.
