# Project Administrator

The Project Administrator is the most powerful role in a project.

&#x20;Users who have this role are able to:

* Edit any project setting
* [Import](../../integrations/csv-import.md) and [Export](../../integrations/csv-export.md) project data
* Remove any user — including other Project Administrators
* Manage [Custom Fields](../../custom-fields/)
* Create [Custom User Roles](custom-user-roles.md)
