# Custom User Roles

In Blue, user permissions are not just assigned individually but can be efficiently managed through user groups. This system enables [Project Administrators ](project-administrator.md)to group users with similar roles and needs, and then assign custom permissions to the entire group. It simplifies access management by allowing the definition of roles that align with the specific functions and access requirements of different teams or departments within a project.

With Blue, you can:

* **Group-Based Permissions**: Craft roles that apply to user groups, ensuring consistent access rights across team members sharing the same responsibilities.
* **Activity and Form Access**: Control group access to project activities and forms, thereby safeguarding sensitive information and focusing group efforts on relevant tasks.
* **Record Visibility**: Specify if a group can only view records assigned to them, streamlining their workflow and maintaining focus.
* **Invitation Capabilities**: Assign the power to invite new users to groups, streamlining team expansion and maintaining control over who can add members to the project.

<figure><img src="../../../.gitbook/assets/custom permissions 1.png" alt=""><figcaption><p>Easily toggle features and functions on and off on a per-user group basis. </p></figcaption></figure>

## **Custom Fields Access**

This is an extremely powerful feature of Blue, as it provides column-level access to data in Blue. You can determine whether a role can view or edit specific custom fields, crucial for maintaining data integrity and relevance. This can range from URL links to documents, designs, or time durations.

<div data-full-width="true">

<figure><img src="../../../.gitbook/assets/custom field user roles.png" alt=""><figcaption><p>You have granular controls over each custom field in your project</p></figcaption></figure>

</div>

## List Management

Assign specific permissions for each list within the project, such as viewing, editing, or deleting. This granularity allows you to tailor roles to precisely manage interactions with different lists, aligning access with the unique requirements of each project phase.

<div data-full-width="true">

<figure><img src="../../../.gitbook/assets/custom permissions list.png" alt=""><figcaption></figcaption></figure>

</div>



&#x20;**Managing Custom Roles**

Creating a custom role in Blue is a streamlined process aimed at enhancing project workflows. To begin, you simply provide a descriptive name and, if necessary, a brief description for the role, establishing its purpose and scope. Following this, you'll select from a range of permissions tailored to the role's responsibilities within the project. These permissions dictate what users in this role can view, edit, or delete, and could include access to specific project activities, forms, and custom fields. By delineating these permissions, you ensure that the role is equipped with the right tools and access levels to contribute effectively to the project.

**Implementing Custom Roles**

After crafting a custom role, you can then assign it to individuals or groups within your project. This assignment process is pivotal as it equips team members with a defined set of capabilities and limitations that align with the project's operational requirements. It is a way to enforce data governance, streamline task allocation, and maintain project security. By implementing these custom roles, you can ensure that every user operates within a controlled environment, with permissions that reflect their involvement and needs, ultimately driving the project towards its objectives with precision and clarity.
