# Organisational Owner

The organisational owner is the owner of the organisation's account in Blue. They are automatically a [Project Administrator](roles/project-administrator.md) in any project they join, and they have access to billing, organisation settings, and the ability to delete an organisation. There can be only one organisation owner per account.&#x20;
