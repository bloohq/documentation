# Records

Records are the fundamental building blocks that form the basis of the system's functionality and data organization. Records are highly versatile and can be renamed to suit your specific needs, allowing them to represent a wide range of entities. Whether it's tracking customer interactions, counting sales records, managing project milestones, or cataloguing inventory items, records in Blue can be adapted to encapsulate virtually any type of information you require. This flexibility ensures that Blue can be tailored to fit diverse business processes and operational needs, making them a powerful tool for organizing and managing critical data.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/blue record.png" alt=""><figcaption><p>An example of a record in Blue</p></figcaption></figure>

</div>

We cover records in detail in their own section:

{% content-ref url="../records/" %}
[records](../records/)
{% endcontent-ref %}
