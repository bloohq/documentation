# Archiving Projects

In many organizations, retaining data from completed projects is crucial for audit purposes or for revisiting the project in the future. However, accumulating hundreds of completed projects can clutter the Blue interface, challenging navigation and current project management.

This is why Blue offers the ability for [Project Administrators](../user-management/roles/project-administrator.md) to archive projects once they are complete or not required.&#x20;

<figure><img src="../../.gitbook/assets/archiving project via cog.png" alt=""><figcaption><p>Easily archive any project</p></figcaption></figure>

When a project is archived, multiple things happen.&#x20;

* **Relocation to 'Archived Projects':** The project is moved from the main 'Projects' area in the sidebar to a designated 'Archived Projects' section. This helps maintain a clean and organized main interface.
* **Visible Archived Status**: In the top bar, an 'Archived' label appears next to the project name, clearly indicating its status.
* **Read-Only Access**: Archived projects become read-only. They are essentially 'frozen in time', meaning all users can view the project and its contents, but no modifications or additions can be made. This ensures the integrity and immutability of the project's data as it was at the time of archiving.
* **API Access Changes**: While the project can still be accessed through [Blue’s API](../integrations/api.md) for read operations, any write operations (attempts to modify or add data) will not be successful. This maintains the project's read-only status even through API interactions.

<figure><img src="../../.gitbook/assets/archive projects.png" alt=""><figcaption><p>Archived projects move to this area of the Blue sidebar</p></figcaption></figure>
