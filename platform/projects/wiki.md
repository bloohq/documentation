# Wiki

### Overview

The Wiki acts as a centralized knowledge base for documenting internal SOPs, policies, guidelines, and best practices. With team-wide access and editing abilities, it enables the collective creation and sharing of institutional knowledge.

{% hint style="warning" %}
There is only one Wiki available per project
{% endhint %}

### Editing Wiki Pages

Like [Documents](documents.md), Wiki pages feature a rich text editor for easy formatting and content creation using toolbar options.

The rich text editor empowers effortless formatting and content creation.&#x20;

You can apply formatting like bold and italics through keyboard shortcuts by using the /slash command - `/bold`, `/italic`&#x20;

`c`&#x20;

<figure><img src="../../.gitbook/assets/docs new slash command.png" alt=""><figcaption><p>Type "/" to open commands in the text editor</p></figcaption></figure>

Like anywhere in Blue where you can drop comments, you can [@mention](../user-management/mentions.md) team members to notify them or jump to their cursor location.

<figure><img src="../../.gitbook/assets/mentions docs.png" alt=""><figcaption><p>Easily @mention other users within Wiki and see who is currently viewing the Wiki.</p></figcaption></figure>

### Infinite Scroll

Browsing wiki pages is optimized through infinite scrolling. As users scroll down through the length of a page, additional content automatically loads continuously. There is no need for manual pagination or clicking to load the next page. Pages extend for virtually unlimited page length and uninterrupted reading and review. This creates a smooth, unbroken viewing experience when working with long documents or lengthy pages. Infinite scrolling reduces the navigation friction of paging through multi-page or sectioned documents.

### Table of Contents

Wiki pages automatically generate a table of contents based on the headings included in the text. Headings formatted with markdown syntax `#` are detected and organized into a navigable table of contents. This provides an overview of the structure and can be hidden or revealed. Clicking any heading in the generated table of contents will automatically scroll the page to that section. The smart table of contents enables quick navigation through long pages and an understanding of the content hierarchy at a glance. As headings are added, removed or edited within the text, the table updates dynamically, ensuring it accurately reflects the latest page structure. With automated generation and responsive updates, the table of contents removes the manual effort of creating navigation for complex pages.

<figure><img src="../../.gitbook/assets/wiki table of contents.png" alt=""><figcaption><p>The table of contents is dynamically generated based on your content</p></figcaption></figure>

### Use Cases

Common uses for Wikis include:

#### Documenting SOPs

Standard operating procedures can be centralized in wikis for easy reference and standardization. Step-by-step SOPs guide users through required workflows and processes. Wikis ensure documents remain up-to-date and accurately reflect the latest protocols.

#### Training Manuals

From employee onboarding to ongoing development, wikis are ideal for creating and managing comprehensive training manuals. Manuals include training materials, resources, tutorials, and skills development programs. Wikis keep content organized and help track version updates.

#### Onboarding Resources

Streamline onboarding by using wikis for policies, guidelines, instructions and orientation materials tailored to different roles. New hires have the resources at their fingertips to ramp up quickly. Access controls ensure they see only relevant materials.

#### Project Guidelines

Store project methodology, implementation plans, coding standards, toolsets, specs and operating procedures in wikis. This knowledge transfer is searchable and facilitates consistent execution across teams and endeavours.

#### Team Policies and Best Practices

Consolidate company policies, communication protocols, security guidelines, troubleshooting tips, and other best practices to foster adherence. Wikis act as living handbooks that enable teams to contribute knowledge.

### Note On Access

It's important to note that users with [Client](../user-management/roles/client.md), [Comment](../user-management/roles/comment-only.md), or [View-only ](../user-management/roles/view-only.md)access will not have edit abilities for wikis and documents but can view them. Their access is read-only. This ensures that only team members who should actively contribute to and manage documentation resources have write permissions. With restricted access, organizations can still share relevant wikis and documents with clients, external partners, and reviewers without compromising control over the content. Read-only permission allows information sharing while still maintaining appropriate access levels.
