# Activity

Activity in Blue provides transparency by recording all actions across projects. There are three levels of activity tracking:

1. **Organisational Activity** - Logs everything happening across all projects company-wide.
2. **Project Activity** - Logs actions within a specific project.
3. **Record Activity —** Provides a granular changelog of the record.&#x20;

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/project and company activity.png" alt=""><figcaption><p>Company and Project Activity </p></figcaption></figure>

</div>



Activity delivers a complete audit trail of activities across the organization. It enables understanding of what is happening company-wide without constant status meetings.

#### Timeline Filtering

Activities can be filtered by:

* **Assignee —** Only shows activity from records specifically assigned to one or more users.&#x20;
* **Tag —** Only shows activity from records tagged by one or more specific tags.&#x20;
* **Date —** Only shows activity between a specific date range.&#x20;

<figure><img src="../../.gitbook/assets/RC9p1GnU.gif" alt=""><figcaption><p>Example of filtering timeline by Assignee</p></figcaption></figure>



#### Unread Highlighting

Unread activities are highlighted in blue, enabling easy identification of new actions. Clicking on them displays details of the activity.

As an uneditable log, Company Activity provides a reliable record of all user actions. It facilitates awareness and transparency between all teams and projects.

####
