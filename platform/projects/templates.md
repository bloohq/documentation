# Templates

Templates allow you to reuse the structure of existing projects to kickstart new initiatives with the same framework already in place. This saves time setting up workflows, lists, custom fields, and automation from scratch.

There are plenty of benefits to creating templates in Blue.&#x20;

* **Acceleration:** Templates dramatically accelerate the project setup process. By copying predefined lists, custom fields, automations, and other structural elements, you skip the manually intensive steps of recreating everything from scratch.
* **Standardization:** Templates allow organizations to standardize project structures based on best practices. This consistency improves training, onboarding, and cross-functional collaboration.
* **Consistency:** Templates enforce process consistency across projects handling similar work. For example, standard request intake procedures or standardized development ticketing systems.&#x20;

### Creating Templates

To create a template from an existing project:

1. Go to the project's settings by clicking on the gear icon at the top
2. Select "Convert to Template"
3. The project will now be available as a template for new projects\


<figure><img src="../../.gitbook/assets/create template.png" alt=""><figcaption><p>Convert a project into a project template</p></figcaption></figure>

{% hint style="info" %}
Note that any changes to the template project will be automatically reflected in _new_ projects created with that template.&#x20;
{% endhint %}

New projects created from existing templates carry across all settings and data from that template

* [Records](../records/)
* [Custom Fields](../custom-fields/)
* [Automations](../automations/)
* [Files](file-management.md)
* [Documents](documents.md)
* [Wiki Data](wiki.md)
* [Forms](forms.md)
* [Custom User Roles](../user-management/roles/custom-user-roles.md)

### Using Templates

When starting a new project, you will see an option to choose "Templates" under the Create Project button.

<figure><img src="../../.gitbook/assets/create new templates.png" alt=""><figcaption></figcaption></figure>

Here you can browse:

* Official Blue templates
* Your organization's custom templates

Converting mature projects into templates ensures that best practices and optimized workflows can be replicated across your organization. By streamlining repeating setup work, enforcing consistency, and embedding institutional knowledge into reusable frameworks, templates accelerate delivery while enabling focus on core objectives. They allow past success to be replicated at scale across the organization.
