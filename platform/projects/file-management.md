# File Management

You'll likely have supporting files and documents during a project or process. These could be work-in-progress design files, word documents for review, pictures, and videos.&#x20;

Blue offers unlimited file storage, [in line with our fair-use policies](https://www.blue.cc/legal/terms), for all your project and process-related needs. The maximum file size for an individual file is 5GB.&#x20;



## Uploading in Comments

You can upload files anywhere where you can write a comment; the files are shown inline with your text instead of attachments to your message. We believe this is important so that everyone has the precise context of your message with the data at their fingertips.&#x20;

<figure><img src="../../.gitbook/assets/file in comments.png" alt=""><figcaption><p>Blue puts your files right into the context — not hidden away. </p></figcaption></figure>

The file will also be shown in the files tab.  Note that if the original comment where the uploaded file is deleted, the file is also deleted from the files tab.

{% hint style="info" %}
**Files in Record Descriptions**

Files uploaded in the description area of the records are not listed in the files tab, as these are typically used for temporary notes. We recommend uploading files to a [specific file custom field ](../custom-fields/file.md)instead.&#x20;
{% endhint %}

## File Custom Field

Blue has a[ specific custom file field for handling structured data uploads](../custom-fields/file.md). Files uploaded in this field will show on the file tab in the project and can be deleted from both the custom field within the record and the files tab.&#x20;

<figure><img src="../../.gitbook/assets/file custom field.png" alt=""><figcaption></figcaption></figure>

## Previewing Files&#x20;

For supported file types, such as most images, videos, PDF, Word and Excel documents, you can right-click to preview the file without downloading it to your device.&#x20;

<figure><img src="../../.gitbook/assets/TAuRnDJR.gif" alt=""><figcaption><p>Previewing files is a great way to quickly access information</p></figcaption></figure>

## Downloading Files

If you want to download files to your computer, you can right-click on the file and then select the download option.

<figure><img src="../../.gitbook/assets/download files.png" alt=""><figcaption><p>You can download any file in two clicks</p></figcaption></figure>



## Deleting Files

Only [Project Administrators](../user-management/roles/project-administrator.md) and [Team Members](../user-management/roles/team-member.md) can delete files by right-clicking on a file and selecting delete.  A warning popup will appear to confirm the file deletion.&#x20;

<figure><img src="../../.gitbook/assets/delete file.png" alt=""><figcaption><p>Blue always confirms if you want to delete a file</p></figcaption></figure>



{% hint style="danger" %}
**File deletion is permanent.**

It's important to note that when you delete a file in Blue, it's permanently deleted from our systems. We keep rolling 30-day backups for all active files and have redundant storage capacity to ensure that we never lose any of your files.&#x20;
{% endhint %}

## Files Tab&#x20;

Every project has a file tab, which shows all the files uploaded in any comment in the project and any additional files uploaded directly in the files tab.&#x20;

### Grid & List View

You can choose between two ways to view your files in the files tab.&#x20;

<figure><img src="../../.gitbook/assets/kMnBgabq.gif" alt=""><figcaption><p>Easily switch between grid and list view based on your preference</p></figcaption></figure>

### Searching for Files

You can search for files by their name using the file search bar. This will instantly search across all files in all folders and sub-folders.&#x20;

### Locating Files

### Moving Files

### Sharing Files

### Folders

#### Creating Folders

Folders are a valuable way to keep your file organised. [Project Administrators ](../user-management/roles/project-administrator.md)and [Team Members](../user-management/roles/team-member.md) can create folders. You can create unlimited amounts of folders and sub-folders to match your organisational file structure.&#x20;

You can create a folder by right-clicking on any empty space in the file tab.

<figure><img src="../../.gitbook/assets/create folder 1.png" alt=""><figcaption><p>Right click on any empty space in the file tab and select "New Folder"</p></figcaption></figure>

Alternatively, you can also click on the "Files" navigation button on the top left:

<figure><img src="../../.gitbook/assets/create folder 2.png" alt=""><figcaption></figcaption></figure>

#### Renaming a Folder&#x20;

Renaming a folder is simple: just right-click on the folder and select rename. Only  [Project Administrators ](../user-management/roles/project-administrator.md)and [Team Members](../user-management/roles/team-member.md) can rename folders.&#x20;

<figure><img src="../../.gitbook/assets/file rename.png" alt=""><figcaption><p>Any folder can be renamed </p></figcaption></figure>

#### Setting Folder Color

By default, folders are blue, but you can set a different color by right-clicking on the folder.  Only [Project Administrators ](../user-management/roles/project-administrator.md)and [Team Members](../user-management/roles/team-member.md) can change the color of folders.&#x20;

<div data-full-width="false">

<figure><img src="../../.gitbook/assets/NqY7Epgo.gif" alt=""><figcaption><p>Changing folder color can make it easier for your team to find the right files</p></figcaption></figure>

</div>

### Bulk Actions

To save you time, Blue supports many bulk actions for file management.&#x20;

* **Upload —** Drag-and-drop multiple files simultaneously or even an entire folder to upload. If you are uploading large files, you can continue using Blue as usual and files with upload in the background.&#x20;
* **Shift-Select —** You can select multiple files simultaneously by holding down SHIFT and selecting the files you want.&#x20;
* **Download —** Using Shift-Select, you can then right-click to download all the files at the same time. Blue will download each file individually in your browser.
* **Move —** You can move files to a folder or subfolder by using Shift-Select, you can then right-click, select move and then select the folder of your choice. Blue does not support moving files between projects. &#x20;
* **Delete —** Using Shift-Select, you can then right-click to delete all the files at the same time. Blue will show a warning/confirmation popup before deleting any files. Blue will delete all the files within the folder if you delete a folder.&#x20;



