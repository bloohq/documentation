# Copying Projects

You can copy projects within your organisation or to other organisations by clicking on the project settings icon. Only [Project Administrators ](../user-management/roles/project-administrator.md)can copy projects.&#x20;

<figure><img src="../../.gitbook/assets/project settings (1).png" alt=""><figcaption><p>Only Project Administrators have access to project settings</p></figcaption></figure>

When copying projects, you can choose what you want to copy across.

<figure><img src="../../.gitbook/assets/copy projects.png" alt=""><figcaption><p>Choose which components of a project you want to copy </p></figcaption></figure>

{% hint style="info" %}
**Time required to copy projects**

Depending on the size of your project, copying can take anywhere from a few seconds to a few minutes.&#x20;
{% endhint %}

You can watch a video on copying projects:

{% embed url="https://www.youtube.com/watch?v=ibBUcFhgjGY" %}
