# Webhooks

* Explain what is a Webhook + give an example&#x20;
* How quickly are Webhooks triggered?&#x20;
* Benefits of Webhooks
* Blue Webhooks have been designed to be powerful
* Give you an unprecedented level of control
* How to create a webhook
* List Triggers for webbooks&#x20;
* Sample of Webhook response
* Explain what a signature is
* Sample of checking signature
* Webhook health
* Mention Zapier and Pabbly as well
* Hint about reaching out to help, but that we offer professional services for integration&#x20;



<details>

<summary>Example Webhook Response</summary>

{% code fullWidth="true" %}
```json
{

  "event": "TODO_CREATED",

  "webhook": {

    "id": "clb4qaopy0000vt24mlp28v03",

    "uid": "edb5b5d51dc9461a952160dbe72153b4",

    "name": "Testing webhook",

    "url": "https://httpdump.app/dumps/7cc66eda-8e83-4039-ba49-4c36db14d508",

    "secret": "",

    "status": "HEALTHY",

    "enabled": true,

    "metadata": {

      "events": [

        "TODO_CREATED",

        "TODO_DELETED",

        "TODO_MOVED",

        "TODO_NAME_CHANGED",

        "TODO_CHECKLIST_CREATED",

        "TODO_CHECKLIST_NAME_CHANGED",

        "TODO_CHECKLIST_DELETED",

        "TODO_CHECKLIST_ITEM_CREATED",

        "TODO_LIST_CREATED",

        "TODO_LIST_DELETED",

        "TODO_LIST_NAME_CHANGED",

        "COMMENT_CREATED",

        "COMMENT_DELETED",

        "COMMENT_UPDATED"

      ],

      "projectIds": [

        "clakt90vu000bvt64hhffqleq",

        "claulnu1y0000vtrcjurjgds4",

        "clb4gay0y000qvtzs8smaays8"

      ]

    },

    "createdById": "claksl2ye0000vtas0h0ln71e",

    "createdAt": "2022-12-01T07:01:25.014Z",

    "updatedAt": "2022-12-01T11:23:00.661Z"

  },

  "currentValue": {

    "id": "clb4zna6x0000vt1swuwso25f",

    "uid": "11021cb836f34732b31e5ebd5dd5eb27",

    "title": "GGGG",

    "html": null,

    "text": null,

    "position": "32767.5",

    "archived": false,

    "done": false,

    "startedAt": null,

    "duedAt": null,

    "timezone": null,

    "createdAt": "2022-12-01T11:23:09.000Z",

    "updatedAt": "2022-12-01T11:23:09.241Z",

    "createdById": "claksl2ye0000vtas0h0ln71e",

    "todoListId": "clakt9v5t000kvt64f563l90l",

    "todoCustomFields": [],

    "todoUsers": [],

    "todoTags": [],

    "todoList": {

      "id": "clakt9v5t000kvt64f563l90l",

      "uid": "df0e931e6e8f4fc2905ece9da167d2d3",

      "createdAt": "2022-11-17T08:29:22.000Z",

      "updatedAt": "2022-11-17T08:29:22.050Z",

      "title": "ETEST",

      "position": 131070,

      "createdById": "claksl2ye0000vtas0h0ln71e",

      "projectId": "clakt90vu000bvt64hhffqleq",

      "project": {

        "id": "clakt90vu000bvt64hhffqleq",

        "uid": "d7db469a8908496f8e5f5e68030fe4c8",

        "slug": "teest",

        "name": "TEEST",

        "description": "",

        "archived": false,

        "createdAt": "2022-11-17T08:28:43.000Z",

        "updatedAt": "2022-11-17T08:28:42.811Z",

        "isTemplate": false,

        "isOfficialTemplate": false,

        "category": "GENERAL",

        "companyId": "clakt8jud0002vt64205yyj10",

        "imageId": null,

        "hideEmailFromRoles": null,

        "company": {

          "id": "clakt8jud0002vt64205yyj10",

          "uid": "400eb3f8238c486caf40957618be9662",

          "slug": "rogue",

          "name": "Rogue",

          "description": null,

          "createdAt": "2022-11-17T08:28:21.000Z",

          "updatedAt": "2022-11-17T08:28:20.726Z",

          "freeTrialExpiredAt": "2022-12-01T08:28:15.000Z",

          "freeTrialStartedAt": "2022-11-17T08:28:15.000Z",

          "subscribedAt": null,

          "imageId": null,

          "subscriptionPlanId": "clakt8jue0003vt64e40q2prj",

          "freeTrialExtendedById": null,

          "freeTrialExtendedAt": null

        }

      }

    }

  }

}
```
{% endcode %}

</details>

We recommend that you can check the webhook signature. This is an example of how you can do this in NodeJS:

```javascript
const crypto = require('crypto');
const key = 'secret'; // from the webhook object
const body = { ...webhookResponse }
const hash = crypto.createHmac('sha256', key).update(body);
const signature = hash.digest('hex');
```

{% hint style="info" %}
**Developer Support**

If you have any questions, please contact us at [help@blue.cc](mailto:help@blue.cc), and we assist. We also offer professional services for integration support.&#x20;
{% endhint %}
