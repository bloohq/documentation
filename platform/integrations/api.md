# API

Blue has an API (Application Programming Interface), so you can build your own integrations if required.

Blue has 100% API coverage. This means that anything that you can do in [any of our apps, ](../../getting-started/apps/)you can do programmatically. We have a powerful GraphQL API that has a full playground where you can test your queries and is fully documented.

{% embed url="https://www.youtube.com/watch?v=GKSAHnoFn4s" %}

## Getting Started

To begin using the Blue API:

* **Access the API Interface**: Log in to Blue, click on your profile on the top right, and under the profile menu, you'll find the API tab.
* **Token Generation**: In the API tab, generate your unique access token. Provide a name for the token and set an expiration date if desired. After generation, you'll receive a Secret ID and a Token ID - ensure these credentials are secure. The Secret ID will only be shown once.&#x20;

## **API Playground**

The Blue API playground is an interactive environment where you can experiment with and test your API queries.

* **Setting Up**: After generating your token, use the "▶️" button to enter the playground.
* **Authentication**: In the Query Variable section under HTTP Headers, enter your saved Secret ID and Token ID for access.
* **Company and Project ID Retrieval**: To retrieve your Company ID or Project ID, refer to the URL structure within Blue. These IDs are crucial for crafting specific queries related to your organization's data.

## **Integration**

To integrate the Blue API:

* **Copy Curl**: Click on “Copy Curl” in the playground. This provides you with a template to start building your custom integrations.
* **API Documentation and Schema**: Our built-in documentation and schema, located on the right side of the playground, offer comprehensive guidance. You can quickly search for queries or download the schema in JSON or SDL format for offline reference.

{% hint style="info" %}
**Need more help?**

Feel free to reach out to our team at [help@blue.cc](mailto:help@blue.cc) for assistance. We also offer professional services for custom integrations.&#x20;
{% endhint %}

\
