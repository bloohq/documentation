# Triggers

A trigger is an event that sets off an automation. For instance, a trigger could be the completion of a record, a change in a record's assignee, or a change in a custom field. When this predefined condition is met, the automation is activated.

These are all the possible automation triggers in Blue:

* **A New Record is Created**: Triggers when a new record is added to the project. Useful for initiating follow-up actions or notifications related to new tasks or entries. This also triggers when there is a [new form submission.](../projects/forms.md)
* **A Record is Moved to Another List**: Activates when a record is transferred from one list to another within the project, indicating a change in its stage or status.
* **A Record is Marked as Complete**: Triggers when a record's status is updated to 'complete', signalling the end of a task or process.
* **A Record is Marked as Incomplete**: Activates when a record previously marked as complete is reverted to an incomplete status.
* **Someone is Assigned to a Record**: Triggers when a new assignee is added to a record, indicating their responsibility for that task or item.
* **Someone is Unassigned from a Record**: Activates when an assignee is removed from a record, signifying a change in task responsibility.
* **Due Date Changes**: Triggers when the due date of a record is altered, which can be critical for time-sensitive tasks.
* **Due Date is Removed**: Activates when a record's due date is cleared, possibly indicating a change in priorities or timelines.
* **Due Date has Expired**: Triggers when a record's due date passes without completion, highlighting potential delays or oversights. This is an excellent automation for managers, as they can flag records/tasks to themselves if they become overdue.&#x20;
* **Tag is Added to Record**: Activates when a new tag is added to a record, useful for categorization and tracking changes in task attributes.
* **Tag is Removed from Record**: Triggers when a tag is removed from a record, indicating a change in its categorization or properties.
* **All Checklists are Marked as Complete**: Activates when every item in a record's checklist is checked off, indicating the completion of all sub-tasks.
* **Any Checklist is Marked as Incomplete**: Triggers when any item in a record's checklist is marked incomplete, signalling unfinished sub-tasks.
* **A Record is Moved/Copied from Another Project**: Activates when a record is brought into the current project from a different one, helpful in tracking cross-project workflows. The trigger applies whether the record is manually moved or copied by a user, or automatically transferred through another automation, providing a comprehensive way to track cross-project activities.
* **A Custom Field is Added to a Record**: Triggers when data is written to a specific custom field in a record, indicating a change or addition of specific information.
* **A Custom Field is Removed from a Record**: Activates when data is removed from a specific custom field in a record, signifying a modification in the record's information structure.

{% hint style="info" %}
**Section marked for improvement**

We are currently enhancing this section to offer more comprehensive insights into each trigger. Our updates will include detailed descriptions, illustrative screenshots, and additional information to provide a clearer and more informative guide on utilizing these triggers effectively.
{% endhint %}

