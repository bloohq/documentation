# Email Automations

Email automations allow you to automatically send emails when certain triggers occur, using data stored within your Blue records. For example, sending a confirmation email when a new record is created or notifying a manager when a task becomes overdue.

### Configuration&#x20;

Getting started with email automations is straightforward. When you first create an automation, we recommend sending a test email to yourself or a colleague. This allows you to validate that the automation is functioning before rolling out.\


To create an email automation:

* Set the trigger, such as "A New Record is Created".
*   Configure the email details:

    * From name and reply-to address so recipients can respond.
    * To address (can be static or pulled dynamically from an [Email custom field)](../../custom-fields/)
    * Enter your own email address or a test email in the recipient field. You can also add CC or BCC email addresses.
    * Email subject and body content using custom field for dynamic content\
      using merge tags.&#x20;

    Next, customize the email content using merge tags:

    * Click inside the email body, then insert merge tags referencing record names, custom fields etc. using the {curly bracket} syntax.
    * Insert file attachments by drag-and-drop or using the attachment icon. Files from File custom fields may automatically attach if under 10MB.

Once you finish customizing the template, confirm creation of the automation. When triggered, it will now send personalized emails leveraging your record data.

{% hint style="warning" %}
Almost all data can be used except for comment, activity and checklists.
{% endhint %}

### Use Cases

* Send confirmation email when client submits request via intake form
* Notify assignee when new high priority task is created
* Remind managers about overdue tasks from their direct reports
* Send survey to customer after support ticket is marked resolved
* Specify which records should activate the automation when the trigger occurs.
* Automate recruitment program by sending confirmation emails

### Key Benefits

* **Personalized Communication**: Emails can leverage custom field data from records using merge tags, enabling dynamic, tailored messaging.
* **Automatic Notifications**: Takes manual work out of sending repetitive email updates and reminders.
* **Structured Data-Driven Workflows**: Automated emails can move projects forward by triggering actions based on record data.

