# Reminders

Reminder makes it easier to stay on top of your tasks. Simply select any unfinished task and set a reminder for later today, tomorrow, or any future date that works for you. When the reminder pops up, you'll be able to jump straight back into the task right from the notification.

This feature helps you prioritize your workload and ensure critical action items never go overlooked. Whether it's replying to an urgent email, submitting a report, or calling a client back, you can schedule reminders so your most pressing responsibilities always get done on time. We handle sending the reminders - you just focus on completing the work.

### Setting a Reminder

To set a reminder on a record:

1. Open the record
2. Click on the clock icon in the top right
3. Select the desired date and time for the reminder
4. Or by Default you could select by&#x20;
   * Later today
   * Tomorrow
   * Later this week
   * This weekend
   * Next week

Once you set a reminder, you will see the date and time displayed directly on the record.

### Getting Reminder Notifications

When the reminder's date and time occurs, you will receive a notification by:

* Email (if enabled in your notification settings)
* Push notification in the Blue web/mobile apps (if enabled)

The notification will alert you to revisit the record.

### Editing or Deleting Reminders

To edit or remove a reminder:

1. Hover over the date on the record
2. Choose to either edit or delete the reminder

Editing allows you to push the reminder to a later date. Deleting will completely remove the reminder.

### Custom Notification Settings

In your personal notification settings, you can toggle email and push notifications on/off for reminders specifically. This allows you to choose how you want to be notified for reminders.

By default, both options are enabled when reminders are first set up.

Overall, reminders are a simple yet effective way to schedule follow-ups and ensure you won't miss important records.  The custom notifications provide flexibility to manage reminders across devices.
