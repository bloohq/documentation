# Assignees

Assignees are a core feature in Blue that enables the delegation of records to specific team members. When a user is assigned to a record, they become responsible for completing or managing that record.

<figure><img src="../../.gitbook/assets/assignee.png" alt=""><figcaption><p>Click on Assignees to see a list of users in the project</p></figcaption></figure>

Blue allows multiple users to be assigned to the same record. This enables collaborative workflows where more than one person manages a task. Teams can leverage multiple assignees when records require input or action from several members.

\
By default, when a user is assigned to a record in Blue, they will receive an in-app and email notification alerting them of the assignment. This ensures assignees know they have been delegated a task requiring their input or completion. Users can customize their notification preferences under user settings if they prefer to disable assignment notifications. [Project Administrators](../user-management/roles/project-administrator.md) can also use [Email Automations](../automations/actions/email-automations.md) to create custom email notifications to users when they are assigned.&#x20;

When a record is assigned to a user, it will automatically appear in their "My Items" sidebar view. This provides quick access to all records currently assigned to that individual across all their projects. The consolidated My Items view enables users to monitor and manage their assigned workload in one place easily.

Another very useful feature is that Project Administrators can use  [Custom User Roles](../user-management/roles/custom-user-roles.md) to restrict users to only see records that are specifically assigned to them.&#x20;

