# Repeat

The repeating records feature automatically allows you to recreate a record on a defined schedule. This is useful for tasks or processes that need to occur repeatedly regularly.

Repeating tasks in Blue provides several key benefits:

* **Save Time:** Automates repetitive tasks so you don't have to manually recreate the same record over and over. This is great for recurring meetings, reports, newsletters etc. Saves having to rebuild agendas/documents every time.
* **Consistency:** Ensures recurring processes follow the same structure by automatically carrying over checklists, custom fields, etc. Maintains continuity for recurring meetings or communications by copying over attendees, descriptions etc.
* **Planning Ahead:** Enables preparation for upcoming recurring tasks well in advance. Can provide team visibility into future workload demands from repeating records

### Creating Repeating records

To set up a repeating record you open the record you want to repeat and click on the ••• menu and then repeat:

<figure><img src="../../.gitbook/assets/repeat record.png" alt=""><figcaption><p>Select repeat to open the repeat record options </p></figcaption></figure>

You are then presented with the following options:

<figure><img src="../../.gitbook/assets/repeat records.png" alt=""><figcaption><p>The repeat record options popup</p></figcaption></figure>

### Repeat Intervals

You can choose from:

* Daily
* Weekly
* Monthly
* Yearly

Or, create a custom repeating schedule that gives you significantly more flexibility and customisation options:

<figure><img src="../../.gitbook/assets/custom repeat record options.png" alt=""><figcaption></figcaption></figure>



Here you can choose:

* Starting from: choose the precise date that the record should start repeating from
* Repeat every: Choose if the record should repeat daily, weekly, monthly, or yearly.
* End Condition: Choose if the record should repeat indefindely or after a certain date or number of repetitions&#x20;

### Copy Options

When a repeating task gets recreated, you can choose what data gets copied over from the original task:

* Assignees
* Tags
* Custom fields
* Checklists
* Comments
* Description

<figure><img src="../../.gitbook/assets/repeat copy options.png" alt=""><figcaption><p>The copy options for the repeating record</p></figcaption></figure>

You are also able to choose which list the record should repeat in. Right now, we only support repeating records _within_ the same project, not across projects.&#x20;

{% hint style="warning" %}
Be aware that if you delete the original record, this will stop new records from being created with the repeat option.&#x20;
{% endhint %}

### Active Repeating Records

For any records that are repeating, these will show the repeat symbol on the record card in [Board View](../views/board.md):

<figure><img src="../../.gitbook/assets/repeat icon on card.png" alt=""><figcaption><p>Repeat Icon on card</p></figcaption></figure>

And also within the individual record:

<figure><img src="../../.gitbook/assets/repeat icon inside record.png" alt=""><figcaption><p>Repeat icon inside the record</p></figcaption></figure>

### Stopping the Repeat

To stop a record from repeating:

1. Open the original recurring record
2. Click the repeat icon
3. Select the "Stop Repeating" option

<figure><img src="../../.gitbook/assets/stop repeat.png" alt=""><figcaption><p>Stop a  record from repeating</p></figcaption></figure>

This will prevent any further recurring tasks from being created.
