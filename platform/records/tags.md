# Tags

Regardless of the type of project or process that you manage, staying organized is key. Tags in Blue offers a robust, flexible system to categorize records—streamlining project tracking and enhancing task management. These identifiers allow you to effectively sort and prioritize your work, bringing structure and clarity to complex projects.

<figure><img src="../../.gitbook/assets/tags.png" alt=""><figcaption><p>Color code tags to make them stand out</p></figcaption></figure>

Tags act as a versatile organizational tool. They can be applied to differentiate task types, indicate urgency, or classify tasks by any category relevant to your project. Each tag can also be color-coded for visual distinction and to inject vibrancy into your workspace, reflecting Blue's belief that a touch of color contributes to a positive and engaging work environment.

<figure><img src="../../.gitbook/assets/task tag.png" alt=""><figcaption><p>Two tags showing on a record card </p></figcaption></figure>

## Using Tags

* **Adding Tags**: To create a tag, consider the aspects of your tasks that need categorization—be it the nature of the task, such as 'bug', 'back-end', or 'UI', or its priority. Once you've identified your categories, you can easily create tags and assign them unique colors for immediate recognition.
* **Filtering with Tags**: Blue’s filtering capabilities make managing your workload more intuitive. You can filter tags using the drop-down Filter icon or directly within the board view by clicking on the desired tag.

You can watch a video on Tags:

{% embed url="https://www.youtube.com/watch?v=ocJ1QcY3030" %}

{% hint style="info" %}
**Cross-Project Tagging**

For organisation-wide views such as Dashboards and maps, tagging becomes especially important because Blue automatically merges tags of the same name. So you just need to filter by "Priority 1" tag, which will show only records across all projects with that tag. &#x20;
{% endhint %}

\


\
