# Dependencies

Dependencies in Blue allow you to connect records together to create structured workflows. By setting up record dependencies, you can ensure tasks are completed in the required order of operations.

There are two types of dependencies:

* **Blocking** — The record blocks other records from starting or completing until it is finished. Other records depend on this one being done first.
* **Blocked By** — The record is blocked by another record that must be completed first before this one can start or finish. This record depends on another one being done before it can proceed.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/dependencies 1.gif" alt=""><figcaption></figcaption></figure>

</div>

### Creating Dependencies

To set up a dependency:

1. Go to the record that should be completed first.
2. Click on Dependencies below the Tags section.
3. Choose "Blocking" to make this record block another one.
4. Select the record you want this one to block from the dropdown.
5. Click Add.

The blocked record will now show "Blocked by" and be prevented from completion until the blocking record is done.

### Completing Blocked Records

If a user attempts to mark a blocked record as complete before its dependencies are satisfied, a warning popup will appear, alerting them of the blocking dependency. This prevents records from being completed out of order by mistake.

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/dependencies 2.gif" alt=""><figcaption></figcaption></figure>

</div>

### Removing Dependencies

To remove a dependency, go to either linked record. Under Dependencies, click the dropdown and select "Remove". This will delete the link between the records.

### Dependency Tracking

All dependencies are logged under the Activity section of records, providing transparency into workflow connections. This allows tracking the completion sequence of interconnected records.

Dependencies enable teams to model workflows and prevent out-of-order or improper sequence record completion. Connecting records establishes order and structure for complex cross-functional projects.
