# Comments

Comments within each record transform how team communication and collaboration are handled. Blue provides a context-rich platform for discussions by integrating comments directly into individual records. This approach significantly reduces the clutter and confusion often associated with scattered email threads and group chats, ensuring that every conversation is directly relevant to the topic at hand.

Understanding that communication is dynamic, Blue offers the capability to edit comments. This feature allows users to update, correct, or refine their messages, ensuring that discussions remain accurate and relevant. The ability to edit comments adds a layer of flexibility and control to your communication, making it more effective and efficient. When a comment is edited, it is marked as edited so other users know that it is not the original comment.&#x20;

Comments in records offer a rich-text experience where you can:&#x20;

* **Attach Files**: Easily add documents, spreadsheets, PDFs, or any other relevant files directly into the comment, making it simple to share information and resources. You can upload files up to 5GB in size.&#x20;
* **Embed Videos and Pictures**: Visual content like videos and pictures can be inserted, providing a more engaging and informative way to communicate. Simply copy and paste links from popular video sites to have an embedded video player.&#x20;
* [**@Mention Users**](../user-management/mentions.md): By @mentioning team members, you can trigger notifications to specific users, ensuring that the right people are alerted and involved in the conversation.
* **Use Emojis and GIFs**: Enhance your messages with a wide range of emojis and GIFs, adding a touch of personality and express emotions or reactions better.&#x20;





