# Copying Records

Blue allows you to duplicate records within a project or across projects easily. This can help accelerate workflow by replicating information without having to rewrite it.

To copy a record:

1. Navigate to the record you want to copy
2. Click on the "..." menu
3. Select "Copy"
4. Choose whether to copy to the current project or another project
5. The copy will be created with the same details.

<figure><img src="../../.gitbook/assets/copy project.png" alt=""><figcaption></figcaption></figure>

Things to note:

* When copying across projects, any attachments and comments will be copied over
* Custom fields will copy over to the new project, and the new project will also have the additional custom fields
* Only admin and team members can move records

