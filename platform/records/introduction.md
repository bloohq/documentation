# Introduction

Records are the fundamental building blocks that form the basis of the system's functionality and data organization. Records are highly versatile and can be renamed to suit your specific needs, allowing them to represent a wide range of entities. Whether it's tracking customer interactions, counting sales records, managing project milestones, or cataloguing inventory items, records in Blue can be adapted to encapsulate virtually any type of information you require. This flexibility ensures that Blue can be tailored to fit diverse business processes and operational needs, making it a powerful tool for organizing and managing critical data.

Records are specific to a project. In the future, we aim to have the ability to have one record available across projects.&#x20;
