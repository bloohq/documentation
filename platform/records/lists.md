# Lists

Lists in Blue are powerful organizational tools designed to help you manage and categorize records efficiently within each project. Whether you’re sorting customer requests, tracking project stages, or organizing inventory, lists provide a structured approach to handling vast amounts of data. With the capacity to hold up to 20,000 records per list and a maximum of 50 lists per project, Blue offers the scalability to accommodate your growing data needs.



* Lists have two main use cases: categorization and steps in a process
* Creating lists
* Bulk Creation of Lists
* Bulk actions on lists
* Deleting lists (warning about deleting records within the list)

## **Creating and Managing Lists**

Creating a list in Blue is straightforward. You can name each list to reflect its contents or purpose and organize them to align with your project’s workflow. The ease of managing these lists means you can quickly add new records, rearrange items, or modify list properties to adapt to changing project requirements.

## **Using Lists for Categorization**

Lists are ideal for categorizing records into distinct groups. This categorization is essential for users who need to segment their information, such as differentiating between various types of client interactions or product categories. Organizing records into relevant lists enhances data retrievability and streamlines your management processes.

## **Lists as Steps in a Process**

Besides categorization, lists can represent different workflow or process stages. This functionality is particularly valuable in project management scenarios, where you can visually track the progress of tasks across stages like ‘To Do’, ‘In Progress’, and ‘Completed’. This visual representation helps monitor workflows and ensure timely completion of tasks.
