# Printing Records PDF

{% hint style="warning" %}
open question is how we name this. Printing vs generating PDFs
{% endhint %}

Printing PDFs of records allows users to generate PDFs that are overlayed with the custom fields data of a specific record. This is great for generating receipts, work orders, and any other PDF documents required for the project or reporting purposes.&#x20;

## PDF Templates

These can be created by [Project Administrators](../user-management/roles/project-administrator.md) only via the project settings dropdown or in the ••• records menu. &#x20;

All PDF templates are viewable by all Project Administrators&#x20;

### Viewing PDF Templates

* Show screenshot of list view

### Creating PDF Templates



* New template and drag and drop





* PDF templates and listed
* Deleted (from sidebar in individual view)
* They can be edited
* Drag and drop of default fields
  * Todo name
  * Date
  * Assignee
  * Tag
  * List
* Drag and drop of custom fields
  * Single Line Text
  * URL
  * Currency
  * Country (no flag!)
  * Date
  * Formula
  * Single Select
  * Multiple Select
  * Location (just address, not map!)
  * Phone Number
  * Email
  * Checkbox
  * Number
  * Percent
  * Unique ID

## Printing Records&#x20;

Go to ••• record menu and select "Print". This is available for any user in the project.

Non-Project administrators will only have the choice to select an existing template. Project administrators have the choice to select a template or create a brand new template.&#x20;

Users have two options:

1. Download — This will instantly download the PDF&#x20;
2. Print — This will use the standard browser printing dialogue to preview the PDF

{% hint style="warning" %}
For [Custom User Roles ](../user-management/roles/custom-user-roles.md)that have custom fields restrictions, this will not affect the generation of PDFs from records. This is important to note as this can "leak" data in the PDF generation process that would otherwise be unavailable for that user role
{% endhint %}

