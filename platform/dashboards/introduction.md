# Introduction

Dashboards in Blue are powerful because they allow you to combine real-time data from multiple projects simultaneously. This allows you to build live dashboards that give you meaningful information for decision-making.&#x20;

## Chart Options

Blue comes with three options for charts:

1. [**Stat Cards:** ](stat-cards.md)Ideal for at-a-glance key metrics, stat cards display real-time, singular data points such as total project revenue, current active issues, or overall project completion percentage. They provide immediate insight into critical data with a simple, clear interface.
2. [**Pie Charts:** ](pie-charts.md)Best for visualizing data composition, pie charts in Blue help you understand the proportion of various elements within a dataset. For instance, you could use a pie chart to show the distribution of tasks across different departments or the percentage split of revenue from different types of customers.&#x20;
3. [Bar Charts: ](bar-graphs.md) Effective for comparing quantities, bar charts allow you to contrast different groups side by side. Use bar charts to compare sales figures across different quarters, the number of tasks each team member completes, or the frequency of certain customer inquiries over time.

## Sharing

Dashboards in Blue are not just for personal viewing; they can be shared with team members within the platform. You can grant edit or view permissions. Anyone with access to a dashboard can share it with other users. &#x20;

{% hint style="info" %}
Sharing privileges are tiered according to user permissions; for instance, users with viewing permissions can invite others to view the dashboard, ensuring that access rights are maintained and respected.
{% endhint %}

Currently, Blue's dashboards are designed for internal collaboration and cannot be shared externally with non-Blue users via public links. However, this is planned for a future release. If you have further ideas for dashboards, you can always [join our community forum](https://ask.blue.cc) or reach out to us directly at [help@blue.cc ](mailto:help@blue.cc)

## Filters

Blue dashboards provide sophisticated filtering capabilities, enabling precise data segmentation by project, list, tag, and timeframe. Lists and tags sharing the same name are smartly consolidated across projects, simplifying the process of filtering data that spans multiple initiatives. This intelligent design allows for seamless cross-project analysis, ensuring comprehensive and relevant insights.

## **Interactive and Customizable Interface**

* **Drag and Drop Functionality**: Customizing your dashboard layout is intuitive with Blue's drag and drop feature. You can quickly reposition charts within a dashboard to suit your viewing preferences or to highlight the most critical information.
* **Display Flexibility**: Blue understands that data visualization is not one-size-fits-all. That's why it provides various display options for all charts and data representations on the dashboards. Whether you need a pie chart to break down task distribution or a line chart to track progress over time, you have the flexibility to choose the format that best conveys your data.

