# Pie Charts

* Show the amount and the %
* CAn color code slices
* Slices can contain multiple values&#x20;
* You can think of each slice as an individual stat card

{% hint style="info" %}
Bar charts have automatic creation, and this will soon be coming to pie charts as well.&#x20;
{% endhint %}
