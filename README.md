---
description: Welcome to Blue.
---

# Welcome

Blue is a powerful yet easy-to-use project and process management platform.&#x20;

**Our mission is to organize the world's work by building the best project management platform on the planet—simple, powerful, flexible, and affordable for all.**

You can use Blue to manage anything — from your Sales CRM to your hiring pipeline and anything with repeatable processes. Blue gives any process clarity and scalability, so you stay on top of everything.&#x20;

This is our documentation. We have comprehensively mapped all the features and functions to enable you to succeed on our platform.

If you have any questions, feel free to reach out to our team:

* Email us at [**help@blue.cc** ](mailto:help@blue.cc)
* Book a free support call [**via this link.**](https://app.onecal.io/b/blue/support)

Every day, we enjoy building Blue. We hope you enjoy using it!

## Discover Blue

<table data-view="cards"><thead><tr><th></th><th></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><strong>Projects</strong></td><td>Projects form the framework for organizing users and data.</td><td><a href=".gitbook/assets/1.jpg">1.jpg</a></td><td><a href="projects/">projects</a></td></tr><tr><td><strong>Records</strong></td><td>Records are the fundamental building blocks for projects.</td><td><a href=".gitbook/assets/2.jpg">2.jpg</a></td><td><a href="records/">records</a></td></tr><tr><td><strong>Views</strong></td><td>Discover the different ways of viewing records in your projects.</td><td><a href=".gitbook/assets/4.jpg">4.jpg</a></td><td><a href="views/">views</a></td></tr><tr><td><strong>Custom Fields</strong></td><td>Create a custom data structure that matches your  requirements.</td><td><a href=".gitbook/assets/4.jpg">4.jpg</a></td><td><a href="custom-fields/">custom-fields</a></td></tr><tr><td><strong>Automations</strong></td><td>Discover how you can automate routine work.</td><td><a href=".gitbook/assets/5.jpg">5.jpg</a></td><td><a href="automations/">automations</a></td></tr><tr><td><strong>User Management</strong></td><td>Learn how to create custom user roles</td><td><a href=".gitbook/assets/6.jpg">6.jpg</a></td><td><a href="user-management/">user-management</a></td></tr><tr><td><strong>Dashboards</strong></td><td>Learn how to create real-time cross-project dashboards</td><td><a href=".gitbook/assets/4.jpg">4.jpg</a></td><td><a href="dashboards/">dashboards</a></td></tr><tr><td><strong>Integrations</strong></td><td>Learn about the tools you can integrate with.</td><td><a href=".gitbook/assets/1.jpg">1.jpg</a></td><td><a href="integrations/">integrations</a></td></tr><tr><td><strong>FAQs</strong></td><td>The most frequently asked questions from our customers.</td><td><a href=".gitbook/assets/3.jpg">3.jpg</a></td><td><a href="start-guide/faqs.md">faqs.md</a></td></tr></tbody></table>
