# Activity

Activity logs within each record provide a detailed and transparent chronicle of all changes made to a record. They serve as a comprehensive audit trail that documents every modification, ensuring clarity and accountability in record management.

<figure><img src="../../.gitbook/assets/Record activity.png" alt=""><figcaption><p>Record activity is meticulously logged.</p></figcaption></figure>

Each entry in the activity log captures the specifics of what was changed, who made the change, and when it occurred. This level of detail extends to all aspects of a record, including changes made to custom fields. Every action is meticulously logged, whether it's an update to a [custom field value](../custom-fields/), a status change, or a modification in the [record's assignment](assignees.md).

One of the critical features of these logs is their immutability. **Activity logs cannot be altered,** which upholds the integrity and accuracy of the record's history.&#x20;

<figure><img src="../../.gitbook/assets/record activity log change.png" alt=""><figcaption><p>An example of changing a record name</p></figcaption></figure>



If an action recorded in the log is reversed or changed, the log doesn’t erase the original entry. Instead, it adds a new entry to document the reversal. This approach ensures that the activity log provides a complete and uneditable history of all changes, preserving the record's integrity over time.

<figure><img src="../../.gitbook/assets/updating field record activity.png" alt=""><figcaption><p>Example of logging the change of content in a custom field</p></figcaption></figure>

The detailed nature of these logs is invaluable for various purposes:

* **Accountability**: By tracking who made each change, the logs foster accountability among team members.
* **Audit and Compliance**: The logs serve as a crucial tool for audit trails and compliance, especially in environments where tracking data changes is mandatory.
* **Project Management and Review**: They enable project managers and team members to review the progression of a record, understand decision-making processes, and analyze workflow efficiency.

You can toggle between showing comments + activity, only comments, and only activity based on what's important to you. Blue remembers your preference across all records, and you can switch your preference within any record.&#x20;

<figure><img src="../../.gitbook/assets/vcIe43zH.gif" alt=""><figcaption><p>Switch between views for acitvity logs</p></figcaption></figure>

\
