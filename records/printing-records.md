# Printing Records

Blue offers a powerful feature that allows you to generate customized PDF documents directly from your records. This functionality is particularly useful for creating invoices, work orders, receipts, and other types of documents that require a professional and standardized format. By leveraging this feature, you can streamline your documentation process, eliminating the need to manually retype information into separate documents.

### Setting up PDF Templates

To get started, you'll need to upload a PDF template within your project. This task can only be performed by Project Administrators. Here's how you can set up a PDF template:

1. Navigate to the project settings by clicking on the project settings icon next to the project name in the top bar.
2. In the project settings menu, locate the "PDF Templates" section.
3. Click on the "upload" button to upload a new PDF template.
4. Drag and drop the desired custom fields from your project onto the template, positioning them in the appropriate sections of the document.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-14 at 17.16.10@2x.png" alt=""><figcaption></figcaption></figure>



### Generating PDFs from Records

After setting up your PDF template, you can generate PDF documents directly from individual records within your project. Here's how:

* Open the record for which you want to generate a PDF document.
* In the record menu, select the "Print".

<figure><img src="../.gitbook/assets/CleanShot 2024-03-14 at 17.18.24@2x.png" alt=""><figcaption></figcaption></figure>

* Choose the PDF template you created earlier from the list of available templates.
* Review the preview of the PDF document to ensure that all the information is accurately populated from the record.
* Select the desired output option: "Download" to save the PDF file locally or "Print" to send the document directly to a connected printer.

<figure><img src="../.gitbook/assets/CleanShot 2024-03-14 at 17.19.27@2x.png" alt=""><figcaption></figcaption></figure>

The generated PDF will include all the relevant data from the record, populating the appropriate fields within the template you created. This process eliminates the need to manually retype or copy-paste information, saving you time and reducing the risk of errors.

### Use Cases and Benefits

The ability to generate PDFs from records in Blue offers several benefits, including:

* **Invoicing and Billing:** Create professional invoices directly from your sales or billing records, ensuring accurate and up-to-date information.
* **Work Orders and Service Reports:** Generate detailed work orders or service reports for your clients or internal teams, streamlining communication and documentation.
* **Receipts and Confirmations:** Provide customers with well-formatted receipts or confirmation documents for their transactions or bookings.
* **Compliance and Record-keeping:** Maintain comprehensive documentation for audit trails, compliance purposes, or archiving records.
