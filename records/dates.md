# Dates

The Date field allows you to associate key dates and times with project [records](./). This helps track important events related to tasks and workflows. The date field is tightly integrated with the [Calendar View](../views/calendar.md), [Timeline View](../views/timeline-and-gantt-charts.md), and with the [filters](../views/filters.md) functionality

### Adding Dates

**Date Selector**

Click on the Date field to open a calendar and time selector. Choose the exact date and time to set the date range or due date for your record.

<figure><img src="../../.gitbook/assets/date picker.png" alt=""><figcaption><p>Setting a start and due date for a record</p></figcaption></figure>

You can also set precise start and end times by clicking on the clock icon:

<figure><img src="../../.gitbook/assets/set time.png" alt=""><figcaption><p>Set precise start and due times for your records</p></figcaption></figure>

Inside of a record you can then see the start and due dates, and this is also logged in the record [activity](activity.md).&#x20;

<div data-full-width="true">

<figure><img src="../../.gitbook/assets/record view of time.png" alt=""><figcaption><p>All information in Blue is always logged</p></figcaption></figure>

</div>

Any records that have a due date will be shown in the project [calendar](../views/calendar.md) view:

<figure><img src="../../.gitbook/assets/calendar view records.png" alt=""><figcaption><p>Calendar view shows all records that have a date set</p></figcaption></figure>



### Use Cases

Potential uses for dates include:

#### Setting Project Start and End Dates

Dates allow establishing defined time periods for project work. Setting target start and end dates provides clarity on expected time investments, ensures alignment with other organizational initiatives, and aids in resource planning and allocation.

#### Defining Task and Milestone Deadlines

Attaching due dates to tasks and milestones enables tracking of timely completion. Setting deadlines creates accountability, allows prioritization, and helps monitor overall project progress.

#### Tracking Process Duration

By capturing start and end times associated with process steps, duration can be calculated to analyze cycle efficiency. Understanding activity time requirements aids optimization.

#### Analyzing Cycle Times and Bottlenecks

Comparing stage durations at scale identifies patterns, variances and bottlenecks. Date information fuels data-driven improvement of workflows and operations.

#### Scheduling Events, Meetings and Appointments

Dates aid appointment setting and calendar integration. Shared visibility of scheduled events and meetings coordinates availability and facilitates attendance.

#### Establishing Timelines and Roadmaps

Milestone dates underpin project plans, timelines and roadmaps. Assigning target dates to key stages plots execution trajectories and conveys objectives.

### Linked Functionality

* [Date filters ](../views/filters.md)to search for records before/after specified dates
* Sync date entries to [external calendars](../integrations/calendar-sync.md)
* Create [calendar views ](../views/calendar.md)of dated tasks

