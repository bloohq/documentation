# Lists

Lists in Blue are powerful organizational tools designed to help you manage and categorize records efficiently within each project. Whether you’re sorting customer requests, tracking project stages, or organizing inventory, lists provide a structured approach to handling vast amounts of data. With the capacity to hold up to 20,000 records per list and a maximum of 50 lists per project, Blue offers the scalability to accommodate your growing data needs.

* Lists have two main use cases: categorization and steps in a process
* Creating lists
* Bulk Creation of Lists
* Bulk actions on lists
* Deleting lists (warning about deleting records within the list)

## **Creating and Managing Lists**

Creating a list in Blue is straightforward. You can name each list to reflect its contents or purpose and organize them to align with your project’s workflow. The ease of managing these lists means you can quickly add new records, rearrange items, or modify list properties to adapt to changing project requirements.



## **Using Lists for Categorization**

Lists are ideal for categorizing records into distinct groups. This categorization is essential for users who need to segment their information, such as differentiating between various types of client interactions or product categories. Organizing records into relevant lists enhances data retrievability and streamlines your management processes.

## **Lists as Steps in a Process**

Besides categorization, lists can represent different workflow or process stages. This functionality is particularly valuable in project management scenarios, where you can visually track the progress of tasks across stages like ‘To Do’, ‘In Progress’, and ‘Completed’. This visual representation helps monitor workflows and ensure timely completion of tasks.

<figure><img src="../.gitbook/assets/CleanShot 2024-01-11 at 14.49.58@2x.png" alt=""><figcaption></figcaption></figure>

## List Locking&#x20;

Locking a list in Blue prevents records within that list from being moved in or out. You also cannot create new records to a locked list. This allows you to maintain control and integrity over key workflows and processes.

How to lock a list:

1. Identify the specific list you want to lock. This could be:
   * A list representing a stage in a sales workflow (e.g. "Qualified Leads")
   * A list with records needing security or audit purposes
2. Click on the three dots next to the list name to open the list menu.
3. Select "Lock List".

<figure><img src="../.gitbook/assets/CleanShot 2024-01-29 at 13.37.06@2x.png" alt="" width="375"><figcaption></figcaption></figure>

When you lock a list in Blue, it becomes fully restricted to ensure the security of the records within it. A small lock icon will be shown right after the list name.&#x20;

Specifically, locking a list prevents the following actions:

* **Renaming:** The list name cannot be altered once locked. This maintains naming consistency.
* **Bulk Actions:** Bulk record actions such as "Complete All", "Assign All", bulk tagging, etc., are disabled.&#x20;
* **Moving Records In/Out:** Existing records cannot be moved in or out of the locked list.
* **Creating Records:** New records cannot be added directly to a locked list.&#x20;
* **Deleting:** The locked list cannot be deleted altogether.&#x20;

<figure><img src="../.gitbook/assets/CleanShot 2024-01-29 at 13.41.33@2x.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Individual records within a locked list can still be opened, edited, and reviewed, and users can post comments.
{% endhint %}
