# Moving Records

Sometimes you may need to reorganize your records by moving them between projects or lists. Here's how:

1. Navigate to the record you want to move
2. Click on the "..." menu
3. Choose "Move"
4. Select the destination project and list

<figure><img src="../../.gitbook/assets/move record.png" alt=""><figcaption></figcaption></figure>

Things to note:

* Only admin and team members can move records
* When moving across projects, attachments and comments will be carried over
* However, custom fields detail won't be moved acorss&#x20;

